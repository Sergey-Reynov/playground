window.anyclip.loadConf = {
  code: "https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/brief/lre.js",
  lre_publisherConfigRoot: "https://config.anyclip.com/anyclip-widget/config/{{pubname}}/{{widnmae}}/conf.js", // config - PRODUCTION
  lre_playlistApiEndpoint: "https://trafficmanager.anyclip.com/trafficmanager/api/v2/player/playlist?",  // tm - PRODUCTION
};
