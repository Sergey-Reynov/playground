window.anyclip.loadConf = {
  code: "https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/brief/lre.js",
  lre_publisherConfigRoot: "https://anyclip-lre-player-qa.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js", // config - QA
  lre_playlistApiEndpoint: "https://trafficmanager.anyclip.com/trafficmanager/api/v2/player/playlist?",  // tm - PRODUCTION
};
