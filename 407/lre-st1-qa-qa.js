window.anyclip.loadConf = {
  code: "https://player.anyclip.com/anyclip-widget/lre-widget/prod/st1/src/lre.js",  // code - st1
  lre_publisherConfigRoot: "https://anyclip-lre-player-qa.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js", // config - QA
  lre_playlistApiEndpoint: "https://pcn-qa-trafficmanager.anyclipsrv.info/trafficmanager/api/v2/player/playlist?",  // tm - QA
};
