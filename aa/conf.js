ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 4,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 514,
 "publisherDomainId": 119,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": true,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "firstAdRequestDelay": 0,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay-after-ad",
 "auto-play-mobile": "autoplay-after-ad",
 "publisherId": "wowheadcom",
 "sfAccountId": "0011r00001yyEBxAAM",
 "widgetId": "0011r00001yyEBx_371",
 "adTag_https": "https://vid.springserve.com/vast/312704?ima=1&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]",
 "adTag_Mobile_https": "https://vid.springserve.com/vast/312711?ima=1&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]",
 "adTag_https_btf": "https://vid.springserve.com/vast/312704?ima=1&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]",
 "adTag_Mobile_https_btf": "https://vid.springserve.com/vast/312711?ima=1&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]",
 "lre_useAdManager": "ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 500,
  "notInviewSmartAdInterval": 8000
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 500,
  "notInviewSmartAdInterval": 8000
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 415,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "isResponsive": true
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "isResponsive": false,
  "floatingTheme": "bar"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
//  "customStyle": "#ac-lre-wrapper.luminous-theme #ac-player-wrapper.ac-floated-player {    bottom: 50px!important;}",
 "customStyle": "#ac-lre-wrapper.luminous-theme #ac-player-wrapper.ac-floated-player {    bottom: 50px!important;} .ac-player-ph .ac-player-wrapper.ac-floated-player.ac-floated-bar-theme:not(.vjs-bar-fullscreen) .ac-lre-player .video-js .vjs-big-play-button { top: calc(60px - (44px / 2)) !important; left: calc(104px - (44px / 2)) !important; }",
 "playAdsLoopComplete": true,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 0
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": true,
  "playerMoved": true,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": false,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "firstAdRequestDelayUpToVideo": -1,
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": true
 },
"externalTracking": {
    "playerReady" : [
       //{ type : "function", value : "alert('player ready')" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=1" },
        { type : "script", value : "https://secure.adnxs.com/px?id=1438247&t=2" }
    ],
    "firstAdRequest": [
       // { type : "function", value : "alert('first ad request')" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=3" },
        { type : "script", value : "https://secure.adnxs.com/px?id=1438247&t=4" }
    ],
    "everyAdImpression": [
       // { type : "function", value : "alert('ad impression')" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=5" },
        { type : "script", value : "https://secure.adnxs.com/px?id=1438247&t=6" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=7" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=8" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=9" }
    ],
    "everyAdComplete" : [
       // { type : "function", value : "alert('ad complete')" },
        { type : "img", value: "https://secure.adnxs.com/px?id=1438246&t=10" },
    ]
},
 "playerPreset": "LuminousX",
 "lre_playerLogo": {
  "enabled": false
 },
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};