// multi-players by widgetId
var widget_A = window.anyclip.getWidget("001w000001Y8Vax_111");
var widget_B = window.anyclip.getWidget("001w000001Y8Vax_222");

// multi-players in widgets array
var widget_A = window.anyclip.widgets[0];
var widget_B = window.anyclip.widgets[1];

// multi-players by sessionID
var widget_A = window.anyclip.getWidget(null, widget_1_sid);
var widget_B = window.anyclip.getWidget(null, widget_2_sid);

========================================

// pause player A and start playing player B.
widget_A.pause();
widget_B.play();

window.anyclip.getWidget().pause();
window.anyclip.getWidget().play();

========================================

window.anyclip.getWidget().goIdle();
window.anyclip.getWidget().goActive();

========================================

// showCarousel - boolean. true to show. false to hide.

//for single player
window.aclreCarouselShowToggle(showCarousel);

// example - hide carousel
window.aclreCarouselShowToggle(false);

//for multiple players (see: Managing Multiple Players)
widget_A.carouselShowToggle(showCarousel)

========================================

window.aclreSetNewURL(newURL);	//The newURL parameter must be encoded.

====================================
window.aclrePlayerMoved();
------------------
//example
let video = document.querySelector("source_container"); //player should be  initialized and ready to play
let target = document.querySelector("target_container");

video.parentElement.removeChild(video);
target.appendChild(video);

window.aclrePlayerMoved();
---------------------
You can use other methods to move the player's container. 
Important is call window.aclrePlayerMoved() after relocation.
Warning: After moving a player using this API, you must complete the move 
by calling this function, otherwise the player will not function properly.
by calling this function, otherwise the player will not function properly.
====================================
Floating player

// goFloat- boolean. true to enable floating. false to disable.

//for single player
window.aclreFloatingModeToggle(goFloat);

//for multiple players (see: Managing Multiple Players)
widget_A.floatingModeToggle(goFloat)

// example: disable floating
window.aclreFloatingModeToggle(false);

========================================

var theWidget = window.anyclip.getWidget(...);

// always exposed
	theWidget.player // simple property (no getter or setter)
	theWidget.goActive()
	theWidget.goIdle()
	theWidget.destroy()
	theWidget.getCurrentPlaylistItem()

// always exposed, but executed with lre_export_for_demo only
*	theWidget.play()
*	theWidget.pause()
*	theWidget.playlist(asCopy)
*	theWidget.setCurrentTime(positionMs)
*	theWidget.currentIndex()             //get current index
*	theWidget.currentIndex(newIndex)     //set current index
*	theWidget.subscribe(callback, topic, context, priority)
*	theWidget.publish(topic, data)
*	theWidget.unsubscribe(callback, topic)
*	theWidget.disableNext()
*	theWidget.disableVideos()
*	theWidget.sendEvent(type, val)

// exposed with lre_export_for_demo
**	theWidget.sendLike(isSendLike)
**	theWidget.isCurrentVideoLiked(clipId)
**	theWidget.sendShare(mediaTarget)
**	theWidget.setPlaylist(list, startIndex?) // startIndex never used
**	theWidget.setExternalPlaylistResponse(playlistResponse)
**	theWidget.playlistCurrentIndex()       //get current index
**	theWidget.playlistCurrentIndex(index)  //set current index
**	theWidget.setTimelineMarks(pinMarksData)
**	theWidget.highlightMark(itemId)

// exposed explicitly (via funcExpose)
***	theWidget.setUrl(url) // url must be encoded
***	theWidget.playerMoved()
***	theWidget.getConfigValue(val, fallback)
***	theWidget.carouselShowToggle(showCarousel)
***	theWidget.floatingModeToggle(goFloat)


