{
    "playlist": [
        {
            "hlsFile": {
                "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/hls_1614691741770/master.m3u8"
            },
            "title": "Watch salmon leaping in Sebec Lake waterfall",
            "mediaid": "ifmgm6sjinseku3jhbytcr3qkznfg3sw",
            "plot": "Watch as these salmon jump and swim through Earley's Falls on Sebec Lake during the fall spawning run. For more, visit bangordailynews.com.\nVideo courtesy of Maine Department of Inland Fisheries and Wildlife. \n---------------------\nSupport the BDN: bangordailynews.com/support\n\nFollow us:\nFacebook: facebook.com/bangordailynews\nTwitter: twitter.com/bangordailynews\nInstagram: instagram.com/bangordailynews",
            "score": 1.0E9,
            "landingPageLink": "https://www.youtube.com/watch?v=416DP-ivFGo",
            "publisherLink": "https://www.youtube.com/watch?v=416DP-ivFGo",
            "files": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691738658_480x270_video.mp4",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691739294_640x360_video.mp4",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691740176_852x480_video.mp4",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691741047_1280x720_video.mp4",
                    "width": 1280,
                    "height": 720
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691741975_1920x1080_video.mp4",
                    "width": 1920,
                    "height": 1080
                }
            ],
            "distributionKeywords": [
                {
                    "category": "PEOPLE",
                    "keywords": [
                        {
                            "value": "Bangor Daily News",
                            "uid": "5U44-HcBw21maYoJgDxL",
                            "thumbnailUrl": "https://cdn5.anyclip.com/people/l/5u44hcbw21mayojgdxl.png",
                            "actions": {
                                "articles": {
                                    "enabled": true,
                                    "text": "Subscribe",
                                    "url": "/subscribe/"
                                }
                            }
                        }
                    ]
                }
            ],
            "created": 1614284501000,
            "duration": 99534,
            "images": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691723252_248x140_thumbnail.jpg",
                    "width": 248,
                    "height": 140
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691723349_480x270_thumbnail.jpg",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691723295_640x360_thumbnail.jpg",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691723167_852x480_thumbnail.jpg",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691722900_1280x720_thumbnail.jpg",
                    "width": 1280,
                    "height": 720
                }
            ],
            "ccUrl": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/cc/EN/1614778542616_subtitles.vtt",
            "lang": [
                "EN"
            ],
            "aspectRatio": "ASPECT_RATIO_16x9",
            "performanceData": {
                "time": 1615964400000,
                "parameters": [
                    {
                        "type": "VIEW",
                        "value": 509
                    },
                    {
                        "type": "LIKE",
                        "value": 1
                    },
                    {
                        "type": "SHARE",
                        "value": 0
                    }
                ]
            },
            "previewUrl": "https://cdn5.anyclip.com/AXfzICdESi8q1GpVZSnV/1614691722293_preview.mp4",
            "defaultOrder": 0,
            "trending": 2.0,
            "videoId": "AXfzICdESi8q1GpVZSnV"
        },
        {
            "hlsFile": {
                "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/hls_1614691694414/master.m3u8"
            },
            "title": "Ice fishing engagement",
            "mediaid": "ifmgm6sihfeviu3jhbytcr3qkznfg3ce",
            "plot": "Casey Jameson's proposed to his girlfriend Lauren Ray on Pemadumcook Lake using a very unusual tactic: ice fishing.\n\nVideo courtesy of Lauren Ray and Casey Jameson.\n---------------------\nSupport the BDN: bangordailynews.com/support\n\nFollow us:\nFacebook: facebook.com/bangordailynews\nTwitter: twitter.com/bangordailynews\nInstagram: instagram.com/bangordailynews",
            "score": 9.99E8,
            "landingPageLink": "https://www.youtube.com/watch?v=veIkcLFJp_E",
            "publisherLink": "https://www.youtube.com/watch?v=veIkcLFJp_E",
            "files": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691692964_152x270_video.mp4",
                    "width": 152,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691693170_202x360_video.mp4",
                    "width": 202,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691693258_270x480_video.mp4",
                    "width": 270,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691693371_404x720_video.mp4",
                    "width": 404,
                    "height": 720
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691693526_608x1080_video.mp4",
                    "width": 608,
                    "height": 1080
                }
            ],
            "distributionKeywords": [
                {
                    "category": "PEOPLE",
                    "keywords": [
                        {
                            "value": "Bangor Daily News",
                            "uid": "5U44-HcBw21maYoJgDxL",
                            "thumbnailUrl": "https://cdn5.anyclip.com/people/l/5u44hcbw21mayojgdxl.png",
                            "actions": {
                                "articles": {
                                    "enabled": true,
                                    "text": "Subscribe",
                                    "url": "/subscribe/"
                                }
                            }
                        }
                    ]
                }
            ],
            "created": 1614007484000,
            "duration": 8104,
            "images": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691691139_248x140_thumbnail.jpg",
                    "width": 248,
                    "height": 140
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691691030_480x270_thumbnail.jpg",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691691089_640x360_thumbnail.jpg",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691690944_852x480_thumbnail.jpg",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691690881_1280x720_thumbnail.jpg",
                    "width": 1280,
                    "height": 720
                }
            ],
            "ccUrl": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/cc/EN/1614778582011_subtitles.vtt",
            "lang": [
                "EN"
            ],
            "aspectRatio": "ASPECT_RATIO_9x16",
            "performanceData": {
                "time": 1615964400000,
                "parameters": [
                    {
                        "type": "VIEW",
                        "value": 450
                    },
                    {
                        "type": "LIKE",
                        "value": 1
                    },
                    {
                        "type": "SHARE",
                        "value": 0
                    }
                ]
            },
            "previewUrl": "https://cdn5.anyclip.com/AXfzH9ITSi8q1GpVZSlD/1614691690763_preview.mp4",
            "defaultOrder": 1,
            "trending": 2.0,
            "videoId": "AXfzH9ITSi8q1GpVZSlD"
        },
        {
            "hlsFile": {
                "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/hls_1614691635341/master.m3u8"
            },
            "title": "Deer gives coyote the slip",
            "mediaid": "ifmgm6sinrteg4zqlfhta4kloqzugwkw",
            "plot": "This coyote didn't show much interest in another potential meal, as it paid no attention when a deer appears to bound away from this trail camera. For more, visit bangordailynews.com.\nVideo courtesy of Tim Martin.\n---------------------\nSupport the BDN: bangordailynews.com/support\n\nFollow us:\nFacebook: facebook.com/bangordailynews\nTwitter: twitter.com/bangordailynews\nInstagram: instagram.com/bangordailynews",
            "score": 9.98E8,
            "landingPageLink": "https://www.youtube.com/watch?v=2HkoOCAfv6c",
            "publisherLink": "https://www.youtube.com/watch?v=2HkoOCAfv6c",
            "files": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691636756_480x270_video.mp4",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691636960_640x360_video.mp4",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691637239_852x480_video.mp4",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691637469_1280x720_video.mp4",
                    "width": 1280,
                    "height": 720
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691637838_1920x1080_video.mp4",
                    "width": 1920,
                    "height": 1080
                }
            ],
            "distributionKeywords": [
                {
                    "category": "PEOPLE",
                    "keywords": [
                        {
                            "value": "Bangor Daily News",
                            "uid": "5U44-HcBw21maYoJgDxL",
                            "thumbnailUrl": "https://cdn5.anyclip.com/people/l/5u44hcbw21mayojgdxl.png",
                            "actions": {
                                "articles": {
                                    "enabled": true,
                                    "text": "Subscribe",
                                    "url": "/subscribe/"
                                }
                            }
                        }
                    ]
                }
            ],
            "created": 1614284508000,
            "duration": 35634,
            "images": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691612400_248x140_thumbnail.jpg",
                    "width": 248,
                    "height": 140
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691612221_480x270_thumbnail.jpg",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691612303_640x360_thumbnail.jpg",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691612146_852x480_thumbnail.jpg",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691612034_1280x720_thumbnail.jpg",
                    "width": 1280,
                    "height": 720
                }
            ],
            "ccUrl": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/cc/EN/1614778527831_subtitles.vtt",
            "lang": [
                "EN"
            ],
            "aspectRatio": "ASPECT_RATIO_16x9",
            "performanceData": {
                "time": 1615964400000,
                "parameters": [
                    {
                        "type": "VIEW",
                        "value": 454
                    },
                    {
                        "type": "LIKE",
                        "value": 0
                    },
                    {
                        "type": "SHARE",
                        "value": 0
                    }
                ]
            },
            "previewUrl": "https://cdn5.anyclip.com/AXfzHlfCs0YO0qKt3CYV/1614691628969_preview.mp4",
            "defaultOrder": 2,
            "trending": 2.0,
            "videoId": "AXfzHlfCs0YO0qKt3CYV"
        },
        {
            "hlsFile": {
                "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/hls_1614691628445/master.m3u8"
            },
            "title": "A ruffed grouse stops by for a snack",
            "mediaid": "ifmgm6sinnztq4zqlfhta4kloqzugwcm",
            "plot": "This video features a ruffed grouse emerging from the woods for a snack.\nFor more, visit bangordailynews.com.\nVideo by Jim Tyler.\n---------------------\nSupport the BDN: bangordailynews.com/support\n\nFollow us:\nFacebook: facebook.com/bangordailynews\nTwitter: twitter.com/bangordailynews\nInstagram: instagram.com/bangordailynews",
            "score": 9.97E8,
            "landingPageLink": "https://www.youtube.com/watch?v=As2bZfC1kuo",
            "publisherLink": "https://www.youtube.com/watch?v=As2bZfC1kuo",
            "files": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691631074_202x270_video.mp4",
                    "width": 202,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691631296_270x360_video.mp4",
                    "width": 270,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691631657_360x480_video.mp4",
                    "width": 360,
                    "height": 480
                }
            ],
            "distributionKeywords": [
                {
                    "category": "PEOPLE",
                    "keywords": [
                        {
                            "value": "Bangor Daily News",
                            "uid": "5U44-HcBw21maYoJgDxL",
                            "thumbnailUrl": "https://cdn5.anyclip.com/people/l/5u44hcbw21mayojgdxl.png",
                            "actions": {
                                "articles": {
                                    "enabled": true,
                                    "text": "Subscribe",
                                    "url": "/subscribe/"
                                }
                            }
                        }
                    ]
                }
            ],
            "created": 1614007487000,
            "duration": 51267,
            "images": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691608830_248x140_thumbnail.jpg",
                    "width": 248,
                    "height": 140
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691608778_480x270_thumbnail.jpg",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691608870_640x360_thumbnail.jpg",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691608720_852x480_thumbnail.jpg",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691608650_1280x720_thumbnail.jpg",
                    "width": 1280,
                    "height": 720
                }
            ],
            "ccUrl": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/cc/EN/1614856253731_subtitles.vtt",
            "lang": [
                "EN"
            ],
            "aspectRatio": "ASPECT_RATIO_3x4",
            "performanceData": {
                "time": 1615964400000,
                "parameters": [
                    {
                        "type": "VIEW",
                        "value": 428
                    },
                    {
                        "type": "LIKE",
                        "value": 0
                    },
                    {
                        "type": "SHARE",
                        "value": 0
                    }
                ]
            },
            "previewUrl": "https://cdn5.anyclip.com/AXfzHks8s0YO0qKt3CXL/1614691627348_preview.mp4",
            "defaultOrder": 3,
            "trending": 1.0,
            "videoId": "AXfzHks8s0YO0qKt3CXL"
        },
        {
            "hlsFile": {
                "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/hls_1614691624299/master.m3u8"
            },
            "title": "Timelapse of Katahdin, the tallest mountain in Maine",
            "mediaid": "ifmgm6simvwdq4zqlfhta4kloqzugtrv",
            "plot": "This timelapse video shows how the sunlight changes on Katahdin throughout the day on Feb. 15, 2021, as seen from the shore of Millinocket Lake at Twin Pines, New England Outdoor Center. The camera is set up with IPTimelapse timelapse and streaming software.\nVideo courtesy of New England Outdoor Center\n---------------------\nFor live streaming video of Katahdin and more timelapse videos, visit https://www.neoc.com/webcams/. \nFor information about IPTimelapse, visit https://iptimelapse.com/. \n---------------------\nSupport the BDN: bangordailynews.com/support\n\nFollow us:\nFacebook: facebook.com/bangordailynews\nTwitter: twitter.com/bangordailynews\nInstagram: instagram.com/bangordailynews",
            "score": 9.96E8,
            "landingPageLink": "https://www.youtube.com/watch?v=7ujjor-e4OA",
            "publisherLink": "https://www.youtube.com/watch?v=7ujjor-e4OA",
            "files": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691621131_480x270_video.mp4",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691621456_640x360_video.mp4",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691621887_852x480_video.mp4",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691622472_1280x720_video.mp4",
                    "width": 1280,
                    "height": 720
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691623369_1920x1080_video.mp4",
                    "width": 1920,
                    "height": 1080
                }
            ],
            "distributionKeywords": [
                {
                    "category": "PEOPLE",
                    "keywords": [
                        {
                            "value": "Bangor Daily News",
                            "uid": "5U44-HcBw21maYoJgDxL",
                            "thumbnailUrl": "https://cdn5.anyclip.com/people/l/5u44hcbw21mayojgdxl.png",
                            "actions": {
                                "articles": {
                                    "enabled": true,
                                    "text": "Subscribe",
                                    "url": "/subscribe/"
                                }
                            }
                        }
                    ]
                }
            ],
            "created": 1614007493000,
            "duration": 81851,
            "images": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691597193_248x140_thumbnail.jpg",
                    "width": 248,
                    "height": 140
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691597241_480x270_thumbnail.jpg",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691597132_640x360_thumbnail.jpg",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691597093_852x480_thumbnail.jpg",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691597019_1280x720_thumbnail.jpg",
                    "width": 1280,
                    "height": 720
                }
            ],
            "ccUrl": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/cc/EN/1614778582186_subtitles.vtt",
            "lang": [
                "EN"
            ],
            "aspectRatio": "ASPECT_RATIO_16x9",
            "performanceData": {
                "time": 1615964400000,
                "parameters": [
                    {
                        "type": "VIEW",
                        "value": 438
                    },
                    {
                        "type": "LIKE",
                        "value": 0
                    },
                    {
                        "type": "SHARE",
                        "value": 0
                    }
                ]
            },
            "previewUrl": "https://cdn5.anyclip.com/AXfzHel8s0YO0qKt3CN5/1614691608551_preview.mp4",
            "defaultOrder": 4,
            "trending": 1.0,
            "videoId": "AXfzHel8s0YO0qKt3CN5"
        },
        {
            "hlsFile": {
                "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/hls_1614691434928/master.m3u8"
            },
            "title": "Tommy Doucet's \"Iroquois Reel\"",
            "mediaid": "ifmgm6shpbztettgmmwweojvlbegulko",
            "plot": "Bath fiddler and author Frank Ferrel, accompanied by pianist Kimberly Holmes, plays Tommy Doucet's \"Iroquois Reel.\" The tine appears in Ferrel's new book and CD collection \"I'm a Yankee Doodle Dandy, Urban French-American Fiddling from the 1930s - 1950s.\" \n\n--------------------------------\nSupport the BDN: bangordailynews.com/support\n\nFollow us:\nFacebook: facebook.com/bangordailynews\nTwitter: twitter.com/bangordailynews\nInstagram: instagram.com/bangordailynews",
            "score": 9.95E8,
            "landingPageLink": "https://www.youtube.com/watch?v=wknCZCP4pUs",
            "publisherLink": "https://www.youtube.com/watch?v=wknCZCP4pUs",
            "files": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691449994_480x270_video.mp4",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691450689_640x360_video.mp4",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691451612_852x480_video.mp4",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691452551_1280x720_video.mp4",
                    "width": 1280,
                    "height": 720
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691453908_1920x1080_video.mp4",
                    "width": 1920,
                    "height": 1080
                }
            ],
            "distributionKeywords": [
                {
                    "category": "PEOPLE",
                    "keywords": [
                        {
                            "value": "Bangor Daily News",
                            "uid": "5U44-HcBw21maYoJgDxL",
                            "thumbnailUrl": "https://cdn5.anyclip.com/people/l/5u44hcbw21mayojgdxl.png",
                            "actions": {
                                "articles": {
                                    "enabled": true,
                                    "text": "Subscribe",
                                    "url": "/subscribe/"
                                }
                            }
                        }
                    ]
                }
            ],
            "created": 1614284496000,
            "duration": 163917,
            "images": [
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691409955_248x140_thumbnail.jpg",
                    "width": 248,
                    "height": 140
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691409891_480x270_thumbnail.jpg",
                    "width": 480,
                    "height": 270
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691409987_640x360_thumbnail.jpg",
                    "width": 640,
                    "height": 360
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691409857_852x480_thumbnail.jpg",
                    "width": 852,
                    "height": 480
                },
                {
                    "file": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691409667_1280x720_thumbnail.jpg",
                    "width": 1280,
                    "height": 720
                }
            ],
            "ccUrl": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/cc/EN/1614778565114_subtitles.vtt",
            "lang": [
                "EN"
            ],
            "aspectRatio": "ASPECT_RATIO_16x9",
            "performanceData": {
                "time": 1615910400000,
                "parameters": [
                    {
                        "type": "VIEW",
                        "value": 405
                    },
                    {
                        "type": "LIKE",
                        "value": 0
                    },
                    {
                        "type": "SHARE",
                        "value": 0
                    }
                ]
            },
            "previewUrl": "https://cdn5.anyclip.com/AXfzGxs2Nfc-b95XHj-N/1614691419770_preview.mp4",
            "defaultOrder": 5,
            "trending": 0.0,
            "videoId": "AXfzGxs2Nfc-b95XHj-N"
        }
    ],
    "geo": {
        "countryCode": "IL"
    },
    "editorialOnly": true,
    "plFixed": false,
    "userAgent": {
        "allow": true,
        "software": {
            "nameCode": "chrome",
            "ver": "89"
        },
        "os": {
            "nameCode": "android",
            "ver": "Marshmallow"
        },
        "hw": {
            "type": "mobile",
            "subType": null
        }
    }
}