function setClass(selector, className, isSet) {
    let el = document.querySelector(selector);
    if (el) {
        if (isSet) el.classList.add(className);
        else el.classList.remove(className);
    }
}
function getCheckLiveEx() {
    return document.querySelector('#chkLiveEx');
}
function setLiveEx() {
    let cc = getCheckLiveEx();
    if (cc) {
        setClass('.video-js', 'vjs-live', cc.checked);
    }
}

function setMaxLiveCCLines(amount) {
    let store = getStore();
    if (store) {
        if (!amount) {
            let cc = document.querySelector('#nMaxLiveCCLines');
            if (cc) amount = parseInt(cc.value);
        }
        store.config.maxLiveCCLines = amount;
    }
}

function formatTime(ms) {
    let tm = new Date(ms);
    return tm.toISOString().slice((ms < 3600000) ? 14 : 10, 23);
}

function generateVTT(longText, duration) {
    /** @type {string[]} */
    let words = longText.split(' ');
    let totalWords = words.length;
    let factor = duration / totalWords;

    let entries = [
        'WEBVTT',
        '',
    ];
    words.forEach((_, idx) => {
        entries.push(
            `${idx + 1}`,
            `${formatTime(idx * factor)} --> ${formatTime((idx + 1) * factor)} align:start`,
            words.slice(0, idx + 1).join(' '),
            ''
        );
    });

    console.log(entries.join('\r\n'));
}

function generateLorem() {
    generateVTT(`Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
    dolorem, ducimus ad at veniam, cupiditate saepe iure reiciendis delectus
    placeat quisquam illo deleniti blanditiis voluptas vero mollitia
    exercitationem nesciunt, laboriosam quibusdam eligendi. Fugit ea
    voluptatibus veritatis dolorem praesentium tempora minima enim!
    Voluptate debitis libero iste asperiores velit. Similique repellat alias
    consectetur, eligendi harum molestias nihil tempora doloribus vel
    cupiditate laboriosam praesentium itaque facilis nam perferendis aut
    repudiandae impedit atque eum, tempore expedita id et culpa voluptate.
    Quisquam est consequatur voluptatem sequi minima id perferendis impedit
    voluptatibus? Voluptatibus, ut quas officiis illum omnis tempora
    inventore at impedit eius nesciunt ipsam laboriosam.`.replace(/[\s]+/gi, ' '), 48000);
}

function createTMResponse(clipUrl, imageUrl, ccUrl) {
    imageUrl = imageUrl || 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/black-248x140.png';
    let plData = {
        playlist: [
            {
                title: `test play video: ${clipUrl}`,
                mediaid: 'ABCDEDFGtestvideo',
                image: imageUrl,
            },
        ],
    };
    let pp = plData.playlist[0];
    if (clipUrl.indexOf('.m3u8')) {
        pp.hlsFile = { file: clipUrl };
    } else {
        pp.file = clipUrl;
    }
    if (ccUrl) {
        pp.ccFiles = [{
            file: ccUrl,
            lang: 'EN',
            langName: 'English',
        }];
        pp.lang = ['EN'];
    }
    return plData;
}

function setClipTo() {
    try { 
        getStore().runtime.isMonetization = false;
    } catch (err) {}

    let widget = window.anyclip.getWidget();
    let url = document.querySelector('#clip-url').value;
    let ccUrl = document.querySelector('#clip-cc-url').value;
    widget.setExternalPlaylistResponse(createTMResponse(url, null, ccUrl));
}

function getStore() {
    return st ? st[Object.keys(st)[0]] : null;
}
