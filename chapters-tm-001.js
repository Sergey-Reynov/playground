var tm_example_001 = {
  "playlist": [
    {
      "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722823922_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/hls_1623722800971/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722643886_248x140_thumbnail.jpg",
      "title": "Newsom adds dream vacations to vaccine incentives",
      "mediaid": "imzwy32elbxue3dqga2wwscigbgdqx2s",
      "plot": "On the eve of California's re-opening, Governor Newsom is is proposing $95 million in aid to help jump start the state's travel economy. Plus, more incentives for those who are vaccinated.",
      "score": 9820.849,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722822860_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722823132_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722823595_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722823922_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1623718906000,
      "duration": 207167,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722643886_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722643854_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722643785_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722643704_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722643586_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        }
      ],
      "performanceData": {
        "time": 1624345200000,
        "parameters": [
          { "type": "VIEW", "value": 311 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/1623722824392_preview.mp4",
      "defaultOrder": 0,
      "videoId": "C3loDXoBlp05kHH0L8_R",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/cc/EN/1623722598741_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/C3loDXoBlp05kHH0L8_R/cc/EN/1623722598741_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722746748_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/hls_1623722723957/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722623103_248x140_thumbnail.jpg",
      "title": "COVID restrictions going away, but COVID isn't; masks won't disappear",
      "mediaid": "ljlwy3selbxueq2biiwwgsseoy4weqls",
      "plot": "Although the vast majority of COVID-19 restrictions will be lifted Tuesday, Gov. Gavin Newsom warned Monday the virus is still active, and mask-wearing will continue to be a reality for non-vaccinated residents, at businesses that require them or for people who simply feel safer wearing them.",
      "score": 9807.451,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722745728_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722746012_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722746366_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722746748_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1623717013000,
      "duration": 166400,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722623103_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722622656_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722622616_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722622716_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722622447_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        }
      ],
      "performanceData": {
        "time": 1624338000000,
        "parameters": [
          { "type": "VIEW", "value": 123 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/1623722725895_preview.mp4",
      "defaultOrder": 1,
      "videoId": "ZWlnDXoBCAB-cJDv9bAr",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/cc/EN/1623722574758_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/ZWlnDXoBCAB-cJDv9bAr/cc/EN/1623722574758_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215330055_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/hls_1619215314818/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260230_248x140_thumbnail.jpg",
      "title": "Caitlyn Jenner running for California governor",
      "mediaid": "omwtmx2bjbvue4rrlbjue3lvjj3fe3kb",
      "plot": "Transgender activist Caitlyn Jenner of \"Keeping Up with the Kardashians\" fame announced on Friday she is seeking to unseat California Governor Gavin Newsom, whose business-disrupting pandemic response helped fuel a recall drive. This report produced by Chris Dignam.",
      "score": 8082.4897,
      "contentOwner": 43,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215329374_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215329523_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215329830_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215330055_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215330360_1920x1080_video.mp4",
          "width": 1920,
          "height": 1080
        }
      ],
      "created": 1619212627000,
      "duration": 78067,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260230_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260294_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260352_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260384_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260459_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215260493_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1624284000000,
        "parameters": [
          { "type": "VIEW", "value": 8433 },
          { "type": "LIKE", "value": 1 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/1619215295700_preview.mp4",
      "defaultOrder": 2,
      "videoId": "s-6_AHkBr1XSBmuJvRmA",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/cc/EN/1619215248161_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/s-6_AHkBr1XSBmuJvRmA/cc/EN/1619215248161_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347067976_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/hls_1624347076585/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062533_248x140_thumbnail.jpg",
      "title": "Daily politics briefing: June 22",
      "mediaid": "n54c22cnnzxueqs7iz4vuq3spbdvazkw",
      "plot": "The key facts behind the day's headlines on June 22 as Commons Education Select Committee chairman Robert Halfon said terminology like white privilege may have contributed towards systemic neglect of white working-class pupils.",
      "score": 7368.981,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347067444_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347067539_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347067764_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347067976_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1624345832000,
      "duration": 44967,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062533_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062477_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062748_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062660_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062598_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347062320_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 0,
        "parameters": [{ "type": "VIEW", "value": 1 }]
      },
      "previewUrl": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/1624347068152_preview.mp4",
      "defaultOrder": 3,
      "videoId": "ox-hMnoBB_FyZCrxGPeV",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/cc/EN/1624347061442_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/ox-hMnoBB_FyZCrxGPeV/cc/EN/1624347061442_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266452079_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/hls_1624266443677/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266410193_248x140_thumbnail.jpg",
      "title": "Business Secretary plays down rumours that 'triple lock' will end",
      "mediaid": "krktoukmlbxue2rnoeyvs4txou4hqstw",
      "plot": "Business Secretary Kwasi Kwarteng has said he does not believe the Government will suspend the pensions “triple lock” to help meet the cost of the pandemic.\n\nReports have suggested Chancellor Rishi Sunak is considering suspending the triple lock – which guarantees the state pension increases in line with inflation, earnings or 2.5%, whichever is higher – for a year.",
      "score": 7318.352,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266451756_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266451886_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266451985_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266452079_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1624264449000,
      "duration": 26700,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266410193_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266410089_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266410244_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266410317_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266410121_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266409995_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1624348800000,
        "parameters": [
          { "type": "VIEW", "value": 44 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/1624266444238_preview.mp4",
      "defaultOrder": 4,
      "videoId": "TU7QLXoBj-q1Yrwu8xJv",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/cc/EN/1624266320406_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/TU7QLXoBj-q1Yrwu8xJv/cc/EN/1624266320406_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291990598_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/hls_1624291975143/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815886_248x140_thumbnail.jpg",
      "title": "Mnuchin Encourages Congress to Reach a $1 Trillion Infrastructure Deal",
      "mediaid": "jvdeuusmgnxue2rnoeyvs4txovsggvku",
      "plot": "Jun.21 -- Former Treasury Secretary Steven Mnuchin talks about the need for Congress to reach a deal on infrastructure. He spoke in an interview with Bloomberg's David Westin that was part of the Qatar Economic Forum.",
      "score": 7275.038,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291989995_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291990195_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291990439_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291990598_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1624288293000,
      "duration": 127334,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815886_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815813_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815911_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815942_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815853_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291815662_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1624348800000,
        "parameters": [
          { "type": "VIEW", "value": 1888 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/1624291959320_preview.mp4",
      "defaultOrder": 5,
      "videoId": "MFJRL3oBj-q1YrwudcUT",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/cc/EN/1624291711870_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/MFJRL3oBj-q1YrwudcUT/cc/EN/1624291711870_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623946005347_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/hls_1623946003999/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945996950_248x140_thumbnail.jpg",
      "title": "Sir Keir Starmer: Boris Johnson is as hopeless as Matt Hancock after border policy failure",
      "mediaid": "ljyvonkhnzxuerkiliwxms3djvkgorth",
      "plot": "Sir Keir Starmer has described the Prime Minister as \"just as hopeless\" as Health Secretary Matt Hancock, following revelations from Boris Johnson's former aide Dominic Cummings.",
      "score": 7030.561,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623946004951_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623946005056_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623946005217_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623946005347_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1623945626000,
      "duration": 78300,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945996950_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945997094_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945996979_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945997040_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945997144_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945996839_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1624323600000,
        "parameters": [
          { "type": "VIEW", "value": 41 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/1623945996752_preview.mp4",
      "defaultOrder": 6,
      "videoId": "ZqW5GnoBEHZ-vKcMTgFg",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/cc/EN/1623945998134_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/ZqW5GnoBEHZ-vKcMTgFg/cc/EN/1623945998134_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037694447_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/hls_1616037685599/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677274_248x140_thumbnail.jpg",
      "title": "Jesuits in US pledge $100M for racial reconciliation  , and other top stories in US news from March 17, 2021.",
      "mediaid": "ifmgqrcxmuyeiwlgm52ditttgnstatde",
      "plot": "Your source for breaking news. You can count on us for the latest headlines in US and international news, business, sports, weather and entertainment. Jesuits in US pledge $100M for racial reconciliation  , and other top stories in US news from March 17, 2021.",
      "score": 6925.268,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037693195_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037693446_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037693872_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037694447_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1615956164000,
      "duration": 46134,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677274_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677327_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677462_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677504_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677359_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037677127_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1623765600000,
        "parameters": [
          { "type": "VIEW", "value": 285 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/1616037676843_preview.mp4",
      "defaultOrder": 7,
      "videoId": "AXhDWe0DYfgt4Ns3e0Ld",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/cc/EN/1616037693840_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/AXhDWe0DYfgt4Ns3e0Ld/cc/EN/1616037693840_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738645841_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/hls_1623738650859/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635961_248x140_thumbnail.jpg",
      "title": "Daily politics briefing: June 15",
      "mediaid": "mfbwyzcenzxueqkkk5iu2r3yjnhtsmle",
      "plot": "A look at the facts behind the day's headlines as Prime Minister Boris Johnson set a new “terminus date” of July 19 for the end of England’s coronavirus restrictions.",
      "score": 6535.4663,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738645462_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738645626_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738645712_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738645841_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1623738491000,
      "duration": 48000,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635961_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635696_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635560_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635778_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635886_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635444_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1624316400000,
        "parameters": [
          { "type": "VIEW", "value": 41 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/1623738635260_preview.mp4",
      "defaultOrder": 8,
      "videoId": "aCldDnoBAJWQMGxKO91d",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/cc/EN/1623738647059_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/aCldDnoBAJWQMGxKO91d/cc/EN/1623738647059_subtitles.vtt"
    },
    {
      "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645075081_1280x720_video.mp4",
      "hlsFile": {
        "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/hls_1623645069943/master.m3u8"
      },
      "image": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062642_248x140_thumbnail.jpg",
      "title": "Boris Johnson faces Tory backlash as lockdown lifting put on hold",
      "mediaid": "gbaxussdjbxuevtfmzzwy2ldozjusudu",
      "plot": "Boris Johnson is facing a furious backlash from Tory MPs as he prepares to put the final lifting of coronavirus lockdown restrictions in England on hold.\n\nThe Prime Minister is expected to announce the ending of social-distancing rules – which had been slated for June 21 – will be delayed for four weeks to July 19.\n\nThe move follows warnings from scientists that the rapid spread of the Delta variant first identified in India risks a “substantial” third wave if it is allowed to spread unchecked.",
      "score": 6375.87,
      "contentOwner": 86,
      "files": [
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645074741_480x270_video.mp4",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645074855_640x360_video.mp4",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645074953_852x480_video.mp4",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645075081_1280x720_video.mp4",
          "width": 1280,
          "height": 720
        }
      ],
      "created": 1623643126000,
      "duration": 35900,
      "images": [
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062642_248x140_thumbnail.jpg",
          "width": 248,
          "height": 140
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062616_480x270_thumbnail.jpg",
          "width": 480,
          "height": 270
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062559_640x360_thumbnail.jpg",
          "width": 640,
          "height": 360
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062398_852x480_thumbnail.jpg",
          "width": 852,
          "height": 480
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062466_1280x720_thumbnail.jpg",
          "width": 1280,
          "height": 720
        },
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062224_1920x1080_thumbnail.jpg",
          "width": 1920,
          "height": 1080
        }
      ],
      "performanceData": {
        "time": 1624348800000,
        "parameters": [
          { "type": "VIEW", "value": 213 },
          { "type": "LIKE", "value": 0 },
          { "type": "SHARE", "value": 0 }
        ]
      },
      "previewUrl": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/1623645062262_preview.mp4",
      "defaultOrder": 9,
      "videoId": "0AzJCHoBVefslicvSIPt",
      "ccFiles": [
        {
          "file": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/cc/EN/1623645054705_subtitles.vtt",
          "lang": "EN"
        }
      ],
      "ccUrl": "https://cdn5.anyclip.com/0AzJCHoBVefslicvSIPt/cc/EN/1623645054705_subtitles.vtt"
    }
  ],
  "geo": { "countryCode": "IL" },
  "editorialOnly": false,
  "plFixed": false,
  "userAgent": {
    "allow": true,
    "software": { "nameCode": "chrome", "ver": "91" },
    "os": { "nameCode": "windows", "ver": "10" },
    "hw": { "type": "computer", "subType": null }
  },
  "topDomain": "usnews.com"
}
