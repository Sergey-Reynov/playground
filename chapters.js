(function () {
    const optimalWidth = 800
    
    let defaultWidth = optimalWidth
    const updateDefaultWidth = () => {
        let width = document.body.clientWidth - 20
        defaultWidth = Math.min(optimalWidth, Math.round(Math.floor(width / 100) * 100))
        return defaultWidth
    }

    // copied as is from webpack.config - simulate 'embedded' parameters
    const envMap = {
        dev: {
            lre_Theme_Path:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme',
            PixelImageUrl: 'https://pixelpcn-dev.anyclip.com/vmp.gif',
            TMApiEndpoint:
                'https://pcn-dev-trafficmanager.anyclipsrv.info/trafficmanager/api/v2/player/playlist?',
            TMSocialApiEndpoint:
                'https://pcn-dev-trafficmanager.anyclipsrv.info/trafficmanager/api/videos/video/action',
            ConfigsPath:
                'https://anyclip-lre-player-dev.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js',
            VideoJSLibUrl:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js/vjs-src',
            adblockDetectUrl:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js',
            acAdmanagerUrl:
                'https://anyclip-lre-player-dev.s3.amazonaws.com/lreprx/js/v1/src/lreprx.js',
            acAdmanagerStagingUrl:
                'https://anyclip-lre-player-dev.s3.amazonaws.com/lreprx/js/st1/src/lreprx.js',
            acAdserverUrl: 'https://pcn-dev-lreprx-server.anyclipsrv.info/?',
            SpsImaRulesPath: './spsImaModeRules.js',
        },
        qa: {
            lre_Theme_Path:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme',
            PixelImageUrl: 'https://pixelpcn-qa.anyclip.com/vmp.gif',
            TMApiEndpoint:
                'https://pcn-qa-trafficmanager.anyclipsrv.info/trafficmanager/api/v2/player/playlist?',
            TMSocialApiEndpoint:
                'https://pcn-qa-trafficmanager.anyclipsrv.info/trafficmanager/api/videos/video/action',
            ConfigsPath:
                'https://anyclip-lre-player-qa.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js',
            VideoJSLibUrl:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js/vjs-src',
            adblockDetectUrl:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js',
            acAdmanagerUrl:
                'https://anyclip-lre-player-qa.s3.amazonaws.com/lreprx/js/v1/src/lreprx.js',
            acAdmanagerStagingUrl:
                'https://anyclip-lre-player-qa.s3.amazonaws.com/lreprx/js/st1/src/lreprx.js',
            acAdserverUrl: 'https://pcn-qa-lreprx-server.anyclipsrv.info/?',
            SpsImaRulesPath:
                'https://anyclip-lre-player-qa.s3.amazonaws.com/anyclip-widget/sps-flow/rules.js',
        },
        int: {
            lre_Theme_Path:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme',
            PixelImageUrl: 'https://pixelpcn-dev.anyclip.com/vmp.gif',
            TMApiEndpoint:
                'https://pcn-int-trafficmanager.anyclipsrv.info/trafficmanager/api/v2/player/playlist?',
            TMSocialApiEndpoint:
                'https://pcn-int-trafficmanager.anyclipsrv.info/trafficmanager/api/videos/video/action',
            ConfigsPath:
                'https://anyclip-lre-player-int.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js',
            VideoJSLibUrl:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js/vjs-src',
            adblockDetectUrl:
                'https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js',
            acAdmanagerUrl:
                'https://anyclip-lre-player-qa.s3.amazonaws.com/lreprx/js/v1/src/lreprx.js',
            acAdmanagerStagingUrl:
                'https://pcn-dev-lreprx-server.anyclipsrv.info/?',
            acAdserverUrl: 'https://pcn-qa-lreprx-server.anyclipsrv.info/?',
            SpsImaRulesPath:
                'https://anyclip-lre-player-qa.s3.amazonaws.com/anyclip-widget/sps-flow/rules.js',
        },
        prod: {
            lre_Theme_Path:
                'https://assets.anyclip.com/anyclip-widget/lre-widget/assets/lre_theme',
            PixelImageUrl: 'https://pixel.anyclip.com/vmp.gif',
            TMApiEndpoint:
                'https://trafficmanager.anyclip.com/trafficmanager/api/v2/player/playlist?',
            TMSocialApiEndpoint:
                'https://trafficmanager.anyclip.com/trafficmanager/api/videos/video/action',
            ConfigsPath:
                'https://config.anyclip.com/anyclip-widget/config/{{pubname}}/{{widnmae}}/conf.js',
            VideoJSLibUrl:
                'https://assets.anyclip.com/anyclip-widget/lre-widget/assets/js/vjs-src',
            adblockDetectUrl:
                'https://assets.anyclip.com/anyclip-widget/lre-widget/assets/js',
            acAdmanagerUrl: 'https://player.anyclip.com/lreprx/js/v1/src/lreprx.js',
            acAdmanagerStagingUrl:
                'https://player.anyclip.com/lreprx/js/st1/src/lreprx.js',
            acAdserverUrl: 'https://lreprx-server.anyclip.com/?',
            SpsImaRulesPath:
                'https://player.anyclip.com/anyclip-widget/lre-widget/sps-flow/rules.js',
        },
    }
    
    const getCustomConfig = mode => {
        if (mode === 'prod-conf-qa-tm-qa') {
            const dt = getCustomConfig('prod')
            const qa = getCustomConfig('qa')
            dt.lre_publisherConfigRoot = qa.lre_publisherConfigRoot
            dt.lre_playlistApiEndpoint = qa.lre_playlistApiEndpoint
            return dt
        }

        if (mode === 'prod-conf-qa') {
            const dt = getCustomConfig('prod')
            const qa = getCustomConfig('qa')
            dt.lre_publisherConfigRoot = qa.lre_publisherConfigRoot
            return dt
        }

        const envData = envMap[mode]
        if (!envData) return null
        return {
            lre_publisherConfigRoot:        envData.ConfigsPath,
            lre_playlistApiEndpoint:        envData.TMApiEndpoint,
            lre_pixelUrl:                   envData.PixelImageUrl,
            lre_socialActionsApiEndpoint:   envData.TMSocialApiEndpoint,
            lre_videojsLibPath:             envData.VideoJSLibUrl,
            lre_adblockDetectorPath:        envData.adblockDetectUrl,
            anyclipAdManagerURL:            envData.acAdmanagerUrl,
            anyclipAdManagerStagingURL:     envData.acAdmanagerStagingUrl,
            anyclipAdServerURL:             envData.acAdserverUrl,
            lre_imaModeRulesLocation:       envData.SpsImaRulesPath,
        }
    }

    const setupPlayer = () => {
        const ph = document.querySelector('[data-lre-player-placeholder]')
        if (!ph) return

        let playerUrl = 'lre/lre.js'
        let pubname = 'usnewscom'
        let widgetname = '0011r00002IjDWF_725'
        let mode = null
        let trigger = null

        if (typeof URLSearchParams !== 'undefined') {
            const params = new URLSearchParams(window.location.search)
            playerUrl = params.get('js') || playerUrl
            pubname = params.get('pubname') || pubname
            widgetname = params.get('widgetname') || widgetname
            mode = params.get('mode') || mode
            trigger = params.get('----anyclip-lre-config') || trigger
        }

        console.log('player:', playerUrl)
        console.log('pubname:', pubname)
        console.log('widgetname:', widgetname)

        mode = 'prod'

        if (trigger) {
            console.log('secret trigger:', trigger)
        } else if (mode) {
            console.log('predefined mode:', mode)
            let cfg = getCustomConfig(mode) || {}
            if (cfg) {
                cfg.lre_publisherConfigRoot = 
                    'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/chapters/conf-for-demo.js'
                    
                window.anyclip = window.anyclip || {}
                window.anyclip.loadConf = cfg
                console.log('config by mode', cfg)
            }
        }

        const scr = document.createElement('script')
        scr.src = playerUrl
        scr.setAttribute('pubname', pubname)
        scr.setAttribute('widgetname', widgetname)
        ph.appendChild(scr);

        return ph
    }

    const chaptersExample = [
        {
          "time": 1000,
          "name": "Chapter the First"
        },
        {
          "time": 10000,
          "name": "Chapter the Second"
        },
        {
          "time": 30000,
          "name": "Chapter the Third"
        },
        {
          "time": 60000,
          "name": "Chapter the Fourth"
        },
        {
          "time": 150000,
          "name": "Chapter the Fifth"
        }
    ];

    const generatePlaylist = (originalTmResponse, chaptersData) => {
        let response = JSON.parse(JSON.stringify(originalTmResponse));
        if (chaptersData) {
            for (let idx in response.playlist) {
                let clip = response.playlist[idx];
                clip.chapters = JSON.parse(JSON.stringify(chaptersData));
            }
        }
        return response;
    }

    const setupControlsWidth = () => {
        const publisherArea = document.querySelector('.publisher-area')
        const indicator = document.querySelector('#indication-width')
        const getWidth = () => publisherArea.clientWidth
        const updateIndicator = () => indicator.innerHTML = `Width: ${getWidth()}px`
        const setWidth = amount => {
            publisherArea.style.width = `${amount}px`
            updateIndicator()
        }

        const changeWidth = changeAmount => setWidth(getWidth() + changeAmount)
        document.querySelector('#button-minus-1').addEventListener('click', () => changeWidth(-1))
        document.querySelector('#button-minus-10').addEventListener('click', () => changeWidth(-10))
        document.querySelector('#button-minus-100').addEventListener('click', () => changeWidth(-100))
        document.querySelector('#button-plus-1').addEventListener('click', () => changeWidth(1))
        document.querySelector('#button-plus-10').addEventListener('click', () => changeWidth(10))
        document.querySelector('#button-plus-100').addEventListener('click', () => changeWidth(100))
        indicator.addEventListener('click', () => setWidth(updateDefaultWidth()))

        setTimeout(() => {
            setWidth(defaultWidth)
            console.log('indication initialized')
        }, 600)
    }

    const getInputValue = selector => {
        if (selector.startsWith('@')) return selector.substr(1)
        let x = document.querySelector(selector)
        // console.log('getInputValue', selector, x)
        if (!x) return undefined
        if (selector.startsWith('#check-')) return (x.checked ? true : false)
        if (selector.startsWith('#number-')) return parseFloat(x.value)
        return x.value
    }

    // const cfgDisplayDiv = document.querySelector('#carousel-configuration')
    // if (cfgDisplayDiv) {
    //     const copyConfig = () => {
    //         try {
    //             navigator.clipboard.writeText(configExample.textContent)
    //             alert('configuration copied to the clipboard')
    //         } catch (err) {
    //             console.log('clipboard not available')
    //         }
    //     }
    //     configCopy.style.display = 'none'
    //     configCopy.addEventListener('click', () => copyConfig())
    // }

    if (!setupPlayer()) return

    var chaptersExampleControl = document.querySelector('#chapters-data-example');
    chaptersExampleControl.innerText = JSON.stringify(chaptersExample, null, 2);

    const updatePlaylist = () => {
        let txt = chaptersExampleControl.textContent
        let chp = JSON.parse(txt)
        console.log(txt, chp)
        if (chp) {
            let tmResponse = generatePlaylist(tm_example_001, chp)
            console.log(tmResponse)
            anyclip.widgets[0].setExternalPlaylistResponse(tmResponse)
        }
    }
    const updatePlaylistLong = () => {
        anyclip.widgets[0].setExternalPlaylistResponse(tm_example_002);
    }

    const trySetPlaylist = () => {
        if (window.anyclip && window.anyclip.widgets) {
            let ww = window.anyclip.widgets[0]
            if (ww && typeof ww.setExternalPlaylistResponse === 'function') {
                setTimeout(updatePlaylist, 1000)
                return
            }
        }
        setTimeout(trySetPlaylist, 500); // try again later
    }

    document.querySelector('#update-player').addEventListener('click', updatePlaylist)
    document.querySelector('#update-player-long').addEventListener('click', updatePlaylistLong)

    updateDefaultWidth()
    console.log(defaultWidth)
    // setupControlsConfig()
    setupControlsWidth()

    // console.log(tm_example_001)
    // console.log(JSON.stringify(tm_example_001))

    trySetPlaylist()

    // updateConfigExample(getConfigData())
})()
