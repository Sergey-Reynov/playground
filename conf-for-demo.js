ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 10,
 "startAdsBeforeClip": 10,
 "loopPlaylist": true,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 165,
 "publisherDomainId": 827,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "playbackSpeedControl": true,
 "videoResolutionControl": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": true,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": true,
 "locked": false,
 "pauseClipNotInView": false,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "fallbackPlaylist": [
  {
   "file": "https://cdn5.anyclip.com/AV0NkJt4rDXdkB4YlmYA/clip/AV_3OafmdgxsIT9tHDHS/clip.mp4",
   "title": "We Could Totally Be Lawyers",
   "image": "https://cdn5.anyclip.com/IMDB_2637276.jpg",
   "mediaid": "AV_3OafmdgxsIT9tHDHS",
   "files": [
    {
     "width": 1920,
     "height": 800,
     "file": "https://cdn5.anyclip.com/AV0NkJt4rDXdkB4YlmYA/clip/AV_3OafmdgxsIT9tHDHS/clip.mp4"
    }
   ],
   "images": [
    {
     "width": 182,
     "height": 268,
     "file": "https://cdn5.anyclip.com/IMDB_2637276.jpg"
    }
   ]
  }
 ],
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay",
 "auto-play-mobile": "autoplay",
 "publisherId": "lre_demo_page",
 "sfAccountId": "001w000001fC68UAAS",
 "widgetId": "widget_noAds_TR",
 "adTag_https": "https://vast1.aniview.com/api/adserver61/vast/?AV_PUBLISHERID=58d7dc4328a0611e7a4535da&AV_CHANNELID=5c373e4328a06176465070c9&AV_URL=$[pageUrl]&format=json&cb=$[cb]&AV_WIDTH=$[width]&AV_HEIGHT=$[height]&AV_CUSTOM1=$[clipId]&AV_CUSTOM2=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&AV_CUSTOM3=$[widgetId]&AV_CUSTOM4=$[abc]&AV_CUSTOM5=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&logo=false&showpreloader=false&hidecontrols=false&pausebutton=false&timelinemode=none&AV_CUSTOM6=$[gdpr]&AV_CUSTOM7=$[cd]&AV_CUSTOM8=$[v]&AV_CUSTOM9=$[schain]",
 "adTag_Mobile_https": "https://vast1.aniview.com/api/adserver61/vast/?AV_PUBLISHERID=58d7dc4328a0611e7a4535da&AV_CHANNELID=5c373e4328a06176465070c9&AV_URL=$[pageUrl]&format=json&cb=$[cb]&AV_WIDTH=$[width]&AV_HEIGHT=$[height]&AV_CUSTOM1=$[clipId]&AV_CUSTOM2=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&AV_CUSTOM3=$[widgetId]&AV_CUSTOM4=$[abc]&AV_CUSTOM5=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&logo=false&showpreloader=false&hidecontrols=false&pausebutton=false&timelinemode=none&AV_CUSTOM6=$[gdpr]&AV_CUSTOM7=$[cd]&AV_CUSTOM8=$[v]&AV_CUSTOM9=$[schain]",
 "adTag_https_btf": "https://vast1.aniview.com/api/adserver61/vast/?AV_PUBLISHERID=58d7dc4328a0611e7a4535da&AV_CHANNELID=5c373e4328a06176465070c9&AV_URL=$[pageUrl]&format=json&cb=$[cb]&AV_WIDTH=$[width]&AV_HEIGHT=$[height]&AV_CUSTOM1=$[clipId]&AV_CUSTOM2=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&AV_CUSTOM3=$[widgetId]&AV_CUSTOM4=$[abc]&AV_CUSTOM5=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&logo=false&showpreloader=false&hidecontrols=false&pausebutton=false&timelinemode=none&AV_CUSTOM6=$[gdpr]&AV_CUSTOM7=$[cd]&AV_CUSTOM8=$[v]&AV_CUSTOM9=$[schain]",
 "adTag_Mobile_https_btf": "https://vast1.aniview.com/api/adserver61/vast/?AV_PUBLISHERID=58d7dc4328a0611e7a4535da&AV_CHANNELID=5c373e4328a06176465070c9&AV_URL=$[pageUrl]&format=json&cb=$[cb]&AV_WIDTH=$[width]&AV_HEIGHT=$[height]&AV_CUSTOM1=$[clipId]&AV_CUSTOM2=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&AV_CUSTOM3=$[widgetId]&AV_CUSTOM4=$[abc]&AV_CUSTOM5=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&logo=false&showpreloader=false&hidecontrols=false&pausebutton=false&timelinemode=none&AV_CUSTOM6=$[gdpr]&AV_CUSTOM7=$[cd]&AV_CUSTOM8=$[v]&AV_CUSTOM9=$[schain]",
 "lre_useAdManager": "off",
 "lre_useAdManagerForMobile": "off",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": false,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 30000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 9000,
  "imaAdRequestTimeout_btf": 9000,
  "maxAdsPerClip": 0,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 0,
  "adRequestDelay": 0
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": false,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 25000,
  "notInviewAdInterval": 5000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 0,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 0,
  "adRequestDelay": 0
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": true,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": "",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 0
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "relevant",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "videoMediaType": "mp4",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": "LuminousX",
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};