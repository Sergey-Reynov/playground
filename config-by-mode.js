(function() {
    const playground = window._____lrePlayground = window._____lrePlayground || {}

    // copied as is from webpack.config - simulate 'embedded' parameters
    playground.envMap = {
      dev: {
        label: "Development",
        lre_Theme_Path:
            'https://acdev-lre-player-dev.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme',
        PixelImageUrl: 'https://pixel-ac-dev.dev-anyclip.com/vmp.gif',
        PixelUrl : 'https://pixel-ac-dev.dev-anyclip.com/pixel',
        marketplaceEventsApiUrl : 'https://ac-dev-marketplace.dev-anyclip.com/v1/events',
        marketplaceEventsApiLre : 'https://ac-dev-marketplace.dev-anyclip.com/v1/lre-events',
        TMApiEndpoint:
            'https://ac-dev-trafficmanager.dev-anyclip.com/trafficmanager/api/v2/player/playlist?',
        TMAuthEndpoint:
            'https://ac-dev-trafficmanager.dev-anyclip.com/trafficmanager/api/authorization/authorize',
        TMInstantDeliveryEndpoint:
            'https://ac-dev-trafficmanager.dev-anyclip.com/trafficmanager/api/v4/player/form-submit-notification',
        TMSocialApiEndpoint:
            'https://ac-dev-trafficmanager.dev-anyclip.com/trafficmanager/api/videos/video/action',
        ConfigsPath:
            'https://acdev-lre-player-dev.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js',
        FormBuilderUrl:
            'https://acdev-lre-player-dev.s3.amazonaws.com/acfb/altenv/other-env/qa/v1/src/acfb.js',
        VideoJSLibUrl:
            'https://acdev-lre-player-dev.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js/vjs-src',
        adblockDetectUrl:
            'https://acdev-lre-player-dev.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js',
        acAdmanagerUrl:
            'https://acdev-lre-player-dev.s3.amazonaws.com/lreprx/js/v1/src/lreprx.js',
        acAdmanagerStagingUrl:
            'https://acdev-lre-player-dev.s3.amazonaws.com/lreprx/js/st1/src/lreprx.js',
        acAdserverUrl: 'https://ac-dev-lreprx-server.dev-anyclip.com/?',
        SpsImaRulesPath: 'https://player.anyclip.com/anyclip-widget/lre-widget/sps-flow/rules.js',
        backupPlaylistLocation: 'https://acdev-pl-dev.s3.amazonaws.com',
      },
      qa: {
        label: "QA",
        lre_Theme_Path:
            'https://acdev-lre-player-qa.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme',
        PixelImageUrl: 'https://pixel-ac-qa.qa-anyclip.com/vmp.gif',
        PixelUrl : 'https://pixel-ac-qa.qa-anyclip.com/pixel',
        marketplaceEventsApiUrl : 'https://ac-qa-marketplace.qa-anyclip.com/v1/events',
        marketplaceEventsApiLre : 'https://ac-qa-marketplace.qa-anyclip.com/v1/lre-events',
        TMApiEndpoint:
            'https://ac-qa-trafficmanager.qa-anyclip.com/trafficmanager/api/v2/player/playlist?',
        TMAuthEndpoint:
            'https://ac-qa-trafficmanager.qa-anyclip.com/trafficmanager/api/authorization/authorize',
        TMInstantDeliveryEndpoint:
            'https://ac-qa-trafficmanager.qa-anyclip.com/trafficmanager/api/v4/player/form-submit-notification',
        TMSocialApiEndpoint:
            'https://ac-qa-trafficmanager.qa-anyclip.com/trafficmanager/api/videos/video/action',
        ConfigsPath:
            'https://acdev-lre-player-qa.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js',
        FormBuilderUrl:
            'https://acdev-lre-player-qa.s3.amazonaws.com/anyclip-widget/acfb/qa/v1/src/acfb.js',
        VideoJSLibUrl:
            'https://acdev-lre-player-qa.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js/vjs-src',
        adblockDetectUrl:
            'https://acdev-lre-player-qa.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js',
        acAdmanagerUrl:
            'https://acdev-lre-player-qa.s3.amazonaws.com/lreprx/js/v1/src/lreprx.js',
        acAdmanagerStagingUrl:
            'https://acdev-lre-player-qa.s3.amazonaws.com/lreprx/js/st1/src/lreprx.js',
        acAdserverUrl: 'https://ac-qa-lreprx-server.qa-anyclip.com/?',
        SpsImaRulesPath: 'https://player.anyclip.com/anyclip-widget/lre-widget/sps-flow/rules.js',
        backupPlaylistLocation: 'https://acdev-pl-qa.s3.amazonaws.com',
      },
      int: {
        label: "Integration",
        lre_Theme_Path:
            'https://acdev-lre-player-int.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme',
        PixelImageUrl: 'https://pixel-ac-dev.dev-anyclip.com/vmp.gif',
        PixelUrl : 'https://pixel-ac-dev.dev-anyclip.com/pixel',
        marketplaceEventsApiUrl : 'https://ac-dev-marketplace.dev-anyclip.com/v1/events',
        marketplaceEventsApiLre : 'https://ac-dev-marketplace.dev-anyclip.com/v1/lre-events',
        TMApiEndpoint:
            'https://ac-int-trafficmanager.dev-anyclip.com/trafficmanager/api/v2/player/playlist?',
        TMAuthEndpoint:
            'https://ac-int-trafficmanager.dev-anyclip.com/trafficmanager/api/authorization/authorize',
        TMInstantDeliveryEndpoint:
            'https://ac-int-trafficmanager.dev-anyclip.com/trafficmanager/api/v4/player/form-submit-notification',
        TMSocialApiEndpoint:
            'https://ac-int-trafficmanager.dev-anyclip.com/trafficmanager/api/videos/video/action',
        ConfigsPath:
            'https://acdev-lre-player-int.s3.amazonaws.com/config/{{pubname}}/{{widnmae}}/conf.js',
        FormBuilderUrl:
            'https://acdev-lre-player-int.s3.amazonaws.com/anyclip-widget/acfb/int/v1/src/acfb.js',
        VideoJSLibUrl:
            'https://acdev-lre-player-int.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js/vjs-src',
        adblockDetectUrl:
            'https://acdev-lre-player-int.s3.amazonaws.com/anyclip-widget/lre-widget/assets/js',
        acAdmanagerUrl:
            'https://acdev-lre-player-int.s3.amazonaws.com/lreprx/js/v1/src/lreprx.js',
        acAdmanagerStagingUrl:
            'https://acdev-lre-player-int.s3.amazonaws.com/lreprx/js/st1/src/lreprx.js',
        acAdserverUrl: 'https://ac-dev-lreprx-server.dev-anyclip.com/?',
        SpsImaRulesPath: 'https://player.anyclip.com/anyclip-widget/lre-widget/sps-flow/rules.js',
        backupPlaylistLocation: 'https://acdev-pl-dev.s3.amazonaws.com',
      },
      prod: {
        label: "Production",
        lre_Theme_Path:
            'https://assets.anyclip.com/anyclip-widget/lre-widget/assets/lre_theme',
        PixelImageUrl: 'https://pixel.anyclip.com/vmp.gif',
        PixelUrl : 'https://pixel.anyclip.com/pixel',
        marketplaceEventsApiUrl : 'https://marketplace.anyclip.com/v1/events',
        marketplaceEventsApiLre : 'https://marketplace.anyclip.com/v1/lre-events',
        TMApiEndpoint:
            'https://trafficmanager.anyclip.com/trafficmanager/api/v2/player/playlist?',
        TMAuthEndpoint:
            'https://trafficmanager.anyclip.com/trafficmanager/api/authorization/authorize',
        TMInstantDeliveryEndpoint:
            'https://trafficmanager.anyclip.com/trafficmanager/api/v4/player/form-submit-notification',
        TMSocialApiEndpoint:
            'https://trafficmanager.anyclip.com/trafficmanager/api/videos/video/action',
        ConfigsPath:
            'https://config.anyclip.com/anyclip-widget/config/{{pubname}}/{{widnmae}}/conf.js',
        FormBuilderUrl:
            'https://player.anyclip.com/acfb/prod/v1/src/acfb.js',
        VideoJSLibUrl:
            'https://assets.anyclip.com/anyclip-widget/lre-widget/assets/js/vjs-src',
        adblockDetectUrl:
            'https://assets.anyclip.com/anyclip-widget/lre-widget/assets/js',
        acAdmanagerUrl: 'https://player.anyclip.com/lreprx/js/v1/src/lreprx.js',
        acAdmanagerStagingUrl:
            'https://player.anyclip.com/lreprx/js/st1/src/lreprx.js',
        acAdserverUrl: 'https://lreprx-server.anyclip.com/?',
        SpsImaRulesPath:
            'https://player.anyclip.com/anyclip-widget/lre-widget/sps-flow/rules.js',
        backupPlaylistLocation: 'https://anyclip-pl-prod.s3.amazonaws.com',
      },
      empty: {
        label: "As is",
      }
    };

    const clearConfig = (cfg) => {
        let count = 0;
        if (cfg) {
            Object.keys(cfg).forEach(key => {
                if (cfg[key] === undefined) delete cfg[key]
                else count++
            })
        }
        return count ? cfg : null
    }

    /**
     * Get parameters - embedded during building version - for replace
     * @param {string} mode build mode - enumerated: 'dev', 'qa', 'int', 'prod'
     * @returns object or null if mode not in enumeration
     */
    playground.getCustomConfig = (mode) => {
        const envData = playground.envMap[mode]
        if (!envData) return null
        return clearConfig({
            lre_publisherConfigRoot:        envData.ConfigsPath,
            lre_playlistApiEndpoint:        envData.TMApiEndpoint,
            lre_pixelImageUrl:              envData.PixelImageUrl,
            lre_pixelUrl:                   envData.PixelUrl,
            lre_tmagw:                      envData.TMAuthEndpoint,
            lre_instantDeliveryApiEndpoint: envData.TMInstantDeliveryEndpoint,
            lre_socialActionsApiEndpoint:   envData.TMSocialApiEndpoint,
            lre_videojsLibPath:             envData.VideoJSLibUrl,
            lre_adblockDetectorPath:        envData.adblockDetectUrl,
            anyclipAdManagerURL:            envData.acAdmanagerUrl,
            anyclipAdManagerStagingURL:     envData.acAdmanagerStagingUrl,
            anyclipAdServerURL:             envData.acAdserverUrl,
            lre_imaModeRulesLocation:       envData.SpsImaRulesPath,
            marketplaceEventsApiUrl:        envData.marketplaceEventsApiUrl,
            marketplaceEventsApiLre:        envData.marketplaceEventsApiLre,
            lre_backupPlaylistLocation:     envData.backupPlaylistLocation,
            lre_formBuilderUrl:             envData.FormBuilderUrl,
        })
    }

    playground.getMixedConfig = (modeTraffic, modeConfig, modeMarketplace) => {
        let cfg = playground.getCustomConfig(modeTraffic)
        let cfgConfig = playground.getCustomConfig(modeConfig)
        let cfgMarketplace = playground.getCustomConfig(modeMarketplace)

        cfg = { ...(cfg || cfgConfig || cfgMarketplace) }
        if (cfgConfig) cfg.lre_publisherConfigRoot = cfgConfig.lre_publisherConfigRoot
        if (cfgMarketplace) {
            cfg.marketplaceEventsApiUrl = cfgMarketplace.marketplaceEventsApiUrl
            cfg.marketplaceEventsApiLre = cfgMarketplace.marketplaceEventsApiLre
        }

        return clearConfig(cfg)
    }

    playground.initSelectModes = selectElement => {
        Object.keys(playground.envMap).forEach(key => {
            let option = document.createElement('option')
            option.textContent = playground.envMap[key].label
            option.value = key
            selectElement.options.add(option)
        })
    }
})();
