ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 5,
 "startAdsBeforeClip": 3,
 "loopPlaylist": true,
 "startWithSound": true,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 7150,
 "accountId": 1238,
 "publisherDomainId": 1966,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "includeKeywordsLabels": true,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": true,
 "locked": false,
 "pauseClipNotInView": false,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "salesforceId": "a0N6M00000TwGp8UAF",
 "allFeeds": false,
 "account": {
  "salesforceId": "0016900002oyvh5AAA"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiIxOTU5Iiwid2lkIjoiMDAxNjkwMDAwMm95dmg1QUFBX003MTUwIiwidXJsIjpbIm9hdHRyYXZlbC5jb20iLCJ3d3cudzNzY2hvb2xzLmNvbSJdLCJleHBpcnkiOiIyMDIzLTEyLTE1VDE0OjU3OjAzLjYwNloiLCJpYXQiOjE2NzExMTYyMjN9.MZqj1Zd8fbrG04E5PeP5I4TDDdlFNaYhLhtE-eE1RBI",
 "fallbackPlaylist": [
  {
   "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254639627_960x540_video.mp4",
   "title": "Black Screen",
   "image": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254635463_248x140_thumbnail.jpg",
   "mediaid": "obivgrbzjfguemkbkrew6nbzje2xm3cf",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254639184_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254639381_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254639438_852x480_video.mp4"
    },
    {
     "width": 960,
     "height": 540,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254639627_960x540_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254635463_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254635533_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254635574_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254635608_852x480_thumbnail.jpg"
    },
    {
     "width": 960,
     "height": 540,
     "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/1666254635712_960x540_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/pQSD9IMB1ATIo49I5vlE/hls_1666254638956/master.m3u8"
   }
  }
 ],
 "playerPlaceholderSelector": [
  ".productPageSlider "
 ],
 "auto-play-desktop": "no-autoplay",
 "auto-play-mobile": "no-autoplay",
 "publisherId": "1959",
 "sfAccountId": "0016900002oyvh5AAA",
 "widgetId": "0016900002oyvh5AAA_M7150",
 "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=bMGz2H4BGT3pD1fjEp0L&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=bMGz2H4BGT3pD1fjEp0L&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=bMGz2H4BGT3pD1fjEp0L&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=bMGz2H4BGT3pD1fjEp0L&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "lre_useAdManager": "off",
 "lre_useAdManagerForMobile": "off",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": false,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 30000,
  "inviewRetryAdInterval": 4000,
  "notInviewRetryAdInterval": 4000,
  "imaAdRequestTimeout": 24000,
  "imaAdRequestTimeout_btf": 24000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 500,
  "notInviewSmartAdInterval": 13000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": false,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 30000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 14000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": ".ac-logobrand-image{ display: none;}/* player color*/.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress{background-color: #c74543 !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-volume-level {background-color: #c74543 !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-progress-control .vjs-progress-holder .vjs-mouse-display .vjs-time-tooltip {background-color: #000 !important;box-shadow: inset -2px -2px 3px 0px #303030 !important;}.w0016900002oyvh5AAA_M7073.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-control.lre-next-up,.w0016900002oyvh5AAA_M7073.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block{ display : none;}.lre-title-text-content{display: none !important;}.ac-player-wrapper .ac-lre-player .video-js .vjs-control.lre-next-up{display: none !important;}.acw .vjs-slider-horizontal .vjs-volume-level, .acw .w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-slider{     height: 8px !important;}.video-js .vjs-play-progress:before {    display : none;}.video-js .vjs-time-tooltip,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-time-control {    font-size: 22px !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-mute-control.vjs-vol-3 .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-control .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-theater-control .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-fullscreen-control .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js.vjs-fullscreen .vjs-fullscreen-control .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-mute-control.vjs-vol-2 .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-mute-control.vjs-vol-0 .vjs-icon-placeholder:before,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-mute-control.vjs-vol-1 .vjs-icon-placeholder:before{ height: 25px !important;}.video-js .vjs-volume-level:before {    font-size: 1.5em !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-control {    height: 21px !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-control-bar {    padding-bottom: 80px !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .vjs-theater-exit-control{font-size: 28px !important;font-family: 'Open Sans' !important;text-shadow: 0px 2px 5px #000000 !important;font-weight: 700 !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-theater-exit-control .vjs-theater-exit-icon{ background-image: url(\"https://enterprise.anyclip.com/logos/arrow.png\") !important;    height: 36px !important;}.acw .w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-progress-control .vjs-slider:hover {  height: 25px !important;}.ac-reset{ z-index: 121 !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-gradient-to-top.lre-gradient-to-top,.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-gradient-to-bottom.lre-gradient-to-bottom{ height: 0 !important;}.w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .vjs-theater-share-control .vjs-theater-share-title{ margin-top: -57px !important;font-family: 'Open Sans' !important;}.acw .w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js.vjs-ended .vjs-icon-replay-btn .vjs-icon-placeholder:before{ display: none;}.ac-lre-player .vjs-poster {    position: absolute;    background-size: cover;}.vjs-loading-spinner {\tdisplay: none !important;}.acw .w0016900002oyvh5AAA_M7150.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-text-track-display>div>div>div{font-size : 40px !important;}",
 "playAdsLoopComplete": true,
// "xRayCampaignsEnabled": false,
 "xRayCampaignsEnabled": true,
 "lumxDesktop": {
//  "enabled": false,
  "enabled": true,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
//  "enabled": false,
  "enabled": true,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
//  "enabled": false,
  "enabled": true,
  "animationInterval": 1000
 },
 "campaigns": {
//  "enabled": false,
  "enabled": true,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": true,
  "timelineIndicationEnabled": true
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": false,
  "speedPane": false
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "https://www.oattravel.com/",
 "socialIconsDesktop": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-right"
 },
 "socialIconsMobile": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-right"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": 1,
 "fullScreenButton": true,
 "theaterModeButton": true,
 "clipTitleUnderPlayer": false,
 "lre_playerLogo": {
  "enabled": false
 },
 "playInTheaterModeOnly": true,
 "customTheaterModeExit": "Back to Main Page",
 "playerVisibilityThreshold": 1,
 "labelCategory": "Single Line Overlay",
 "openShareDialog": false,
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};