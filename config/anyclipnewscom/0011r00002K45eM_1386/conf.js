ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 1,
 "startAdsBeforeClip": 1,
 "loopPlaylist": true,
// "startWithSound": true,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 2215,
 "publisherDomainId": 426,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": true,
 "videosFromLastDays": 120,
 "overrideCacheRefresh": true,
 "locked": false,
 "pauseClipNotInView": false,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": true,
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJhbnljbGlwbmV3c2NvbSIsIndpZCI6IjAwMTFyMDAwMDJLNDVlTV8xMzg2IiwidXJsIjpbImFueWNsaXAuY29tIiwiZGVtby5hbnljbGlwLmNvbSIsImRlbW8uYW55Y2xpcC5jb20vIyIsIm5ld3MuYW55Y2xpcC5jb20iXSwiZXhwaXJ5IjoiMjAyMy0wOC0wNFQxNDowMzozMy40MjlaIiwiaWF0IjoxNjU5NjIxODEzfQ.18zgnH2vUuKUJJbXS2Ig2NHvkiOCSEGN-W3hJGbxmUg",
 "playerPlaceholderSelector": [
  "#ac-lre-player"
 ],
 "auto-play-desktop": "autoplay",
 "auto-play-mobile": "autoplay",
 "publisherId": "anyclipnewscom",
 "sfAccountId": "0011r00002K45eMAAR",
 "widgetId": "0011r00002K45eM_1386",
 "adTag_https": "https://vid.springserve.com/vast/anyclipVH5873111?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "adTag_Mobile_https": "https://vid.springserve.com/vast/anyclipVH5873111?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "adTag_https_btf": "https://vid.springserve.com/vast/anyclipVH5873111?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "adTag_Mobile_https_btf": "https://vid.springserve.com/vast/anyclipVH5873111?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "lre_useAdManager": "off",
 "lre_useAdManagerForMobile": "off",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": false,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 25000,
  "notInviewAdInterval": 10000,
  "inviewRetryAdInterval": 10000,
  "notInviewRetryAdInterval": 10000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 0,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 0,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": false,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 25000,
  "notInviewAdInterval": 1000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 0,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 0,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "always",
  "floatingPosition": "top-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": -1,
  "floatingMargin": 0,
  "floatingTheme": "page-width",
  "visibilityTogglePercents": 100
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": ".ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-custom-control-spacer {    visibility: hidden;}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": true,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": false,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": false,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 5000
 },
 "campaigns": {
  "enabled": false,
  "text": "Shop",
  "url": "https://www.amazon.com/gp/search?ie=UTF8&tag=anyclip-20&linkCode=ur2&linkId=91b2e3bf057f10ede488c1b6404c52e6&camp=1789&creative=9325&index=aps&keywords=$[name]"
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": true,
  "floatingModeToggle": true
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": true,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": true,
  "timelineIndicationEnabled": true
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "playerPreset": 3,
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};