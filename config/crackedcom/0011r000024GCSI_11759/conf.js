ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 9,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 4860,
 "publisherDomainId": 884,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJjcmFja2VkY29tIiwid2lkIjoiMDAxMXIwMDAwMjRHQ1NJXzExNzU5IiwidXJsIjpbImNoZWV6YnVyZ2VyLmNvbSIsImNyYWNrZWQuY29tIiwiZWJhdW1zd29ybGQuY29tIiwia25vd3lvdXJtZW1lLmNvbSIsInN0YWdpbmcuY3JhY2tlZC5jb20iLCJ3d3cuY2hlZXpidXJnZXIuY29tIl0sImV4cGlyeSI6IjIwMjMtMDgtMDdUMTM6Mzg6MDEuODI5WiIsImlhdCI6MTY1OTg3OTQ4MX0.ERzp9l0vogWzw0mdF46EmAcZUXPdj_NItNPvdKVQ2oY",
 "playerPlaceholderSelector": [
  "#outstream",
  "#ac-lre=player-ph",
  "#entry-1 > p:nth-child(4)",
  "#content-wrapper > div.page-wrapper.article > section > div.left > p:nth-child(6)",
  "#content-wrapper > div.page-wrapper.article > section > div.left > h3",
  "#content-wrapper > div.page-wrapper.article > section > div.left "
 ],
 "fallbackPlaylist": [
  {
    "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144143185_1280x720_video.mp4",
    "hlsFile": {
      "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/hls_1659144127199/master.m3u8"
    },
    "image": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062820_248x140_thumbnail.jpg",
    "title": "Why You'll Never Buy A House | Honest Ads",
    "mediaid": "krctm52ujfeuetsulezwe4jwgzgecmdk",
    "plot": "What if Real Estate Companies and Realtors were actually honest about how horrifically terrible they are? Roger Horton investigates.\n\nSUBSCRIBE HERE: http://goo.gl/ITTCPW\n\nJoin us Mondays, Thursdays, Fridays, and Sundays at 1pm EST for brand-new Cracked shows and series!\n\nCLICK HERE for more HONEST ADS: https://youtu.be/gmQE4qdb9fg\nCLICK HERE for more YOUR BRAIN ON CRACKED: https://youtu.be/mjWqtWntljg\nCLICK HERE for WE REMADE ZACK SNYDER’S JUSTICE LEAGUE FOR ONLY $20: https://youtu.be/Q_JeapnFFy4\nCLICK HERE for more LONG STORY SHORT(ish): https://youtu.be/iCPdhuxmNT4\nCLICK HERE for more CANONBALL: https://youtu.be/tclMdfFcxmQ\n\nGet more of this in the One Cracked Fact newsletter at https://cracked.com/newsletters!\n\nSOURCES:\nhttps://www.washingtonpost.com/news/made-by-history/wp/2017/07/20/the-false-promise-of-homeownership/?noredirect=on\nhttps://www.cracked.com/article_27207_the-american-housing-market-totally-broken-hooray.html\nhttps://www.cracked.com/personal-experiences-2440-we-target-poor-i-foreclose-peoples-homes-living.html\nhttps://learn.roofstock.com/blog/real-estate-facts\nhttps://www.usatoday.com/story/money/2021/06/11/millennials-facing-financial-and-physical-regrets-after-buying-homes/7594826002/\nhttps://www.nerdwallet.com/article/mortgages/the-20-mortgage-down-payment-is-dead\nhttp://www.homebuyinginstitute.com/news/starter-homes-are-hard-to-find/ \n\nRoger Horton: Jack Hunter\nBuyers: Jordan Breeding, Anna Breeding\nWriters: Ryan Menezes, Jordan Breeding\nDirector: Jordan Breeding\nDirector of Photography: Dave Brown\nEditor: Jordan Breeding\nSound: Mike Schoen \nProduction Assistant: Eli Hall\n\nRyan's Twitter: https://twitter.com/MenezesCracked\nJordan’s Twitter: https://twitter.com/The_J_Breeding\nJordan’s Writing Portfolio: https://thejordanbreedingblog.wordpress.com/2017/03/22/portfolio/\nDave’s Instagram: https://www.instagram.com/deforestbrown/\n\n00:00 - If Home Buying Were Honest\n\n#realestate #homebuyers #realtor",
    "score": 0.4156472679195796,
    "landingPageLink": "https://www.youtube.com/watch?v=Y-i7cawhw8s",
    "contentOwner": 1658,
    "publisherLink": "https://www.youtube.com/watch?v=Y-i7cawhw8s",
    "files": [
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144141493_480x270_video.mp4",
        "width": 480,
        "height": 270
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144142050_640x360_video.mp4",
        "width": 640,
        "height": 360
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144142550_852x480_video.mp4",
        "width": 852,
        "height": 480
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144143185_1280x720_video.mp4",
        "width": 1280,
        "height": 720
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144143988_1920x1080_video.mp4",
        "width": 1920,
        "height": 1080
      }
    ],
    "created": 1659114031000,
    "duration": 539191,
    "images": [
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062820_248x140_thumbnail.jpg",
        "width": 248,
        "height": 140
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062758_480x270_thumbnail.jpg",
        "width": 480,
        "height": 270
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062655_640x360_thumbnail.jpg",
        "width": 640,
        "height": 360
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062688_852x480_thumbnail.jpg",
        "width": 852,
        "height": 480
      },
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062488_1280x720_thumbnail.jpg",
        "width": 1280,
        "height": 720
      }
    ],
    "lang": [
      "EN"
    ],
    "performanceData": {
      "time": 1659952800000,
      "parameters": [
        {
          "type": "VIEW",
          "value": 103452
        },
        {
          "type": "LIKE",
          "value": 12
        },
        {
          "type": "SHARE",
          "value": 0
        }
      ]
    },
    "previewUrl": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/1659144062959_preview.mp4",
    "defaultOrder": 1,
    "videoId": "TE6wTIIBNTY3bq66LA0j",
    "ccFiles": [
      {
        "file": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/cc/EN/1659144408005_subtitles.vtt",
        "lang": "EN",
        "langName": "English"
      }
    ],
    "access": {
      "level": "SITE",
      "sites": [
        "1355",
        "1176"
      ]
    },
    "ccUrl": "https://cdn5.anyclip.com/TE6wTIIBNTY3bq66LA0j/cc/EN/1659144408005_subtitles.vtt"
  }
],
 "auto-play-desktop": "autoplay-inview",
 "auto-play-mobile": "autoplay-inview",
 "publisherId": "crackedcom",
 "sfAccountId": "0011r000024GCSIAA4",
 "widgetId": "0011r000024GCSI_11759",
 "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=elgkPn4B5vSrw48_lRwZ&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=e1gkPn4B5vSrw48_5xyx&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=elgkPn4B5vSrw48_lRwZ&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=e1gkPn4B5vSrw48_5xyx&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "lre_useAdManager": "ac",//"ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": false,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 15000,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 4000,
  "notInviewRetryAdInterval": 4000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 500,
  "notInviewSmartAdInterval": 13000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": false,
  "apsPubId": "e6bf2815-2b1a-4048-983c-ed44b725538d",
  "apsSlotId": "AnyClip_Desktop",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": false,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 15000,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 18000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": false,
  "apsPubId": "e6bf2815-2b1a-4048-983c-ed44b725538d",
  "apsSlotId": "AnyClip_Desktop",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": true,
  "leftText": "Recommended videos",
  "rightText": "Powered by <a href='https://anyclip.com/?source=right_branding' class='any-clip-brand' target='_blank'>AnyClip</a>"
 },
 "floatingDesktop": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 415,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "isResponsive": true
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "bar"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": true,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "vertical"
 },
 "customStyle": ".ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player .lre-cancel-float {    background-image: url(https://anyclip-player.s3.amazonaws.com/anyclip-widget/lre-widget/assets/lre_theme/venturebeat-close-btn.svg);}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player{height: 233px !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player, .ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-force-float { z-index: 99999 }.ac-context-menu.ac-floated-player { z-index: 99999 }.ac-lre-wrapper.luminous-theme .ac-player-playlist .purejscarousel-slides-container .lre-playlist-item.purejscarousel-slide .lre-playlist-item-link .lre-playlist-thumbnail-wrapper p {    height: 30px  !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-control {    padding-bottom: unset !important;}.ac-lre-wrapper.luminous-theme .ac-branding-right-text{   display: none;}.ac-lre-wrapper.luminous-theme .ac-branding-left-text{   display: none;}#ac-lre-wrapper.luminous-theme #ac-player-wrapper.ac-floated-player {    bottom: 112px!important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js img.ac-logobrand-image{height: 40px !important;}.ac-lre-ima-ad-wrapper * { position: initial }.ac-logobrand-image,.vjs-control-bar{ cursor: pointer !important;}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 0
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "luma",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": false
 },
 "playerPreset": 1,
 "lre_playerLogo": {
  "enabled": "true",
  "file": "https://enterprise.anyclip.com/logos/logos/CRD.png",
  "link": "https://www.cracked.com"
 },
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{
        name : "Load CatapultX",
        condition : function() {
            var script = document.createElement('script');
            script.src = "https://tags.catapultx.com/bootstrapper?group-id=S9ihDXBQ0mrwBWVE9N8cw&video-container=ac-lre-player";
            script.defer = true;
            document.getElementsByTagName('head')[0].appendChild(script);

                  var detectWidgetTimeout = null;
        function detectWidget() {
            clearTimeout(detectWidgetTimeout);
            var widget = anyclip.getWidget();
            if (widget && widget.subscribe) {
                // Do subscribe
                widget.subscribe(
                    function() {
                        // Stop CatapultX
                        document.querySelector('cx-overlay').setAttribute('cx-wait', 'true');
                    },
                    'adImpression',
                    null,
                    1
                );
                widget.subscribe(
                    function() {
                        // Resume CatapultX
                        document.querySelector('cx-overlay').setAttribute('cx-wait', 'false');
                    },
                    'adComplete',
                    null,
                    1
                );
                widget.subscribe(
                    function() {
                        // Resume CatapultX
                        document.querySelector('cx-overlay').setAttribute('cx-wait', 'false');
                    },
                    'adSkipped',
                    null,
                    1
                );
                widget.subscribe(
                    function() {
                        // Resume CatapultX
                        document.querySelector('cx-overlay').setAttribute('cx-wait', 'false');
                    },
                    'adAborted',
                    null,
                    1
                );
                widget.subscribe(
                    function() {
                        // Resume CatapultX
                        document.querySelector('cx-overlay').setAttribute('cx-wait', 'false');
                    },
                    'adError',
                    null,
                    1
                );

                // CatapultX events
                window.addEventListener('cx-impression', function(event) {
                    // Publish event displayAdImpression
                    widget.publish('displayAdImpression', event.detail.bid);
                });

            } else {
                detectWidgetTimeout = setTimeout(detectWidget, 1);
            }
        }
        detectWidget();
        return true;
    }
}],
"customMacros": {}
};