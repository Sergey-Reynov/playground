ac_lre_conf = {
 "playerWidth": 375,
 "playlistLimit": 7,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 2873,
 "accountId": 629,
 "publisherDomainId": 289,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "includeKeywordsLabels": false,
 "enableVideoTargeting": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": true,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "account": {
  "salesforceId": "001w000001KVCMuAAP"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJmYXN0Y29tcGFueWNvbSIsIndpZCI6IjAwMXcwMDAwMDFLVkNNdV8xNjc3IiwidXJsIjpbInd3dy5mYXN0Y29tcGFueS5jb20iXSwiZXhwaXJ5IjoiMjAyNC0wMS0zMFQxMTozNDo1Ny4wODlaIiwiaWF0IjoxNjc1MDc4NDk3fQ.7IjW_WRIofzQSO9KMtwzLw_Oj4cWfxC7QWVhyVPSbYY",
 "fallbackPlaylist": [
  {
   "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669711285_1280x720_video.mp4",
   "title": "Meet the Undergrad Founders Supporting Local Startups During Covid-19",
   "image": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693177_248x140_thumbnail.jpg",
   "mediaid": "ifme6rsvofzgcvcbnn4wuttqgvehsrdj",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669708287_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669708813_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669709481_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669711285_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669712798_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693177_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693243_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693269_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693292_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693320_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/1595669693425_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXOFUqraTAkyjNp5HyDi/hls_1595669737397/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496944953_1280x720_video.mp4",
   "title": "LL Cool J On Building a Brand, Hip-Hop Culture, and Pitching Mark Cuban",
   "image": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496889905_248x140_thumbnail.jpg",
   "mediaid": "ifme4n2cmvdgm6rznrju2obrje4daq2y",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496942506_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496943132_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496943901_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496944953_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496946706_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496889905_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496889955_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496890005_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496890158_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496890203_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/1595496890256_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXN7BeFfz9lSM81I80CX/hls_1595496902280/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978625085_1280x720_video.mp4",
   "title": "WordPress Co-Founder on the Myth of Where to Find Top Talent",
   "image": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978572136_248x140_thumbnail.jpg",
   "mediaid": "ifme4y2igzeva32fnbquu3dqha3xux3h",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978623848_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978624181_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978624550_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978625085_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978572136_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978572174_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978572226_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978572318_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/1594978572354_1280x720_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXNcH6IPoEhaJlp87z_g/hls_1594978638997/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460091618_1280x720_video.mp4",
   "title": "How 3 Innovative Startups Pivoted to Meet Covid-19 Demands",
   "image": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078424_248x140_thumbnail.jpg",
   "mediaid": "ifme2okplfcum6snlbrwmm3horkeqtcc",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460089846_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460090317_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460090838_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460091618_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460092899_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078424_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078485_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078533_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078639_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078692_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/1594460078716_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXM9OYEFzMXcf3gtTHLB/hls_1594460122025/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389778556_1280x720_video.mp4",
   "title": "How to Make Smart Decisions in Uncertain Times",
   "image": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389768863_248x140_thumbnail.jpg",
   "mediaid": "ifme2nkdjjpuqzdugnwxovdkirre6x3o",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389776685_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389777156_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389777724_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389778556_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389779862_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389768863_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389768917_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389769120_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389769199_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389769254_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/1594389769318_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXM5CJ_Hdt3mwTjDbO_n/hls_1594389783474/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393243976_1280x720_video.mp4",
   "title": "8 Inspirational Quotes From Iconic American Entrepreneurs",
   "image": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235363_248x140_thumbnail.jpg",
   "mediaid": "ifme2nkqlb3hgzdugnwxovdkirrfgll2",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393241997_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393242472_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393243109_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393243976_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393245496_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235363_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235502_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235591_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235618_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235680_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/1594393235737_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXM5PXvsdt3mwTjDbS-z/hls_1594393248377/master.m3u8"
   }
  }
 ],
 "playerPlaceholderSelector": [
  "#inc-anyclip"
 ],
 "auto-play-desktop": "autoplay-inview",
 "auto-play-mobile": "autoplay-inview",
 "publisherId": "fastcompanycom",
 "sfAccountId": "001w000001KVCMuAAP",
 "widgetId": "001w000001KVCMu_1677",
 "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=lFimaH4B5vSrw48_n4xo&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=Dij4J30BpdYRyxZd4_99&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=lFimaH4B5vSrw48_n4xo&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=Dij4J30BpdYRyxZd4_99&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "lre_useAdManager": "ac",
 "lre_useAdManagerForMobile": "ac",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 4000,
  "notInviewRetryAdInterval": 4000,
  "imaAdRequestTimeout": 24000,
  "imaAdRequestTimeout_btf": 24000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 500,
  "notInviewSmartAdInterval": 13000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Sites",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 18000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Sites",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": true,
  "leftText": "Article continues after video.",
  "rightText": "Featured Video"
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "classic"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": ".ac-logobrand-image-destination {  display: none;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper.ac-floated-player.ac-floated-page-width-theme {  top: 0 !important;  bottom: unset !important;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .lre-vjs-video-title-block {  display: flex;  position: absolute;  top: -32px;  left: 0px;  color: #999999;  font-size: 12px;  font-family: le-monde-livre-std;  right: 0%;}.ac-lre-player .video-js.lx-size-0 .lre-vjs-video-title-block,.ac-lre-player .video-js.lx-size-1 .lre-vjs-video-title-block {  max-width: 100% !important;}.ac-lre-wrapper.luminous-theme .outer-title-container {  position: absolute;  top: 92px;  left: 0;}.ac-lre-wrapper.luminous-theme  .outer-title-container  .outer-title-container-text {  font-size: 16px !important;  width: 90%;  font-weight: normal !important;  font-family: le-monde-livre-std, serif;  color: #474747;  line-height: normal;}.brand-container  + .ac-player-ph  .ac-player-wrapper:not(.ac-floated-player)  .ac-lre-player {  padding-top: 50px;  font-size: 10px;  font-family: IBM-Plex-Mono !important;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .lre-vjs-video-title-block  .lre-title-text,.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .lre-vjs-video-title-block  .lre-title-text-content {  overflow: hidden;  text-overflow: ellipsis;  white-space: nowrap;  width: 100%;  margin-top: -5px;  padding-left: 0px;  padding-right: 10%;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .lre-vjs-video-title-block  .lre-title-text-content {  line-height: 40px !important;}.brand-container .ac-branding-left-text:not(#fake) {  width: 100%;  text-align: left;  font-family: le-monde-livre-std, serif !important;  font-style: italic;  color: #474747 !important;  font-size: 20px;  line-height: 2;  border-bottom: solid;  padding-bottom: 10px;  border-color: #8080804d;  border-width: 2px;}.brand-container .ac-branding-right-text:not(#fake) {  color: rgb(0, 0, 0);  display: block;  font-family: ibm-plex-mono !important;  font-size: 10px;  font-weight: bold;  letter-spacing: 2px;  line-height: 1.5;  text-transform: uppercase;  text-decoration: none;  transition: all 150ms ease-out 0s;  margin-top: 25px;  margin-bottom: 50px;  text-align: left;}.ac-lre-ad-bar {  background-color: #999;}.ac-lre-player-ph {  text-shadow: none;  border-bottom: solid;  padding-bottom: 50px;  border-color: #8080804d;  border-width: 2px;}.ac-reset {  width: auto !important;}.video-js.vjs-user-inactive.vjs-playing .vjs-text-track-display:not(#fake) {  bottom: 0;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .vjs-text-track-display  > div  > div  > div {  border-radius: 30px;  font-size: 14px !important;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .vjs-play-progress {  background-color: white;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .vjs-slider {  background-color: white;}.acw .ac-lre-ad-bar {  z-index: 9;}.ac-lre-player .video-js.lx-size-0 .lre-vjs-video-title-block,.ac-lre-player .video-js.lx-size-1 .lre-vjs-video-title-block {  max-width: 100% !important;}.w001w000001KVCMu_1677.luminous-theme .ac-player-wrapper.ac-floated-player {  top: 77px !important;  width: 100% !important;  left: 0 !important;  margin-top: 0px !important;}.acw .ac-context-menu-link {  padding: 0px 4px !important;}.article-body ul li:before {  display: none;}.wDWlK a {  border-bottom: 0 !important;}.acw  .w001w000001KVCMu_1677.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .lre-vjs-video-title-block  .lre-title-text-content {  width: 90% !important;  line-height: 50px !important;}.w001w000001KVCMu_1677.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .vjs-text-track-display  > div  > div  > div {  top: 0 !important;}.acw .w001w000001KVCMu_1677.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block{     top: 5px !important;}.acw .h001w000001KVCMu_1677 .w001w000001KVCMu_1677.luminous-theme.allow-outer-title .ac-lre-player [id*='ac-lre-vjs']:not(.vjs-fullscreen) .ac-lre-social-action-items{ top: 8px !important;}.acw .h001w000001KVCMu_1677 .w001w000001KVCMu_1677.luminous-theme .ac-player-ph .ac-player-wrapper .ac-lre-player [id*=ac-lre-vjs] .ac-lre-social-action-items{ top: 33px !important;}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 1000
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": false,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": true,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "visible"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": 1,
 "fullScreenButton": true,
 "theaterModeButton": false,
 "clipTitleUnderPlayer": true,
 "maxClicksAdImpression": 1,
 "maxClicksSession": 1,
 "ab_test": {
  "enabled": false,
  "variants": [
   {
    "variantName": "fastcompanycom|max_clicks_0|A|50",
    "percentage": 50
   },
   {
    "variantName": "fastcompanycom|max_clicks_0|B|0",
    "percentage": 0,
    "conf": {
     "id": 6568,
     "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=lFimaH4B5vSrw48_n4xo&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid-rn=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]",
     "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=Dij4J30BpdYRyxZd4_99&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid-rn=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]",
     "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=lFimaH4B5vSrw48_n4xo&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid-rn=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]",
     "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=Dij4J30BpdYRyxZd4_99&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid-rn=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]",
     "lre_imaAds": {
      "inviewRetryAdInterval": 5000,
      "notInviewRetryAdInterval": 5000,
      "inviewSmartAdInterval": 1000,
      "notInviewSmartAdInterval": 14000
     },
     "customStyle": ".ac-logobrand-image-destination{   display: none;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player {    top: 0px !important;    left: 0px !important;    margin: 0px !important;    width: 100% !important;    height: 100% !important;    background-color: transparent !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper {    margin-left: -0px;    margin-top: 100px;    margin-right: 67px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block {    display: flex;    position: absolute;    top: -32px;    left: 0px;    color: #999999;    font-size: 12px;    font-family: le-monde-livre-std;    right: 0%;}.ac-lre-player .video-js.lx-size-0 .lre-vjs-video-title-block, .ac-lre-player .video-js.lx-size-1 .lre-vjs-video-title-block {    max-width: 100% !important;}.ac-lre-wrapper.luminous-theme .outer-title-container .outer-title-container-text {    font-size: 16px !important;    width: 90%;    line-height: 1.5 !important;    line-spacing: 20px !important;    font-weight: normal !important;    font-family: le-monde-livre-std,serif;    color: #474747;    margin-top: -287px!important;}.brand-container+.ac-player-ph .ac-player-wrapper:not(.ac-floated-player) .ac-lre-player {    padding-top: 50px;    font-size: 10px;    font-family: IBM-Plex-Mono!important!}.brand-container .ac-branding-left-text {    width: 100% !important;    text-align: left !important;    font-family: \"le-monde-livre-std\",serif !important;    font-style: italic !important;    color: #474747!important;    font-size: 20px !important;    line-height: 2 !important;    border-bottom: solid !important;    padding-bottom: 30px !important;    border-color: #8080804d !important;    border-width: 2px !important;    }.brand-container .ac-branding-right-text {   color: rgb(0, 0, 0) !important;    display: block !important;    font-family: ibm-plex-mono !important;    font-size: 10px !important;    font-weight: bold !important;    letter-spacing: 2px !important;    line-height: 1.5 !important;    text-transform: uppercase !important;    text-decoration: none !important;    transition: all 150ms ease-out 0s !important;    margin-top: 50px !important;    text-align: left !important;}.ac-lre-ad-bar {    background-color: #999;  }.ac-lre-player-ph {    text-shadow: none;    border-bottom: solid;    padding-bottom: 50px;    border-color: #8080804d;    border-width: 2px;}.ac-reset {    width: auto!important;}.brand-container+.ac-player-ph .ac-player-wrapper:not(.ac-floated-player) .ac-lre-player {    padding-top: 15px;    width: 325px;}.ac-lre-wrapper.luminous-theme .outer-title-container .outer-title-container-text {    font-size: 18px;    line-height: 1.5;    margin-top: -287px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-text-track-display>div>div>div {border-radius: 30px;font-size: 14px !important;margin-bottom: 20px !important;top: 45px !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress {    background-color: white;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress {    background-color: white;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-slider      background-color: white;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player .ac-lre-vjs-F0834BhiD5EDcH4cNgLWhXQUYByQmP1S-dimensions {    height: 280px!important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player{overflow: visible !important;}.ac-player-wrapper.ac-floated-player .ac-lre-player {    top: 81px !important;}/*new css*/.acw .ac-lre-ad-bar{z-index: 9;}.acw .ac-context-menu-link{padding: 0px 4px !important;}.article-body ul li:before{ display: none;}.wDWlK a{ border-bottom: 0 !important;}.acw .w001w000001KVCMu_1677.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block .lre-title-text-content{     width: 90%!important;    line-height: 50px !important;}.w001w000001KVCMu_1677.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-text-track-display>div>div>div{ top: 0 !important;}"
    }
   },
   {
    "variantName": "fastcompanycom|max_clicks_0|C|50",
    "percentage": 50,
    "conf": {
     "id": 7227,
     "customStyle": ".ac-logobrand-image-destination{   display: none;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player {    top: 0px !important;    left: 0px !important;    margin: 0px !important;    width: 100% !important;    height: 100% !important;    background-color: transparent !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper {    margin-left: -0px;    margin-top: 100px;    margin-right: 67px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block {    display: flex;    position: absolute;    top: -32px;    left: 0px;    color: #999999;    font-size: 12px;    font-family: le-monde-livre-std;    right: 0%;}.ac-lre-player .video-js.lx-size-0 .lre-vjs-video-title-block, .ac-lre-player .video-js.lx-size-1 .lre-vjs-video-title-block {    max-width: 100% !important;}.ac-lre-wrapper.luminous-theme .outer-title-container .outer-title-container-text {    font-size: 16px !important;    width: 90%;    line-height: 1.5 !important;    line-spacing: 20px !important;    font-weight: normal !important;    font-family: le-monde-livre-std,serif;    color: #474747;    margin-top: -287px!important;}.brand-container+.ac-player-ph .ac-player-wrapper:not(.ac-floated-player) .ac-lre-player {    padding-top: 50px;    font-size: 10px;    font-family: IBM-Plex-Mono!important!}.brand-container .ac-branding-left-text {    width: 100% !important;    text-align: left !important;    font-family: \"le-monde-livre-std\",serif !important;    font-style: italic !important;    color: #474747!important;    font-size: 20px !important;    line-height: 2 !important;    border-bottom: solid !important;    padding-bottom: 30px !important;    border-color: #8080804d !important;    border-width: 2px !important;    }.brand-container .ac-branding-right-text {   color: rgb(0, 0, 0) !important;    display: block !important;    font-family: ibm-plex-mono !important;    font-size: 10px !important;    font-weight: bold !important;    letter-spacing: 2px !important;    line-height: 1.5 !important;    text-transform: uppercase !important;    text-decoration: none !important;    transition: all 150ms ease-out 0s !important;    margin-top: 50px !important;    text-align: left !important;}.ac-lre-ad-bar {    background-color: #999;  }.ac-lre-player-ph {    text-shadow: none;    border-bottom: solid;    padding-bottom: 50px;    border-color: #8080804d;    border-width: 2px;}.ac-reset {    width: auto!important;}.brand-container+.ac-player-ph .ac-player-wrapper:not(.ac-floated-player) .ac-lre-player {    padding-top: 15px;    width: 325px;}.ac-lre-wrapper.luminous-theme .outer-title-container .outer-title-container-text {    font-size: 18px;    line-height: 1.5;    margin-top: -287px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-text-track-display>div>div>div {border-radius: 30px;font-size: 14px !important;margin-bottom: 20px !important;top: 45px !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress {    background-color: white;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress {    background-color: white;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-slider      background-color: white;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player .ac-lre-vjs-F0834BhiD5EDcH4cNgLWhXQUYByQmP1S-dimensions {    height: 280px!important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player{overflow: visible !important;}.ac-player-wrapper.ac-floated-player .ac-lre-player {    top: 81px !important;}/*new css*/.acw .ac-lre-ad-bar{z-index: 9;}.acw .ac-context-menu-link{padding: 0px 4px !important;}.article-body ul li:before{ display: none;}.wDWlK a{ border-bottom: 0 !important;}.acw .w001w000001KVCMu_1677.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block .lre-title-text-content{     width: 90%!important;    line-height: 50px !important;}.w001w000001KVCMu_1677.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-text-track-display>div>div>div{ top: 0 !important;}",
     "maxClicksAdImpression": 2,
     "maxClicksSession": 2
    }
   }
  ]
 },
"conditionalConf": [{}],
"customMacros": {}
};