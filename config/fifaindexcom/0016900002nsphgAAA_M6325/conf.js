ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 1,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 6325,
 "accountId": 1178,
 "publisherDomainId": 1444,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 400,
 "preloadPosters": false,
 "includeKeywordsLabels": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "salesforceId": "a0N6900000hs3HIEAY",
 "allFeeds": false,
 "account": {
  "salesforceId": "0016900002nsphgAAA"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJmaWZhaW5kZXhjb20iLCJ3aWQiOiIwMDE2OTAwMDAybnNwaGdBQUFfTTYzMjUiLCJ1cmwiOlsiZmlmYWluZGV4LmNvbSIsIm11dC5nZyJdLCJleHBpcnkiOiIyMDIzLTA4LTIyVDExOjM5OjIzLjI5OFoiLCJpYXQiOjE2NjExNjgzNjN9.sxCWEJTIlHHDmjBB0ryaiI2W1kWtqwQIUj_g_eVPuo0",
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay-inview",
 "auto-play-mobile": "no-autoplay",
 "publisherId": "fifaindexcom",
 "sfAccountId": "0016900002nsphgAAA",
 "widgetId": "0016900002nsphgAAA_M6325",
 "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=w9fOsoABdAdaDOL-1Ajq&w=$[width]&h=$[height]&v=1&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=PnHPsoABvkyXq-6V5VJv&w=$[width]&h=$[height]&v=1&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]",
 "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=w9fOsoABdAdaDOL-1Ajq&w=$[width]&h=$[height]&v=1&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=PnHPsoABvkyXq-6V5VJv&w=$[width]&h=$[height]&v=1&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]",
 "lre_useAdManager": "ima",
 "lre_useAdManagerForMobile": "off",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 12500,
  "notInviewAdInterval": 12500,
  "inviewRetryAdInterval": 4000,
  "notInviewRetryAdInterval": 4000,
  "imaAdRequestTimeout": 24000,
  "imaAdRequestTimeout_btf": 24000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 500,
  "notInviewSmartAdInterval": 13000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Sites",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 12500,
  "notInviewAdInterval": 12500,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 14000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Sites",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": true,
  "floatingMode": "only",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 1800000,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
//  "floatingEnabled": false,
  "floatingEnabled": true,
  "floatingMode": "only",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
//  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": "#ac-lre-wrapper.luminous-theme #ac-player-wrapper.ac-floated-player {    bottom: 100px!important;}body{    overflow-x: hidden !important;}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 1000
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": true
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": false
 },
 "playerPreset": 1,
 "fullScreenButton": true,
 "theaterModeButton": false,
 "clipTitleUnderPlayer": false,
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{
    name: "Destroy when Mobile or Tablet",
    condition : function() {
        var detectWidgetTimeout = null;
        var isMobileOrTablet = false;
        (function (userAgent) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(userAgent.substr(0, 4))) {
                isMobileOrTablet = true;
            }
        })(navigator.userAgent || navigator.vendor || window.opera);
        function detectWidget() {
            clearTimeout(detectWidgetTimeout);
            var widget = anyclip.getWidget();
            if (widget && widget.destroy) {
                // Do destroy
                widget.destroy();
            } else {
                detectWidgetTimeout = setTimeout(detectWidget, 1);
            }
        }
        return isMobileOrTablet ? (detectWidget(), true) : false;
    }
}],
"customMacros": {}
};