ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 6,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 2654,
 "accountId": 605,
 "publisherDomainId": 522,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "account": {
  "salesforceId": "001w000001RrzqEAAR"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJpbnRlcm5ldGJyYW5kc2NvbSIsIndpZCI6IjAwMXcwMDAwMDFScnpxRV8xNTgwIiwidXJsIjpbImZvcmQtdHJ1Y2tzLmNvbSIsInd3dy5mb3JkLXRydWNrcy5jb20iXSwiZXhwaXJ5IjoiMjAyMy0wOC0xN1QxMTozODoxMy41ODlaIiwiaWF0IjoxNjYwNzM2MjkzfQ.d6G1zhv0Q-xYgoh0kGiJipYzid_QFnrv7aECFs_J3N4",
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay-after-ad",
 "auto-play-mobile": "autoplay-after-ad",
 "publisherId": "internetbrandscom",
 "sfAccountId": null,
 "widgetId": "001w000001RrzqE_1580",
 "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=h1gxPn4B5vSrw48_thxq&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&topic=$[topic]&sect=$[sect]",
 "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=MjY9vHwBmv5bzn5Kg1ue&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&topic=$[topic]&sect=$[sect]",
 "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=h1gxPn4B5vSrw48_thxq&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&topic=$[topic]&sect=$[sect]",
 "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=MjY9vHwBmv5bzn5Kg1ue&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&topic=$[topic]&sect=$[sect]",
 "lre_useAdManager": "ac",//"ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": false,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 6000,
  "inviewRetryAdInterval": 29000,
  "notInviewRetryAdInterval": 14000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 1000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "3081",
  "apsSlotId": "ibVideo",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": false,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 6000,
  "inviewRetryAdInterval": 30000,
  "notInviewRetryAdInterval": 15000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 1000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "3081",
  "apsSlotId": "ibVideoMobile",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": true,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 415,
  "floatingDelay": 0,
  "floatingReopenWait": 60000,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 250,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "bar"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": true,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": ".ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-custom-control-spacer{visibility:hidden;}.ac-lre-wrapper {  margin: auto;  width: 640px;}@media screen and (max-width: 1199px) {.ac-lre-wrapper {  width: auto;}}.ac-context-menu{z-index:98;}.ac-lre-ima-ad-wrapper {text-align:initial}.video-js .vjs-load-progress div {    background: #dfbbff;}.ac-context-menu.ac-floated-player.ac-context-menu-active {    height: fit-content;}@media screen and (max-width: 480px) {#ac-lre-wrapper.luminous-theme #ac-player-wrapper.ac-floated-player {    bottom: 123px!important;}}.ac-context-menu.ac-floated-player, .ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player, .ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-force-float {    z-index: 7999 !important;}@media only screen and (max-width: 1560px) {.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player {    right: 0px !important;}}.ac-context-menu-active .context-menu-items .context-menu-item .ac-context-menu-link{text-align: left;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player{bottom: 148px !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player .lre-cancel-float {    height: 22px;    width: 22px;    background-image: none;    background-size: contain;    background-repeat: no-repeat;    opacity: 1.0;    display: block;    position: absolute;    top: -33px;    right: 5px;    background: #28497C;    border-radius: 999px;    border-style: solid;    border-color: #fff;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player .lre-cancel-float:before {    -ms-transform: rotate(45deg);    -moz-transform: rotate(45deg);    -webkit-transform: rotate(45deg);    transform: rotate(45deg);    content: 'x';    position: relative;    left: 0.5px;    top: -2px;    color: #fff;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player .lre-cancel-float:after {-ms-transform: rotate(-45deg);    -moz-transform: rotate(-45deg);    -webkit-transform: rotate(-45deg);    transform: rotate(-45deg);}.ac-lre-desktop .ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-item.ac-as-item-selected label {  color: #7881ff !important;  line-height: 3em !important;  font-weight: bold !important;  width: 22em !important;  height: 3em !important;  font-size: 15px !important;  margin-bottom: 0 !important;}.ac-lre-desktop .ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-item.ac-as-item-selected:hover label {  color: white !important;}.ac-lre-desktop .ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-item label {    color: white;    margin-bottom: 0;    font-size: 15px;}.lre-title-text-content{Height: 81px !important;}.ac-lre-player-ph{z-index: 98 !important;}@supports (-webkit-touch-callout: none) {.ac-lre-wrapper.luminous-theme .ac-player-wrapper.ac-floated-player {    bottom: 128px !important;}}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 0
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true,
  "adCompleteTimeout": 1000
 },
 "adServerMobile": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true,
  "adCompleteTimeout": 1000
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": false,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "luma",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": 1,
 "ab_test": {
  "enabled": false,
  "variants": [
   {
    "variantName": "internetbrandscom|Google-3-Addpoding-Test-1|A|70",
    "percentage": 70
   },
   {
    "variantName": "internetbrandscom|Google-3-Addpoding-Test-1|B|30",
    "percentage": 30,
    "conf": {
     "id": 6975,
     "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=s9mboYIBdAdaDOL-ndne&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&topic=$[topic]&sect=$[sect]",
     "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=s9mboYIBdAdaDOL-ndne&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&topic=$[topic]&sect=$[sect]"
    }
   }
  ]
 },
"conditionalConf": [{}],
"customMacros": {    
    "gptTargeting" : function(key) {
        if(window.googletag && googletag.apiReady) {
            var data = googletag.pubads().getTargeting(key);
            return data.length != 0 ? data.toString() : null;
        } else {
            return null;
        }
    },
    "topic" : function() {
        return this.gptTargeting("topic");
    },
    "sect" : function() {
        return this.gptTargeting("sect");
    }
}
};