ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 7,
 "startAdsBeforeClip": 5,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 20009145,
 "accountId": 1,
 "publisherDomainId": 4524,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "includeKeywordsLabels": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "account": {
  "salesforceId": "001w000001flCw2AAE"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJscmVfcWFfYWxleGVpIiwid2lkIjoiMDAxdzAwMDAwMWZsQ3cyQUFFX00yMDAwOTE0NSIsInVybCI6W10sImV4cGlyeSI6IjIwMjMtMTAtMjRUMDY6MDI6MDcuNDE2WiIsImlhdCI6MTY2NjU5MTMyN30.R0EcnReVWwOEbc9XKZWM17E8hjP1RGdQNNIDwrU7U_4",
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "no-autoplay",
 "auto-play-mobile": "autoplay-inview",
 "publisherId": "lre_qa_alexei",
 "sfAccountId": "001w000001flCw2AAE",
 "widgetId": "001w000001flCw2AAE_M20009145",
 "adTag_https": "https://vid.springserve.com/vast/184920?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_Mobile_https": "https://vid.springserve.com/vast/184920?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_https_btf": "https://vid.springserve.com/vast/184920?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_Mobile_https_btf": "https://vid.springserve.com/vast/184920?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "lre_useAdManager": "ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 2,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 11000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Hubs",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 2,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 11000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Hubs",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": true,
  "leftText": "Recommended videos",
  "rightText": "Powered by <a href='https://anyclip.com/?source=right_branding' class='any-clip-brand' target='_blank'>AnyClip</a>"
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": true,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": "",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": true,
  "animationInterval": 15000
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "luma",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": 1,
 "fullScreenButton": false,
 "theaterModeButton": false,
 "clipTitleUnderPlayer": false,
 "playInTheaterModeOnly": false,
 "customTheaterModeExit": "Back to Main Page",
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};