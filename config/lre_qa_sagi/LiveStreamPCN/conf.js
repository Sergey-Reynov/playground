ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 7,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 75228,
 "accountId": 1,
 "publisherDomainId": 880,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": true,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": true,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "account": {
  "salesforceId": "001w000001flCw2AAE"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJscmVfcWFfc2FnaSIsIndpZCI6IkxpdmVTdHJlYW1QQ04iLCJ1cmwiOltdLCJleHBpcnkiOiIyMDIzLTA4LTMxVDEzOjAwOjI2LjE0M1oiLCJpYXQiOjE2NjE5NTA4MjZ9.cdyiziHSwC_UubtfmzHvilBxhPYLiUGjEZrF2EBH8o8",
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay",
 "auto-play-mobile": "autoplay",
 "publisherId": "lre_qa_sagi",
 "sfAccountId": "001w000001flCw2AAE",
 "widgetId": "LiveStreamPCN",
 "adTag_https": "https://vid.springserve.com/vast/412415?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_Mobile_https": "https://vid.springserve.com/vast/412415?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_https_btf": "https://vid.springserve.com/vast/412415?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_Mobile_https_btf": "https://vid.springserve.com/vast/412415?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "lre_useAdManager": "ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 5000,
  "notInviewAdInterval": 5000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 5000,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 11000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 5000,
  "notInviewAdInterval": 5000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 5000,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 11000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": true,
  "leftText": "",
  "rightText": "Right Branding"
 },
 "floatingDesktop": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "classic"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": "",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": true,
  "animationInterval": 15000
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": true,//false,
 "sendClipUnitsPlayed": true,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "playerPreset": 6,
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};