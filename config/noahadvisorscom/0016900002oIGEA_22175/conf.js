ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 1,
 "startAdsBeforeClip": 7,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 5557,
 "accountId": 1187,
 "publisherDomainId": 1441,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "includeKeywordsLabels": false,
 "enableVideoTargeting": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": true,
 "locked": false,
 "pauseClipNotInView": false,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": true,
 "account": {
  "salesforceId": "0016900002oIGEAAA4"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJub2FoYWR2aXNvcnNjb20iLCJ3aWQiOiIwMDE2OTAwMDAyb0lHRUFfMjIxNzUiLCJ1cmwiOlsiYW55Y2xpcC5jb20iLCJub2FoLWFkdmlzb3JzLmNvbSIsIm5vYWgtY29uZmVyZW5jZS5jb20iXSwiZXhwaXJ5IjoiMjAyNC0wMS0yNVQwOTo0Mjo0Mi4zNDlaIiwiaWF0IjoxNjc0NjM5NzYyfQ.9EMGjzdyJjC2Da9Y6xvmXyefjT8xmGhKTGFLhW7AE2Q",
 "playerPlaceholderSelector": [
  "#ac-lre-player"
 ],
 "auto-play-desktop": "no-autoplay",
 "auto-play-mobile": "no-autoplay",
 "publisherId": "noahadvisorscom",
 "sfAccountId": "0016900002oIGEAAA4",
 "widgetId": "0016900002oIGEA_22175",
 "adTag_https": "https://vid.springserve.com/vast/299517?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "adTag_Mobile_https": "https://vid.springserve.com/vast/299517?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "adTag_https_btf": "https://vid.springserve.com/vast/299517?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "adTag_Mobile_https_btf": "https://vid.springserve.com/vast/299517?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&domain=$[domain]",
 "lre_useAdManager": "off",
 "lre_useAdManagerForMobile": "off",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 25000,
  "notInviewAdInterval": 25000,
  "inviewRetryAdInterval": 10000,
  "notInviewRetryAdInterval": 10000,
  "imaAdRequestTimeout": 24000,
  "imaAdRequestTimeout_btf": 24000,
  "maxAdsPerClip": 0,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 1500,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 25000,
  "notInviewAdInterval": 25000,
  "inviewRetryAdInterval": 10000,
  "notInviewRetryAdInterval": 10000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 0,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 1500,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": true,
  "floatingMode": "always",
  "floatingPosition": "top-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": -1,
  "floatingMargin": 0,
  "floatingTheme": "page-width",
  "visibilityTogglePercents": 100
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": ".video-js .vjs-tech{visibility: visible !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress{background-color: #1CB248 !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-volume-level {background-color: #1CB248 !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-progress-control .vjs-progress-holder .vjs-mouse-display .vjs-time-tooltip {background-color: #1CB248 !important;box-shadow: inset -2px -2px 3px 0px #82cf99 !important;}.ac-as-menu-item:hover{  background-color: #1CB248 !important;}.ac-lre-desktop .ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-item:hover, .ac-lre-desktop .ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-item.ac-as-item-selected:hover{color: white !important;}.ac-lre-desktop .ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-item.ac-as-item-selected:hover label {color: white !important;}.ac-as-menu-item.ac-as-item-selected {   color: #1CB248 !important;}.ac-as-menu-item.ac-as-item-selected label {   color: #1CB248 !important;}.ac-lre-player .video-js .ac-as-menu-pane .ac-as-menu-wrapper .ac-as-menu-header:hover:not(.top-level):before {    border: solid #1CB248 !important;    border-width: 0 0 2px 2px !important;}.ac-lre-player .video-js .ac-luminous-x-container .ac-luminous-x-circle-container.ac-luminous-x-circle-container {  background: rgba(28, 178, 72,0.35) !important;}.ac-lre-player .video-js .ac-luminous-x-container .ac-luminous-x-circle-container.ac-luminous-x-circle-container .luminous-x-item-wrapper .luminous-x-item {border: solid 1px #1CB248 !important;}.ac-lre-player .video-js .ac-luminous-x-container .ac-luminous-x-circle-container.ac-luminous-x-circle-container .luminous-x-item-wrapper .lumi-x-item-label {border: 1px solid #1CB248 !important;background-color: #1CB248 !important;}.lumi-x-label-action-trackedcampaigns {    background-color: rgba(0,0,0,0.43) !important;color: #ffffff !important;}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": true,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 1000
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": true,
  "floatingModeToggle": true
 },
 "lre_export_for_demo": true,
 "sendClipUnitsPlayed": true,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": false
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": true,
  "timelineIndicationEnabled": true
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": false,
  "shareButton": false,
  "viewButton": false,
  "likeButton": false,
  "position": "top-left"
 },
 "playerPreset": 3,
 "fullScreenButton": true,
 "theaterModeButton": false,
 "clipTitleUnderPlayer": false,
 
 	 "startInBestResolutionDesktop": true,

 
 "ab_test": {
  "enabled": false,
  "variants": [
   {
    "variantName": "noah-advisorscom|resolution|A|0",
    "percentage": 0
   },
   {
    "variantName": "noah-advisorscom|resolution|B|100",
    "percentage": 100,
    "conf": {
     "id": 7678,
     "allFeeds": false,
	 "startInBestResolutionDesktop": true,
     "altEnv": "https://player.anyclip.com/anyclip-widget/lre-widget/prod/noa1/src/lre.js"
    }
   }
  ]
 },
"conditionalConf": [{}],
"customMacros": {}
};