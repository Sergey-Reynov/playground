ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 2,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 123021,
 "accountId": 1,
 "publisherDomainId": 1,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "includeKeywordsLabels": true,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": false,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "account": {
  "salesforceId": "001w000001flCw2AAE"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJwdWItcWEiLCJ3aWQiOiJhbXAtcGx5LWFkcy13aGVuLWNhcm91c2VsLWVuZHMtMiIsInVybCI6W10sImV4cGlyeSI6IjIwMjMtMTEtMjdUMTI6MDY6MDYuMjE3WiIsImlhdCI6MTY2OTU1MDc2Nn0.ImpvgBZwW0vIjqoBuKum1Gl27juwQ3AKORzEuw6YmpU",
 "fallbackPlaylist": [
  {
   "file": "https://acdev-weavo-qa.qa-anyclip.com/AWIDVBZaq0jfH7w59ZzA/clip/AWIDVPifq0jfH7w59ZzV/clip.mp4",
   "title": "John Wick 3 Ready To Start Filming",
   "image": "https://acdev-weavo-movies-qa.qa-anyclip.com/35/JohnWick3ReadyToStartFilming.jpg",
   "mediaid": "AWIDVPifq0jfH7w59ZzV",
   "files": [
    {
     "width": -1,
     "height": -1,
     "file": "https://acdev-weavo-qa.qa-anyclip.com/AWIDVBZaq0jfH7w59ZzA/clip/AWIDVPifq0jfH7w59ZzV/clip.mp4"
    }
   ],
   "images": [
    {
     "width": 1280,
     "height": 720,
     "file": "https://acdev-weavo-movies-qa.qa-anyclip.com/35/JohnWick3ReadyToStartFilming.jpg"
    }
   ]
  }
 ],
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay-inview",
 "auto-play-mobile": "autoplay",
 "publisherId": "pub-qa",
 "sfAccountId": "001w000001flCw2AAE",
 "widgetId": "amp-ply-ads-when-carousel-ends-2",
//  "adTag_https": "https://vid.springserve.com/vast/412415?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_https": "https://ac-qa-marketplace.qa-anyclip.com/v1/waterfall?sti=pEeYa4EBADc5UsK4HvmA&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]",
 "adTag_Mobile_https": "https://ac-qa-marketplace.qa-anyclip.com/v1/waterfall?sti=pEeYa4EBADc5UsK4HvmA&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]",
//  "adTag_https_btf": "https://vid.springserve.com/vast/412415?w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]&consent=$[cd]&viewability=$[v]&schain=$[schain]&gdpr=$[gdpr]",
 "adTag_https_btf": "https://ac-qa-marketplace.qa-anyclip.com/v1/waterfall?sti=pEeYa4EBADc5UsK4HvmA&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]",
 "adTag_Mobile_https_btf": "https://ac-qa-marketplace.qa-anyclip.com/v1/waterfall?sti=pEeYa4EBADc5UsK4HvmA&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]",
//  "lre_useAdManager": "off",
 "lre_useAdManager": "ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 10,
  "notInviewAdInterval": 10,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 24000,
  "imaAdRequestTimeout_btf": 24000,
  "maxAdsPerClip": 1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 11000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": true,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 5000,
  "notInviewAdInterval": 1000,
  "inviewRetryAdInterval": 5000,
  "notInviewRetryAdInterval": 5000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": false,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 1000,
  "notInviewSmartAdInterval": 11000,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": true,
  "leftText": "Recommended videos",
  "rightText": "Powered by <a href='https://anyclip.com/?source=right_branding' class='any-clip-brand' target='_blank'>AnyClip</a>"
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 400,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": false,
  "floatingMode": "only",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": false,
 "lre_carouselEnabledMobile": true,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": "",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": true,
  "animationInterval": 15000
 },
 "campaigns": {
  "enabled": false,
  "text": "",
  "url": ""
 },
 "adServerDesktop": {
  "adSkipTimeout": 0,
  "enabled": false,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": 0,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": false,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "static-playlist",
 "lre_playlistUseTrafficManager": false,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": false
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": false,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": false,
  "position": "top-left"
 },
 "playerPreset": 4,
 "fullScreenButton": true,
 "theaterModeButton": false,
 "clipTitleUnderPlayer": false,
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {}
};