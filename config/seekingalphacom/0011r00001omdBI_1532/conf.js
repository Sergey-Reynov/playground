ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 5,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 2561,
 "publisherDomainId": 314,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": false,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": false,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "fallbackPlaylist": [
  {
   "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639591617_1280x720_video.mp4",
   "title": "The Quarter That Shook Markets",
   "image": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639545069_248x140_thumbnail.jpg",
   "mediaid": "ifmek5tfovauqnkiiy4ectclnvye2msr",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639590990_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639591177_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639591341_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639591617_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639545069_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639544878_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639545097_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639544954_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/1585639544756_1280x720_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXEveuAH5HF8ALKmpM2Q/hls_1585639586479/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585345104494_1280x720_video.mp4",
   "title": "Ballmer Expects Markets to 'Stay Depressed for Some Time'",
   "image": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585344984557_248x140_thumbnail.jpg",
   "mediaid": "ifmekzbwgy4vazkvg5gtetc7ozcgozt2",
   "files": [
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585345103472_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585345103829_852x480_video.mp4"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585345104257_480x270_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585345104494_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585344984557_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585344984447_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585344984345_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585344984407_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/1585344984213_1280x720_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXEd669PeU7M2L_vDgfz/hls_1585345092643/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066521831_1280x720_video.mp4",
   "title": "Breaking the ASX’s Glass Ceiling",
   "image": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066327459_248x140_thumbnail.jpg",
   "mediaid": "ifmeiutuirjs2vlrjjhxctkuk4zdk6tq",
   "files": [
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066520332_640x360_video.mp4"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066520853_480x270_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066521189_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066521831_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066327459_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066327294_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066328038_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066327495_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066328099_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/1584066327208_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXDRtDS-UqJOqMTW25zp/hls_1584066485474/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501196401_1280x720_video.mp4",
   "title": "Investors Yank Cash From U.S. Credit Funds",
   "image": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145637_248x140_thumbnail.jpg",
   "mediaid": "ifmeg52cliytc3d2ozys23lqifwv6mcb",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501195710_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501195887_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501196119_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501196401_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145637_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145715_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145450_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145667_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145549_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/1583501145369_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXCwBZ11lzvq-mpAm_0A/hls_1583501188654/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722620602_1280x720_video.mp4",
   "title": "Dubai Moves to Shield Prized Emirates Airline From Virus Fallout",
   "image": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722430335_248x140_thumbnail.jpg",
   "mediaid": "ifmekmdbnvjeqzkvg5gtetc7ozheg3tm",
   "files": [
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722618784_852x480_video.mp4"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722619550_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722619943_640x360_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722620602_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722430335_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722430443_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722430387_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722430474_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/1585722430229_1280x720_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXE0amRHeU7M2L_vNCnl/hls_1585722576251/master.m3u8"
   }
  }
 ],
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay-inview",
 "auto-play-mobile": "autoplay-inview",
 "publisherId": "seekingalphacom",
 "sfAccountId": "0011r00001omdBIAAY",
 "widgetId": "0011r00001omdBI_1532",
 "adTag_https": "https://vid.springserve.com/vast/595502?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&a=$[a]",
 "adTag_Mobile_https": "https://vid.springserve.com/vast/595502?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&a=$[a]",
 "adTag_https_btf": "https://vid.springserve.com/vast/595502?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&a=$[a]",
 "adTag_Mobile_https_btf": "https://vid.springserve.com/vast/595502?ima=$[sps_ima_mode]&w=$[width]&h=$[height]&url=$[pageUrl]&cb=$[cb]&widgetid=$[widgetId]&lob=$[abc]&clipid=$[clipId]&key_custom1=^w=$[widgetId]^c=$[clipId]^i=$[clipPlayCounter]^ab=$[abc]^v=$[v]^p=$[publisherId]&key_custom2=^d=$[domain]^u=$[utm]^dv=$[device]^co=$[geo]^pl=$[playback_type]&gdpr=$[gdpr]&consent=$[cd]&viewability=$[v]&schain=$[schain]&us_privacy=$[us_privacy]&a=$[a]",
 "lre_useAdManager": "ac",//"ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 999999999,
  "notInviewAdInterval": 999999999,
  "inviewRetryAdInterval": 999999999,
  "notInviewRetryAdInterval": 999999999,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 0,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 999999999,
  "notInviewAdInterval": 999999999,
  "inviewRetryAdInterval": 999999999,
  "notInviewRetryAdInterval": 999999999,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": 1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 0,
  "adRequestDelay": 0,
  "aps": false,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "",
  "apsSlotId": "",
  "apsSecure": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": false,
  "leftText": "",
  "rightText": ""
 },
 "floatingDesktop": {
  "floatingEnabled": true,
  "floatingMode": "after-in-view",
  "floatingPosition": "bottom-right",
  "floatingWidth": 415,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": true,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": "#ac-lre-wrapper.luminous-theme #ac-player-wrapper #ac-lre-player .video-js .vjs-progress-control .vjs-progress-holder .vjs-mouse-display .vjs-time-tooltip {    color: #fff;   background-color: #ff9402;    box-shadow: inset -2px -2px 3px 0px #ff9402;}.ac-lre-wrapper.luminous-theme .ac-player-playlist .purejscarousel-slides-container .lre-playlist-item.purejscarousel-slide .lre-playlist-item-link .lre-playlist-thumbnail-wrapper .lre-playlist-item-playing {    font-size: 10px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block {    font-size: 10px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress {    background-color: #ff9402;}.ac-player-playlist p {    bottom: 12px;    font-size: 10px}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js.vjs-layout-x-small.lx-size-0 .vjs-current-time, .ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js.vjs-layout-xs-small.lx-size-0 .vjs-current-time {    padding-left: 19px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js.vjs-layout-x-small.lx-size-0 .vjs-duration, .ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js.vjs-layout-xs-small.lx-size-0 .vjs-duration {    padding-left: 0px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-time-divider {    padding-left: 5px;}#ac-lre-wrapper.luminous-theme #ac-player-wrapper #ac-lre-player .video-js .vjs-volume-level {    background-color: #fff;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block .lre-title-text, .ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .lre-vjs-video-title-block .lre-title-text-content {    width: 90%;}.ac-lre-wrapper {    top: 10px;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper {    top: -10px;}.ac-lre-wrapper.luminous-theme .ac-player-playlist .purejscarousel-btn-next {    background-image: url(\"https://anyclip-player.s3.amazonaws.com/pub-assets/seekingalphacom/right-arrow.svg?hash=8667114fab\");}.ac-lre-wrapper.luminous-theme .ac-player-playlist .purejscarousel-btn-prev {    background-image: url(\"https://anyclip-player.s3.amazonaws.com/pub-assets/seekingalphacom/back.svg\");}.ac-lre-wrapper.luminous-theme #ac-player-wrapper #ac-lre-player .video-js #lre-vjs-gradient-to-bottom.lre-gradient-to-bottom {    background-image: none;}.ac-lre-wrapper.luminous-theme #ac-player-wrapper #ac-lre-player .video-js #lre-vjs-gradient-to-top.lre-gradient-to-top{    background-image: none;}.lre-ad-countdown-timer {    display: inline-block;    width: 60px;    height: 20px;    background-color: rgba(77,77,77,0.73);    position: absolute;    bottom: 4.9em;    left: 0;    text-align: center;    line-height: 24px;    font-size: 10px;    border: 1px solid rgba(111,111,111,0.7);    color: #fff;    font-family: Arial,sans-serif;    z-index: 1000;}.ac-player-wrapper .ac-lre-player.vjs-flag-ads.vjs-flag-ads-hide-controls .ac-lre-ima-ad-wrapper .lre-vlm-btn {         right: 15px;    bottom: 45px;    background-image: url(“https://anyclip-player.s3.amazonaws.com/pub-assets/seekingalphacom/sound_icon.svg“);}.ac-lre-ima-ad-wrapper .lre-vlm-btn {    width: 18px !important;    height: 18px !important;    right: 15px !important;    bottom: 45px !important;}.ac-lre-ad-bar {    background-color: #00000057;  }",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": false,
  "animationInterval": 0
 },
 "campaigns": {
  "enabled": true,
  "text": "Marketplace",
  "url": "https://seekingalpha.com/marketplace?c=New"
 },
 "adServerDesktop": {
  "adSkipTimeout": -1,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": -1,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": false,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "recent",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "visible"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-left"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": 1,
 "lre_playerLogo": {
  "enabled": true,
  "file": "",
  "link": ""
 },
 "ab_test": {
  "enabled": false,
  "variants": []
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJzZWVraW5nYWxwaGFjb20iLCJ3aWQiOiIwMDExcjAwMDAxb21kQklfMTUzMiIsInVybCI6WyJ3d3cuc2Vla2luZ2FscGhhLmNvbSJdLCJleHBpcnkiOiIyMDIzLTA1LTA5VDA4OjUyOjE0LjgyM1oiLCJpYXQiOjE2NTIwODYzMzV9.KnL38evXCZOGYjh-ie7vlbKmGQWIEULB4L0Rmq0G_oE",
"conditionalConf": [{}],
"customMacros": {}
};