ac_lre_conf = {
 "playerWidth": "100%",
 "playlistLimit": 5,
 "startAdsBeforeClip": 1,
 "loopPlaylist": false,
 "startWithSound": false,
 "skipVastSeconds": -1,
 "enableDebugEvents": false,
 "lre_nlpid": 2,
 "id": 1049,
 "accountId": 498,
 "publisherDomainId": 214,
 "playlistDurationLimit": 480,
 "playlistLimitType": "CLIPS",
 "loadPlayerWithAdBlocker": true,
 "analyzer": "ANYCLIP",
 "ignoreFeedsPriorities": false,
 "defaultPlayerWidth": 640,
 "preloadPosters": false,
 "onlyVideosFromLastDays": false,
 "tryDefaultPlaceholder": true,
 "videosFromLastDays": 30,
 "overrideCacheRefresh": true,
 "locked": false,
 "pauseClipNotInView": true,
 "preRoll": false,
 "multipleRetryIntervalDesktopInView": 1,
 "multipleRetryIntervalDesktopNotInView": 1,
 "multipleRetryIntervalMobileInView": 1,
 "multipleRetryIntervalMobileNotInView": 1,
 "selfServe": false,
 "allFeeds": false,
 "account": {
  "salesforceId": "0011r00002IjDWFAA3"
 },
 "wtok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiJ1c25ld3Njb20iLCJ3aWQiOiIwMDExcjAwMDAySWpEV0ZfNzI1IiwidXJsIjpbInVzbmV3cy5jb20iLCJ3d3cudXNuZXdzLmNvbSJdLCJleHBpcnkiOiIyMDIzLTEwLTAzVDE4OjQ5OjQ4LjMyN1oiLCJpYXQiOjE2NjQ4MjI5ODh9.xjHPxr9gFNUtpIesFIeG5kRHiRsHm4oAbVBrM7oms6E",
 "fallbackPlaylist": [
  {
   "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611083098408_1280x720_video.mp4",
   "title": "Explainer: Who is Kremlin critic Alexei Navalny?",
   "image": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919179_248x140_thumbnail.jpg",
   "mediaid": "ifmggy2ckzfvatlsom3f6zlzm5gdqt3t",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611083095461_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611083096516_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611083097320_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611083098408_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611083099789_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919179_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919259_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919289_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919378_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919485_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/1611082919519_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXccBVKPMrs6_eygL8Os/hls_1611083135754/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611083047347_1280x720_video.mp4",
   "title": "Microsoft joins GM, Cruise self-driving partnership",
   "image": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082881942_248x140_thumbnail.jpg",
   "mediaid": "ifmggy2ckvltstlsom3f6zlzm5gdqt3b",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611083045688_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611083046103_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611083046577_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611083047347_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611083048575_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082881942_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082882019_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082882049_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082882091_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082882152_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/1611082882226_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXccBUW9Mrs6_eygL8Oa/hls_1611083023940/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082956267_1280x720_video.mp4",
   "title": "Uganda accuses U.S. envoy of trying to subvert election",
   "image": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868290_248x140_thumbnail.jpg",
   "mediaid": "ifmggy2ckn4fctlsom3f6zlzm5gdqttt",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082953777_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082954537_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082955472_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082956267_1280x720_video.mp4"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082957376_1920x1080_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868290_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868380_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868436_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868473_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868537_1280x720_thumbnail.jpg"
    },
    {
     "width": 1920,
     "height": 1080,
     "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/1611082868641_1920x1080_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXccBSxQMrs6_eygL8Ns/hls_1611082977975/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611083062682_1280x720_video.mp4",
   "title": "Spectacular Northern Lights over frozen forest and river",
   "image": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611082883705_248x140_thumbnail.jpg",
   "mediaid": "ifmggy2cku4w6qldmnyukmcxmvdfgqsx",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611083062097_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611083062254_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611083062436_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611083062682_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611082883705_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611082883811_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611082883883_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611082883918_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/1611082883983_1280x720_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXccBU9oAccqE0WeFSBW/hls_1611083056917/master.m3u8"
   }
  },
  {
   "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079228098_1280x720_video.mp4",
   "title": "Drone footage shows Swiss vineyards under snow",
   "image": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079224704_248x140_thumbnail.jpg",
   "mediaid": "ifmggyt2mvteitlsom3f6zlzm5gdgnzy",
   "files": [
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079226794_480x270_video.mp4"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079227120_640x360_video.mp4"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079227498_852x480_video.mp4"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079228098_1280x720_video.mp4"
    }
   ],
   "images": [
    {
     "width": 248,
     "height": 140,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079224704_248x140_thumbnail.jpg"
    },
    {
     "width": 480,
     "height": 270,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079224786_480x270_thumbnail.jpg"
    },
    {
     "width": 640,
     "height": 360,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079224818_640x360_thumbnail.jpg"
    },
    {
     "width": 852,
     "height": 480,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079224853_852x480_thumbnail.jpg"
    },
    {
     "width": 1280,
     "height": 720,
     "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/1611079224908_1280x720_thumbnail.jpg"
    }
   ],
   "hlsFile": {
    "file": "https://cdn5.anyclip.com/AXcbzefDMrs6_eygL378/hls_1611079237777/master.m3u8"
   }
  }
 ],
 "playerPlaceholderSelector": [],
 "auto-play-desktop": "autoplay-after-ad",
 "auto-play-mobile": "autoplay-after-ad",
 "publisherId": "usnewscom",
 "sfAccountId": "0011r00002IjDWFAA3",
 "widgetId": "0011r00002IjDWF_725",
 "adTag_https": "https://marketplace.anyclip.com/v1/waterfall?sti=3nFpzIABvkyXq-6VnYtW&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https": "https://marketplace.anyclip.com/v1/waterfall?sti=AYsdBH0BBaam04j5as4-&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=1RVpzIABhRNLCGGWqt9v&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "adTag_Mobile_https_btf": "https://marketplace.anyclip.com/v1/waterfall?sti=AIscBH0BBaam04j5_M7Q&w=$[width]&h=$[height]&v=$[v]&cb=$[cb]&pid=$[publisherId]&sid=$[sid]&cid=$[clipId]&wid=$[widgetId]&dom=$[domain]&abc=$[abc]&geo=$[geo]&dev=$[device]&bw=$[browser]&os=$[os]&ip=$[ip]&url=$[pageUrl]&gdpr=$[gdpr]&consent=$[cd]&schain=$[schain]&us_privacy=$[us_privacy]&utm=$[utm]&pl=$[playback_type]&ima=$[sps_ima_mode]&clipPlayCounter=$[clipPlayCounter]&tid=$[tid]&amznbid=$[amznbid]&amzniid=$[amzniid]&ua=$[ua]&domain=$[domain]&page=$[page]&itemid=$[itemid]&zone=$[zone]&permutive=$[permutive]&key_custom3=$[cma1]&gpt=$[gpt]",
 "lre_useAdManager": "ima",
 "lre_useAdManagerForMobile": "ima",
 "lre_imaAds": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 50000,
  "inviewRetryAdInterval": 9000,
  "notInviewRetryAdInterval": 9000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 9500,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Sites",
  "apsSecure": true,
  "playAdsOnPlayerShow": true
 },
 "lre_imaAdsForMobile": {
  "adIndicator": true,
  "adIndicatorText": "Ad",
  "playAdsOnLoopEnd": true,
  "playAdsNotInView": false,
  "playAdsOnUserPause": false,
  "inviewAdInterval": 30000,
  "notInviewAdInterval": 50000,
  "inviewRetryAdInterval": 10000,
  "notInviewRetryAdInterval": 10000,
  "imaAdRequestTimeout": 20000,
  "imaAdRequestTimeout_btf": 20000,
  "maxAdsPerClip": -1,
  "maxArqPerClip": -1,
  "useSecureIframe": false,
  "playAdsBeforeClickToPlay": true,
  "prerollTimeout": 0,
  "inviewSmartAdInterval": 0,
  "notInviewSmartAdInterval": 10000,
  "adRequestDelay": 0,
  "aps": true,
  "apsBidTimeout": 2000,
  "apsInView": true,
  "apsNotInView": true,
  "apsPubId": "171208af-037d-48f1-af92-1c24c2ee644a",
  "apsSlotId": "AC_Sites",
  "apsSecure": true,
  "playAdsOnPlayerShow": true
 },
 "aspectratio": "16:9",
 "branding": {
  "enabled": true,
  "leftText": "Recommended Videos",
  "rightText": "Powered by <a href='https://anyclip.com/?source=right_branding' class='any-clip-brand' target='_blank'>AnyClip</a>"
 },
 "floatingDesktop": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 415,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0
 },
 "floatingMobile": {
  "floatingEnabled": false,
  "floatingMode": "always",
  "floatingPosition": "bottom-right",
  "floatingWidth": 300,
  "floatingDelay": 0,
  "floatingReopenWait": 0,
  "closeButtonDelay": 0,
  "floatingMargin": 0,
  "floatingTheme": "DISABLED"
 },
 "secondPlayer": {
  "enabled": false,
  "placeholderSelector": []
 },
 "ampStickyBarMode": false,
 "lre_carouselEnabled": true,
 "lre_carouselEnabledMobile": false,
 "carousel": {
  "type": "horizontal"
 },
 "customStyle": ".brand-container {  display: flex;}.brand-container .ac-branding-left-text {  margin-bottom: 5px;  color: #111111;  font-size: 0.85rem !important;  font-family: \"Montserrat\", Helvetica Neue, Helvetica, sans-serif !important;  font-weight: 700 !important;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-play-progress {  background-color: #c4212a;}.ac-lre-wrapper.luminous-theme  .ac-player-wrapper  .ac-lre-player  .video-js  .vjs-progress-control  .vjs-progress-holder  .vjs-mouse-display  .vjs-time-tooltip {  color: #fff;  background-color: #c4212a;  box-shadow: inset -2px -2px 3px 0px #c4212a;}.ac-lre-wrapper.luminous-theme .ac-player-wrapper .ac-lre-player .video-js .vjs-volume-level {  background-color: #c4212a;}@media screen and (max-width: 768px) {  .brand-container .ac-branding-left-text {    font-size: 1.125rem !important;  }}.brand-container .ac-branding-right-text {  align-self: flex-end;  margin-bottom: 3px;}.ac-context-menu {  z-index: 1500;}@media (max-width: 400px) {    .brand-container .ac-branding-left-text {        font-size: 13px!important;        margin-bottom: 0;    }}.ac-lre-player-ph .ac-lre-wrapper.luminous-theme .ac-player-ph .ac-player-wrapper .ac-lre-player [id*=ac-lre-vjs] .ac-lre-social-action-items {    top: 55px !important;}.ac-lre-ad-bar{z-index : 500 !important;}",
 "playAdsLoopComplete": true,
 "xRayCampaignsEnabled": false,
 "lumxDesktop": {
  "enabled": true,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxMobile": {
  "enabled": false,
  "autoshow": true,
  "hideTimeout": 90000
 },
 "lumxTeaser": {
  "enabled": true,
  "animationInterval": 10000
 },
 "campaigns": {
  "enabled": false,
  "text": "Test",
  "url": "https://www.anyclip.com"
 },
 "adServerDesktop": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "adServerMobile": {
  "adSkipTimeout": 5000,
  "enabled": true,
  "stagingEnabled": false,
  "showMuteButton": true
 },
 "funcExpose": {
  "setUrl": false,
  "playerMoved": false,
  "floatingModeToggle": false
 },
 "lre_export_for_demo": true,//false,
 "sendClipUnitsPlayed": false,
 "lre_playlistType": "luma",
 "lre_playlistUseTrafficManager": true,
 "shuffleTMResponse": false,
 "shuffleFallback": false,
 "externalTracking": {
  "firstAdRequest": null,
  "everyAdImpression": null
 },
 "cookieSupport": {
  "userIdCreate": true
 },
 "trendingArticles": {
  "enabled": false,
  "timer": 6
 },
 "closedCaptions": {
  "state": "hidden"
 },
 "chaptering": {
  "menuListEnabled": false,
  "timelineIndicationEnabled": false
 },
 "videoMediaType": "hls",
 "maxRetryIntervalDesktopInView": 300000,
 "maxRetryIntervalDesktopNotInView": 300000,
 "maxRetryIntervalMobileInView": 300000,
 "maxRetryIntervalMobileNotInView": 300000,
 "advSettingsControl": {
  "resolutionPane": true,
  "speedPane": true
 },
 "fullScreenButtonToRedirect": {
  "enabled": false,
  "url": "",
  "sameTab": false
 },
 "socialShareLandingPageURL": "",
 "socialIconsDesktop": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-right"
 },
 "socialIconsMobile": {
  "enabled": true,
  "shareButton": false,
  "viewButton": true,
  "likeButton": true,
  "position": "top-right"
 },
 "seo": {
  "googleSearch": true
 },
 "playerPreset": 1,
 "externalAdTagsUri": "https://player.anyclip.com/anyclip-widget/lre-widget/pubscripts/usnews/external-adtags.js",
 "externalAdTagsTimeout": 20000,
 "ab_test": {
  "enabled": false,
  "variants": []
 },
"conditionalConf": [{}],
"customMacros": {
    "cust1": function() {
        if (location.host.search('usnews.com') === -1) return null;
        if (window.utag && window.utag.data) {
            let data = window.utag.data;
            let site_portal = data["site_portal"] || "";
            let site_product_subsection = data["site_product_subsection"] || "";
            let page_type = data["page_type"] || "";
            let site_product_section = data["site_product_section"] || "";
            let site_product = data["site_product"] || "";
            let site_vertical = data["site_vertical"] || "";
            let content_partner = data["content_partner"] || "";
            return `^0=${site_portal}^1=${site_product_subsection}^2=${page_type}^3=${site_product_section}^4=${site_product}^5=${site_vertical}^6=${content_partner}`;
        }
    },
     "cma1" : function() {
        if (window.utag && window.utag.data) {
            let data = window.utag.data;
            let site_portal = data["site_portal"] || "";
            let site_product_subsection = data["site_product_subsection"] || "";
            let page_type = data["page_type"] || "";
            let site_product_section = data["site_product_section"] || "";
            let site_product = data["site_product"] || "";
            let site_vertical = data["site_vertical"] || "";
            let content_partner = data["content_partner"] || "";
            return `^0=${site_portal}^1=${site_product_subsection}^2=${page_type}^3=${site_product_section}^4=${site_product}^5=${site_vertical}^6=${content_partner}`;
        } else {
            return null;
        }
    }
}
};