(function () {
    const params = new URLSearchParams(window.location.search);
    const showElement = (el, isShow) => {
        if (el) el.style.display = isShow ? 'block' : 'none';
    };

    const loadRoboto = () => {
        let roboto = document.createElement('link');
        roboto.rel = 'stylesheet';
        roboto.href = 'https://fonts.googleapis.com/css?family=Roboto';
        document.head.appendChild(roboto);
        showElement(document.querySelector('#x-roboto'), true);
        showElement(document.querySelector('#x-no-roboto'), false);
    };
    document.querySelector('#btnLoadRoboto').addEventListener('click', loadRoboto);

    // load font Roboto (optional)
    let isRoboto = params.get('roboto') === '1';
    if (isRoboto) {
        loadRoboto();
    } else {
        showElement(document.querySelector('#x-roboto'), false);
        showElement(document.querySelector('#x-no-roboto'), true);
    }

    // detect fb source
    const urlQA = 'https://acdev-lre-player-qa.s3.amazonaws.com/anyclip-widget/acfb/qa/v1/src/acfb.js';
    let url = params.get('js');
    if (!url) {
        const urlType = params.get('t');
        if (urlType) {
            let urls = {
                preview: 'fb/acfb.js',
                lh: 'http://localhost/lre-form-builder/acfb.js',
                cdn: 'https://player.anyclip.com/acfb/prod/v1/src/acfb.js',
                prod: 'https://anyclip-player.s3.amazonaws.com/acfb/prod/v1/src/acfb.js',
                int: 'https://acdev-lre-player-int.s3.amazonaws.com/anyclip-widget/acfb/int/v1/src/acfb.js',
            };
            url = urls[urlType] || urlQA;
            const subversion = params.get('v');
            if (subversion) {
                url = url.replace('/v1/', '/' + subversion + '/');
            }
        }
    }

    // test forms without player

    let target = document.querySelector('#formTester');
    let exampleMetaData = null;
    let formBuilder = null;

    /** @type {HTMLInputElement} */
    let chkEnableInstantDelivery = document.querySelector('#chkEnableInstantDelivery');
    const updateMetadata = (md) => {
        if (md && md.globalConfig) {
            if (chkEnableInstantDelivery.checked) md.globalConfig.enableInstantDelivery = true;
            else delete md.globalConfig.enableInstantDelivery;
        }
    };

    let scr = document.createElement('script');
    scr.src = url || urlQA;
    scr.onload = () => {
        if (window.createLreFormBuilder) {
            formBuilder = window.createLreFormBuilder();

            formBuilder.subscribe((data) => {
                console.log('received: show', data);
                chkEnableInstantDelivery.disabled = true;
            }, 'show');

            formBuilder.subscribe((data) => {
                console.log('received: submit', data);
                if (!data.isWaitSubmit) {
                    chkEnableInstantDelivery.disabled = false;
                }
            }, 'submit');

            formBuilder.subscribe((data) => {
                console.log('received: close', data);
                chkEnableInstantDelivery.disabled = false;
            }, 'close');

            formBuilder.setTarget(target);
            txtMetadata.value = JSON.stringify(exampleMetaData, null, 2);

            let ssp = document.querySelector('#chkShowSpinner');
            if (ssp && typeof formBuilder.showSpinner === 'function') {
                ssp.parentElement.style.display = null;
                ssp.addEventListener('change', (e) => {
                    formBuilder.showSpinner(e.target.checked);
                });
            }

            let evt = document.querySelector('#emailValidationTester');
            if (typeof formBuilder.isEmail === 'function') {
                /** @type {HTMLTextAreaElement} */
                let txtValidEmails = evt.querySelector('#txtValidEmails');
                let validEmailExamples = [
                    `abc@def.ghi`,
                    `abc@def.gh.io`,
                    `a.bc@def.gh.io`,

                    `simple@example.com`,
                    `very.common@example.com`,
                    `disposable.style.email.with+symbol@example.com`,
                    `other.email-with-hyphen@example.com`,
                    `fully-qualified-domain@example.com`,
                    `user.name+tag+sorting@example.com`,
                    `x@example.com`,
                    `example-indeed@strange-example.com`,
                    `test/test@test.com`,
                    `example@s.example`,
                    `mailhost!username@example.org`,
                    `user%example.com@example.org`,
                    `user-@example.org`,
                    `1234567890123456789012345678901234567890123456789012345678901234@fghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.com`,
                ];
                txtValidEmails.value = validEmailExamples.join('\n');

                let txtInvalidEmails = evt.querySelector('#txtInvalidEmails');
                let invalidEmailExamples = [
                    `abc.def.ghi`,
                    `abc@d@ef.ghi`,
                    `.abc@def.ghi`,
                    `a..bc@def.ghi`,
                    `אבג!#$%^&*@email.com`,
                    `3@f@gmail.com`,

                    `Abc.example.com`,
                    `A@b@c@example.com`,
                    `a"b(c)d,e:f;g<h>i[j\k]l@example.com`,
                    `just"not"right@example.com`,
                    `this is"not\allowed@example.com`,
                    `this\ still\"not\\allowed@example.com`,
                    `1234567890123456789012345678901234567890123456789012345678901234+x@example.com`,
                    `i_like_underscore@but_its_not_allowed_in_this_part.example.com`,
                    `QA[icon]CHOCOLATE[icon]@test.com`,
                    `1234567890123456789012345678901234567890123456789012345678901234@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmno.com`,
                    `admin@mailserver1`,
                    `" "@example.org`,
                    `"john..doe"@example.org`,
                    `postmaster@[123.123.123.123]`,
                    `postmaster@[IPv6:2001:0db8:85a3:0000:0000:8a2e:0370:7334]`,
                ];
                txtInvalidEmails.value = invalidEmailExamples.join('\n');

                let btnVE = evt.querySelector('#btnTestEmailValidation');
                btnVE.addEventListener('click', () => {
                    validEmailExamples = txtValidEmails.value.split('\n');
                    console.groupCollapsed('Validate emails - following examples MUST BE %cVALID', 'color:green');
                    validEmailExamples.forEach((item) => {
                        let email = item.trim();
                        if (email) {
                            let isValid = formBuilder.isEmail(email);
                            console.log(`%c[${email}]:${isValid ? '' : 'in'}valid`, `color:${isValid ? 'green' : 'red'}`);
                        }
                    });
                    console.groupEnd();

                    invalidEmailExamples = txtInvalidEmails.value.split('\n');
                    console.groupCollapsed('Validate emails - following examples MUST BE %cINVALID', 'color:blue');
                    invalidEmailExamples.forEach((item) => {
                        let email = item.trim();
                        if (email) {
                            let isValid = formBuilder.isEmail(email);
                            console.log(`%c[${email}]:${isValid ? '' : 'in'}valid`, `color:${isValid ? 'red' : 'blue'}`);
                        }
                    });
                    console.groupEnd();
                    alert('See validation test in console');
                });
            } else {
                evt.parentElement.removeChild(evt);
            }
        }
    };
    document.head.appendChild(scr);

    /** @type {HTMLTextAreaElement} */
    let txtMetadata = document.querySelector('#txtMetadata');
    let restartForm = () => {
        let strMetadata = txtMetadata.value;
        try {
            let md = JSON.parse(strMetadata);
            if (md && md.globalConfig && md.widget) {
                updateMetadata(md);
                formBuilder.showForm(md);
                exampleMetaData = null;
            } else {
                alert('Incorrect metadata');
            }
        } catch (er) {
            console.log('Metadata error', er);
            alert(`Error\n${er}`);
        }
    };
    document.querySelector('#btnSetMetadata').addEventListener('click', restartForm);

    document.querySelector('#btnRestartForm').addEventListener('click', restartForm);
    document.querySelector('#btnCloseForm').addEventListener('click', () => formBuilder.hideForm());
    document.querySelector('#btnAbortSubmit').addEventListener('click', () => {
        formBuilder.formSubmitError();
    });
    document.querySelector('#btnFinishSubmit').addEventListener('click', () => {
        formBuilder.hideForm();
    });
    document.querySelector('#btnShowError').addEventListener('click', () => {
        let txt = document.querySelector('#txtErrorMessage');
        if (txt) formBuilder.showError(txt.value);
    });
    document.querySelector('#btnHideError').addEventListener('click', () => {
        formBuilder.showError(null);
    });

    document.querySelector('#btnShowForm').addEventListener('click', () => {
        updateMetadata(exampleMetaData);
        formBuilder.showForm(exampleMetaData);
        exampleMetaData = null;
    });
    document.querySelector('#btnHideForm').addEventListener('click', () => {
        formBuilder.hideForm();
    });
    document.querySelector('#btnGetActiveForm').addEventListener('click', () => {
        let id = formBuilder.getActiveForm();
        console.log('getActiveForm:', id);
    });
    document.querySelector('#btnGetFormData').addEventListener('click', () => {
        let formData = formBuilder.getFormData();
        console.log('getFormData:', formData);
    });

    document.querySelector('#btnGetVersion').addEventListener('click', () => {
        let v = formBuilder.getVersion();
        let str = 'Form Builder version:\n\n' + JSON.stringify(v, null, 2);
        alert(str);
        console.log('getVersion:', v);
    });

    let setAlign = (align) => {
        let ff = formBuilder.querySelector('.form-container');
        if (ff) {
            ff.classList.remove('align-left', 'align-center', 'align-right');
            ff.classList.add('align-' + align);
        }
    };

    document.querySelector('#btnAlignLeft').addEventListener('click', () => setAlign('left'));
    document.querySelector('#btnAlignCenter').addEventListener('click', () => setAlign('center'));
    document.querySelector('#btnAlignRight').addEventListener('click', () => setAlign('right'));

    document.querySelector('#btnGetTarget').addEventListener('click', () => {
        let target = formBuilder.getTarget();
        console.log('target', target);
    });

    document.querySelector('#btnGetSize').addEventListener('click', () => {
        let size = formBuilder.getSize();
        console.log('size', size);
    });

    document.querySelector('#btnHideBack').addEventListener('click', () => formBuilder.showBackground(false));
    document.querySelector('#btnShowBack').addEventListener('click', () => formBuilder.showBackground(true));

    document.querySelector('#btnDestroy').addEventListener('click', () => {
        console.log('Destroy FB');
        formBuilder.destroy();
        console.log('FB has been destroyed');
    });

    __exampleMetaData = {
        globalConfig: {
            name: 'sergey-test-001',
            version: '1',
            styles: {
                bgColor: '#fff',
                alignment: 'right',
                font: {
                    family: 'Arial',
                    size: 14,
                    color: '#73c7ea',
                },
            },
            links: {
                privacy: {
                    title: 'Privacy Policy',
                    src: 'https://anyclip.com/?source=powered&wid=001w000001flCw2AAE_M14701301',
                },
                termsConditions: {
                    isHidden: false,
                    title: 'Terms & Conditions',
                    src: 'https://anyclip.com/?source=powered&wid=001w000001flCw2AAE_M14701301',
                },
            },
        },
        widget: {
            component: 'Container',
            props: [
                {
                    columns: 12,
                    order: 1,
                    widget: {
                        component: 'Container',
                        props: [
                            {
                                columns: 12,
                                order: 1,
                                widget: {
                                    component: 'Logo',
                                    props: {
                                        src: 'https://cdn4.anyclip.com/form/5f/5f3b75d8809ccd1f_1653571604225.jpeg',
                                    },
                                },
                            },
                            {
                                columns: 12,
                                order: 2,
                                widget: {
                                    component: 'Title',
                                    props: {
                                        text: '<p>Titlefgddddddddddddddddddddddddddddddddd</p>',
                                    },
                                },
                            },
                            {
                                columns: 12,
                                order: 3,
                                widget: {
                                    component: 'Description',
                                    props: {
                                        text: '<p><span style="color: rgb(82, 181, 213); font-size: 23px;"><strong>ggsdgsgsdgsdgnhfghfg_fghfg fghbhhhh fgh fg fhfg hfgh&nbsp;</strong></span></p>',
                                    },
                                },
                            },
                        ],
                    },
                },
                {
                    columns: 12,
                    order: 2,
                    widget: {
                        component: 'Form',
                        props: {
                            skipButton: {
                                isHidden: false,
                                title: 'Skip',
                                bgColor: '',
                                textColor: '#2869d1',
                            },
                            submitButton: {
                                title: 'Submit',
                                bgColor: '#2869d1',
                                textColor: '#fff',
                            },
                            fields: [
                                {
                                    columns: '12',
                                    order: 0,
                                    widget: {
                                        component: 'TextField',
                                        props: {
                                            type: 'text',
                                            name: 'name-1653565348942',
                                            placeholder: 'Lebbjbjsg',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: 1,
                                    widget: {
                                        component: 'TextField',
                                        props: {
                                            type: 'text',
                                            name: 'name-1653565541259',
                                            placeholder: 'dsfasdfasdf',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: 2,
                                    widget: {
                                        component: 'SelectField',
                                        props: {
                                            name: 'name-1653565543059',
                                            label: 'sdgsgsdg',
                                            options: [
                                                {
                                                    value: 1,
                                                    label: '1',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 2,
                                                    label: '2',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 3,
                                                    label: '3',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 4,
                                                    label: '4',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 5,
                                                    label: '5',
                                                    isSelected: false,
                                                },
                                            ],
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: 3,
                                    widget: {
                                        component: 'RadioField',
                                        props: {
                                            name: 'name-1653565559758',
                                            label: 'QQQQQQQQQQQQQQQQQQQQQQ',
                                            options: [
                                                {
                                                    value: 1,
                                                    label: '1',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 2,
                                                    label: '2',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 3,
                                                    label: '3',
                                                    isSelected: true,
                                                },
                                                {
                                                    value: 4,
                                                    label: '4',
                                                    isSelected: false,
                                                },
                                            ],
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: 4,
                                    widget: {
                                        component: 'EmailField',
                                        props: {
                                            type: 'email',
                                            name: 'name-1653565582069',
                                            placeholder: 'asas@ddd.com',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: 5,
                                    widget: {
                                        component: 'CheckboxOptionsField',
                                        props: {
                                            name: 'name-1653565705729',
                                            label: 'tetttttsttstsst',
                                            options: [
                                                {
                                                    value: 1,
                                                    label: '1',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 2,
                                                    label: '2',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 3,
                                                    label: '3',
                                                    isSelected: false,
                                                },
                                                {
                                                    value: 4,
                                                    label: '4',
                                                    isSelected: false,
                                                },
                                            ],
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                            ],
                        },
                    },
                },
            ],
        },
    };

    exampleMetaData = {
        globalConfig: {
            id: '2822144012',
            name: 'Feedback',
            version: '1',
            styles: {
                bgColor: 'white',
                alignment: 'left',
                font: {
                    family: 'Roboto',
                    family1: 'Courier New',
                    family2: 'Times New Roman',
                    family3: 'Cursive',
                    color: '#666666',
                    color1: '#000088',
                    color2: '#008800',
                    size: 14,
                    size1: 16,
                    size2: 20,
                },
                links: {
                    privacy: {
                        title: 'Privacy Policy',
                        src: 'https://anyclip.com/privacy-policy/',
                    },
                    termsConditions: {
                        isHidden: false,
                        title: 'Terms & Conditions',
                        src: 'https://anyclip.com/terms-conditions/',
                    },
                },
            },
        },
        widget: {
            component: 'Container',
            props: [
                {
                    columns: '12',
                    order: '1',
                    widget: {
                        component: 'Container',
                        props: [
                            {
                                widget: {
                                    component: 'Banner',
                                    props: {
                                        src: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-1280x100.png',
                                        src1: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-1280x160.png',
                                        src2: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-1280x720.png',
                                        src3: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-800x148.png',
                                        src4: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-1244x528.png',
                                        src5: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-226x225.png',
                                        src6: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-50x50.png',
                                        src7: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/banner-example-1280x50.png',
                                    },
                                },
                            },
                            {
                                columns: '12',
                                order: '1',
                                widget: {
                                    component: 'Logo',
                                    props: {
                                        src: 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/fb/logo-wheels-tv.png',
                                    },
                                },
                            },
                            {
                                columns: '12',
                                order: '2',
                                widget: {
                                    component: 'Title',
                                    props: {
                                        text: `Get <span style="color:blue;font-weight:bold">Pre-Approved</span>`,
                                    },
                                },
                            },
                            {
                                columns: '12',
                                order: '3',
                                widget: {
                                    component: 'Description',
                                    props: {
                                        text:
                                            `<p>Thank you for your interest in the <span style="color:red">2022 Acura MDX</span>.</p>` +
                                            `<p>We're_happy_to_help_you_in_any_way_that_we_can,_from_helping_you_find_your_next_car, scheduling ` +
                                            `a test drive, getting you pre-approved, valuing your trade-in, and or going over current specials.</p>` +
                                            `<ul><li>Alpha</li><li>Beta Delta Eta</li><li>One of the following:<ul>` +
                                            `<li>One scandinavian doll</li><li>Two japanese dolls</li><li>Three american dolls</li></ul></li>` +
                                            `<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li></ul>` +
                                            `<p>Quidquid latine dictum sit, altum videtur.</p>` +
                                            `<ol><li>Alpha</li><li>Beta</li><li>Gamma Delta Eta</li></ol>` +
                                            `<p>Please let us know how to reach you and we'll get right back to you.</p>`,
                                    },
                                },
                            },
                        ],
                    },
                },
                {
                    columns: '12',
                    order: '2',
                    widget: {
                        component: 'Form',
                        props: {
                            skipButton: {
                                _isHidden: true,
                                title: 'Skip',
                                bgColor: 'none',
                                textColor: 'blue',
                            },
                            submitButton: {
                                title: 'Get Pre-Approved',
                                bgColor: 'blue',
                                textColor: 'white',
                            },
                            fields: [
                                {
                                    columns: '12',
                                    order: '1',
                                    widget: {
                                        component: 'TextField',
                                        props: {
                                            type: 'text',
                                            name: 'firstName',
                                            placeholder: 'First name',
                                            defaultValue: '',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: 12,
                                    widget: {
                                        component: 'TextField',
                                        props: {
                                            type: 'text',
                                            name: 'lastName',
                                            placeholder: 'Last name WWWWWWWWWWWWWWWWWWWWWWWWW',
                                            defaultValue: '',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    widget: {
                                        component: 'DateField',
                                        props: {
                                            name: 'date',
                                            label: 'Select date',
                                            defaultValue: '',
                                            day: {
                                                label: 'Day',
                                            },
                                            month: {
                                                label: 'Month',
                                            },
                                            year: {
                                                label: 'Year',
                                            },
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: 12,
                                    widget: {
                                        component: 'TextField',
                                        props: {
                                            type: 'email',
                                            name: 'email',
                                            placeholder: 'Enter email WWWWWWWWWWWWWWWWWWWWWWWWW',
                                            defaultValue: '',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: 12,
                                    widget: {
                                        component: 'TextAreaField',
                                        props: {
                                            name: 'comments',
                                            placeholder: 'Comments WWWWWWWWWWWWWW WWWWWWWWWWWWW WWWWWWWWWWWWWWWW WWWWWWW',
                                            defaultValue: '',
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    widget: {
                                        component: 'RadioField',
                                        props: {
                                            name: 'radio',
                                            label: 'Select Radio Button WWWWWWWWWWWWWWWWWWWWWWWWW',
                                            defaultValue: '',
                                            options: [
                                                {
                                                    value: 1,
                                                    label: 'Option 1 WWWWW WWWWWWWWWWWWWWWWWWWW',
                                                },
                                                {
                                                    value: 2,
                                                    label: 'Option 2 WWWWWWWWWWW WWWWWWWWWWWWWW',
                                                },
                                                {
                                                    value: 3,
                                                    _isSelected: true,
                                                    label: 'Option 3 WWWWWWWWWWWWWWW WWWWWWWWWW',
                                                },
                                            ],
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: 12,
                                    widget: {
                                        component: 'SelectField',
                                        props: {
                                            name: 'select',
                                            label: 'Choose Option WWWWW WWWWWWWWWWWW WWWWWWWW',
                                            options: [
                                                {
                                                    value: 1,
                                                    label: 'Option 1 WWWW WWWWWWWWWWWWWW WWWWWWW',
                                                },
                                                {
                                                    value: 2,
                                                    _isSelected: true,
                                                    label: 'Option 2 WWWWWWWWWW WWWWWWWWWWWWWWW',
                                                },
                                                {
                                                    value: 3,
                                                    label: 'Option 3 WWWWWWWWWWWWWWWW WWWWWWWWW',
                                                },
                                            ],
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                                {
                                    columns: 12,
                                    widget: {
                                        component: 'CheckboxOptionsField',
                                        props: {
                                            name: 'select',
                                            label: 'Choose Option WWWWWWWW WWW WWWWWWWWWWWWWW',
                                            options: [
                                                {
                                                    value: 1,
                                                    label: 'Option 1 WWWWWWWWWWWW WWWW WWWWWWWWW',
                                                    _isSelected: true,
                                                },
                                                {
                                                    value: 2,
                                                    label: 'Option 2 WWWWWW WWWWWWWWWWWWWW WWWWW',
                                                    _isSelected: true,
                                                },
                                                {
                                                    value: 3,
                                                    label: 'Option 3 WWWWWWWW WWWW WWWWWWWWWWWWW',
                                                    _isSelected: true,
                                                },
                                            ],
                                            validation: {
                                                required: {
                                                    value: true,
                                                    errMsg: 'Is required',
                                                },
                                            },
                                        },
                                    },
                                },
                            ],
                        },
                    },
                },
            ],
        },
    };
})();
