const checkFrameSize = () => {
    const doc = document.documentElement;
    const vv = {
        left: window.scrollX,
        top: window.scrollY,
        docClientWidth: doc.clientWidth,
        docClientHeight: doc.clientHeight,
        windowInnerWidth: window.innerWidth,
        windowInnerHeight: window.innerHeight,
    };

    const obj = doc.querySelector('iframe');
    const rect = obj.getBoundingClientRect();

    console.log(vv, rect);

    const union = {
        left: Math.max(rect.left, 0),
        top: Math.max(rect.top, 0),
        right: Math.min(rect.right, doc.clientWidth),
        bottom: Math.min(rect.bottom, doc.clientHeight),
    };

    console.log(union);

    const squareUnion = (union.right - union.left) * (union.bottom - union.top);
    const squareObj = rect.width * rect.height;
    const percentage = Math.max(0, squareUnion) / squareObj * 100;

    console.log(squareUnion, squareObj, percentage);
}
