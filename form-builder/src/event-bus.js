/**
 * Event bus.
 * In future event bus may to support sync/async events execution, if needed.
 */
export class EventBus {
    constructor() {
        /**
         * Contains all subscriptions
         * @private
         * @type {object}
         */
        this._topics = {}
    }

    /**
     * Validate topic.
     * Now we checking topic type only, in future an additional checking may be added
     * @param {string} topic Topic to validate
     * @returns {boolean} Returns true if topic is valid, false otherwise
     * @private
     */
    _isTopicValid(topic) {
        return typeof topic === 'string';
    }

    /**
     * Subscribe to an event
     * @param {function} callback Callback function
     * @param {string} topic Event name
     * @param {object} [context] (optional) Context to run callback function
     * @param {number} [priority] (optional) Callback execution priority. Default value: 10
     */
    subscribe(callback, topic, context, priority) {
        //validate topic value
        if (!this._isTopicValid(topic)) {
            throw new Error(
                'You must provide a valid topic to create a subscription.'
            );
        }

        //normalise priority value (by default: 10)
        priority = priority || 10;

        //build subscription info
        var subscriptionInfo = {
            callback: callback,
            context: context,
            priority: priority,
        };

        // create the topic if not yet created
        if (!_topics[topic]) {
            _topics[topic] = [];
        }

        // add the subscription info
        if (!_topics[topic].length) {
            // if subscription data added for the first time
            _topics[topic].push(subscriptionInfo);
        } else {
            // if other subscriptions for this topic already exists
            var subscriptionAdded = false;
            // loop over all existing subscriptions and add new subscription according to priority
            for (var i = _topics[topic].length - 1; i >= 0; i--) {
                if (_topics[topic][i].priority <= priority) {
                    _topics[topic].splice(i + 1, 0, subscriptionInfo);
                    subscriptionAdded = true;
                    break;
                }
            }

            // if new subscription was not added because it have higher priority - add it
            // to the beginning of subscriptions array
            if (!subscriptionAdded) {
                _topics[topic].unshift(subscriptionInfo);
            }
        }
    }

    /**
     * Unsubscribe from an event
     * @param {string} topic Event name
     * @param {function} callback Callback function to unsubscribe
     */
    unsubscribe(callback, topic) {
        //validate topic value
        if (!this._isTopicValid(topic)) {
            throw new Error(
                'You must provide a valid topic to remove a subscription.'
            );
        }

        //if reference is null - remove all existing subscriptions
        if (!callback) {
            //remove subscription element
            if (_topics[topic]) {
                delete _topics[topic];
            }
        } else {
            //remove specific subscription
            if (_topics[topic]) {
                var length = _topics[topic].length,
                    counter = 0;
                for (; counter < length; counter++) {
                    if (_topics[topic][counter].callback === callback) {
                        //remove found subscription
                        _topics[topic].splice(counter, 1);

                        // Adjust counter and length after item has been removed
                        counter--;
                        length--;
                    }
                }
            }
        }
    }

    /**
     * Run callback function on event
     * @param {string} topic Event name
     * @param {object} [data] (optional) Data passed to callback function
     */
    publish(topic, data) {
        //validate topic value
        if (!_isTopicValid(topic)) {
            throw new Error('You must provide a valid topic to publish.');
        }

        //get args as array (excluding 'topic')
        var args = [].slice.call(arguments, 1);

        // return if the topic doesn't exist, or there are no listeners
        if (!_topics[topic] || _topics[topic].length < 1) {
            return;
        }

        // send the event to all listeners
        var i = 0;
        _topics[topic].forEach(function (subscription) {
            try {
                subscription.callback.apply(subscription.context, args);
            } catch (e) {
                console.error('Subscribed function throws error: ' + e);
            }
        });
    }
}
