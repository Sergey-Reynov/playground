/**
 * @callback FrameMessageListener
 * @param {string} type
 * @param {Object} data
 * @returns {void}
 */

export class ExternalModuleConnector {
    /**
     * @param {HTMLDivElement} holderElement 
     * @param {string} src 
     * @param {Object} attributes 
     */
    constructor(holderElement, src, attributes) {
        // let moduleAttributes = Object.keys(attributes || {})
        //     .map(key => `${key}="${attributes[key]}"`);

        /** @type {HTMLIFrameElement} */
        this.frame = document.createElement('iframe');
        // this.frame.srcdoc = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">
        //     <meta http-equiv="X-UA-Compatible" content="IE=edge">
        //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
        //     <title>ac-lre-player-module</title>
        //     <script src="${src}" ${moduleAttributes.join(' ')}></script>
        //     </head><body></body></html>`;
        Object.keys(attributes).forEach(key => this.frame.setAttribute(key, attributes[key]));
        this.frame.src = src;

        holderElement.appendChild(this.frame);
        this.listeners = [];

        window.addEventListener('message', event => {
            let { source, data } = event;
            if (source === this.frame.contentWindow) {
                this.listeners.forEach(listener => listener(data.type, data.data));
                console.log('%cModule sent:', 'color:blue;background:violet', data.type, data.data);
            }
        });
    }

    /**
     * @param {string} type 
     * @param {Object} data 
     */
    postMessage(type, data) {
        console.log('%cHost sending:', 'color:white;background:green', { type, data });
        this.frame.contentWindow.postMessage({ type, data });
    }

    /**
     * @param {FrameMessageListener} listener 
     */
    addMessageListener(listener) {
        if (listener && this.listeners.indexOf(listener) < 0) {
            this.listeners.push(listener);
        }
    }

    removeMessageListener(listener) {
        let idx = this.listeners.indexOf(listener);
        if (idx >= 0) this.listeners.splice(idx, 1);
    }

    destroy() {
        if (this.frame) {
            this.listeners = [];
            this.postMessage('destroy');
            this.frame.parentElement.removeChild(this.frame);
            this.frame = null;
        }
    }
}
