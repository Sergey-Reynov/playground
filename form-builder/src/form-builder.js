export class FormBuilder {
    constructor() {}

    createForm(formMetadata) {
        console.log('create form', formMetadata);
        let cfg = this.getConfig(formMetadata);
        console.log('create form', cfg);
        if (!cfg) return null;

        let tokens = [this.beginFormContainer(cfg)];
        this.addFormHeader(tokens, cfg.static.props);
        this.addFormFields(tokens, cfg.form.props.fields);
        this.addFormButtons(tokens, cfg.form.props);
        this.addFormLinks(tokens, cfg.global.styles?.links || {});
        tokens.push(`</div>`);

        return tokens.join('');
    }

    getConfig(formMetadata) {
        let cfgWidget = formMetadata?.widget;
        if (!cfgWidget) return null;
        if (cfgWidget.component != 'Container') return null;

        let cfg = {
            static: {},
            form: {},
            global: formMetadata.globalConfig || {},
        };

        for (let idx in cfgWidget.props) {
            let wgt = cfgWidget.props[idx].widget || {};
            if (wgt.component === 'Container') cfg.static = wgt;
            else if (wgt.component === 'Form') cfg.form = wgt;
        }

        return cfg;
    }

    beginFormContainer(cfg) {
        let tokens = [`<div`];
        let classes = ['ac-lre-form-container'];
        let styles = [];
        let cfgStyles = cfg.global.styles || {};
        if (cfgStyles.alignment) classes.push(`align-${cfgStyles.alignment}`);
        if (cfgStyles.bgColor) styles.push(`background:${cfgStyles.bgColor}`);
        if (cfgStyles.font) {
            if (cfgStyles.font.family) {
                styles.push(`font-family: '${cfgStyles.font.family}'`);
            }
            if (cfgStyles.font.size) {
                styles.push(`font-size: ${cfgStyles.font.size}px`);
            }
            if (cfgStyles.font.color) {
                styles.push(`color: ${cfgStyles.font.color}`);
            }
        }
        tokens.push(`style="${styles.join(';')}"`);
        tokens.push(`class="${classes.join(' ')}"`);
        tokens.push(`data-id="${cfg.global.id}"`);
        tokens.push(`data-name="${cfg.global.name}"`);
        tokens.push(`data-version="${cfg.global.version}"`);
        tokens.push('>');
        return tokens.join(' ');
    }

    addFormHeader(tokens, props) {
        tokens.push(`<div class="ac-lre-form-slot ac-lre-form-header">`);
        for (let idx in props) {
            let token = this.createStaticWidget(props[idx].widget);
            if (token) tokens.push(token);
        }
        tokens.push(`</div>`);
    }

    addFormButtons(tokens, props) {
        tokens.push(`<div class="ac-lre-form-slot ac-lre-form-buttons">`);
        tokens.push(`<div class="ac-lre-form-button-list">`);
        tokens.push(this.createFormButton(props.skipButton, 'btnSkip'));
        tokens.push(this.createFormButton(props.submitButton, 'btnSubmit'));
        tokens.push(`</div></div>`);
    }

    createFormButton(metadata, id) {
        let styles = [];
        if (metadata.textColor) styles.push('color:' + metadata.textColor);
        if (metadata.bgColor) styles.push('background:' + metadata.bgColor);
        if (metadata.isHidden) styles.push('visibility: hidden');
        return `<button id="${id}" style="${styles.join(';')}">${
            metadata.title
        }</button>`;
    }

    addFormFields(tokens, fields) {
        tokens.push(`<div class="ac-lre-form-slot ac-lre-form-data">`);
        for (let idx in fields) {
            let token = this.createFormField(fields[idx].widget);
            if (token) tokens.push(token);
        }
        tokens.push(`</div>`);
    }

    createFormField(metadata) {
        switch (metadata.component) {
            case 'TextField':
                return this.createTextField(metadata.props);
            case 'TextAreaField':
                return this.createTextAreaField(metadata.props);
            case 'DateField':
                return this.createDateField(metadata.props);
            case 'RadioField':
                return this.createRadioField(metadata.props);
            case 'SelectField':
                return this.createSelectField(metadata.props);
            case 'CheckboxField':
                return this.createCheckboxField(metadata.props);
        }
        return null;
    }

    createTextField(metadata) {
        return `<div class="ac-lre-field-text">
            <label for="${metadata.name}">${metadata.placeholder}</label>
            <input type="text" id="${metadata.name}" name="${
            metadata.name
        }" value="${metadata.defaultValue || ''}">
        </div>`;
    }

    createTextAreaField(metadata) {
        return `<div class="ac-lre-field-text-area">
            <label for="${metadata.name}">${metadata.placeholder}</label>
            <textarea rows="4" id="${metadata.name}" name="${metadata.name}">${
            metadata.defaultValue || ''
        }</textarea>
        </div>`;
    }

    createDateField(metadata) {
        return `<div class="ac-lre-field-date">
            <label for="${metadata.name}">${
            metadata.label
        } [<b>TODO: customize view</b>]</label>
            <input type="date" id="${metadata.name}" name="${
            metadata.name
        }" value="${metadata.defaultValue || ''}">
        </div>`;
    }

    createRadioField(metadata) {
        let id = metadata.name;
        let tokens = [
            `<div class="ac-lre-field-radio">`,
            `<div>${metadata.label}</div>`,
        ];
        Array.from(metadata.options || []).forEach((option, index) => {
            let optionId = `${id}-${option.value}`;
            tokens.push(`${index > 0 ? '<br>' : ''}`);
            tokens.push(
                `<input type="radio" id="${optionId}" name="${id}" value="${
                    option.value
                }" ${option.isSelected ? 'checked' : ''}>`
            );
            tokens.push(`<label for="${optionId}">${option.label}</label>`);
        });
        tokens.push(`</div>`);
        return tokens.join('');
    }

    createSelectField(metadata) {
        let tokens = [
            `<div class="ac-lre-field-select">`,
            `<label for="${metadata.name}">${metadata.label}</label>`,
            `<select id="${metadata.name}" name="${metadata.name}">`,
        ];
        Array.from(metadata.options || []).forEach((option) => {
            tokens.push(
                `<option value="${option.value}">${option.label}</option>`
            );
        });
        tokens.push(`</select></div>`);
        return tokens.join('');
    }

    createCheckboxField(metadata) {
        let id = `${metadata.name}`;
        let tokens = [
            `<div class="ac-lre-field-checkboxes">`,
            `<label for="checks-${id}">${metadata.label}</label>`,
            `<div id="checks-${id}">`,
        ];
        Array.from(metadata.options || []).forEach((option, index) => {
            let optionId = `${id}-${option.value}`;
            tokens.push(`${index > 0 ? '<br>' : ''}`);
            tokens.push(
                `<input type="checkbox" id="${optionId}" ${
                    option.isSelected ? 'checked' : ''
                }>`
            );
            tokens.push(`<label for="${optionId}">${option.label}</label>`);
        });
        tokens.push(`</div></div>`);
        return tokens.join('');
    }

    addFormLinks(tokens, links) {
        tokens.push(`<div class="ac-lre-form-slot ac-lre-form-links">`);
        tokens.push(this.createStaticLink(links.privacy));
        tokens.push(this.createStaticLink(links.termsConditions));
        tokens.push(`</div>`);
    }

    createStaticLink(link) {
        if (!link) return null;
        return `<div style="visibility:${
            link.isHidden ? 'hidden' : 'visible'
        }"><a href="${link.src}" target="_blank">${link.title}</a></div>`;
    }

    createStaticWidget(metadata) {
        if (metadata) {
            switch (metadata.component) {
                case 'Logo':
                    return this.createLogo(metadata);
                case 'Title':
                    return this.createTitle(metadata);
                case 'Description':
                    return this.createDescription(metadata);
            }
        }
        return null;
    }

    createLogo(metadata) {
        return `<img class="ac-lre-form-logo" src="${metadata.props.src}">`;
    }

    createTitle(metadata) {
        return `<div class="ac-lre-form-title">${metadata.props.text}</div>`;
    }

    createDescription(metadata) {
        return `<div class="ac-lre-form-description">${metadata.props.text}</div>`;
    }

    getExampleMetadata() {
        return {
            globalConfig: {
                id: '2822144012',
                name: 'Feedback',
                version: '1',
                styles: {
                    bgColor: 'gainsboro',
                    _alignment: 'center',
                    font: {
                        family: 'Roboto',
                        size: 14,
                        color: 'darkgreen',
                    },
                    links: {
                        privacy: {
                            title: 'Privacy Policy',
                            src: 'https://anyclip.com/privacy-policy/',
                        },
                        termsConditions: {
                            isHidden: false,
                            title: 'Terms & Conditions',
                            src: 'https://anyclip.com/terms-conditions/',
                        },
                    },
                },
            },
            widget: {
                component: 'Container',
                props: [
                    {
                        columns: '12',
                        order: '1',
                        widget: {
                            component: 'Container',
                            props: [
                                {
                                    columns: '12',
                                    order: '1',
                                    widget: {
                                        component: 'Logo',
                                        props: {
                                            src: 'https://anyclip.com/wp-content/uploads/2021/05/AnyClip_Logo_2021-05-06-2021_Primary_Black.png',
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: '2',
                                    widget: {
                                        component: 'Title',
                                        props: {
                                            text: 'Contact Us MaxNumber',
                                        },
                                    },
                                },
                                {
                                    columns: '12',
                                    order: '3',
                                    widget: {
                                        component: 'Description',
                                        props: {
                                            text: "We're happy to help you in any way that we can, from helping you find your next car",
                                        },
                                    },
                                },
                            ],
                        },
                    },
                    {
                        columns: '12',
                        order: '2',
                        widget: {
                            component: 'Form',
                            props: {
                                skipButton: {
                                    isHidden: true,
                                    title: 'Skip',
                                    bgColor: 'none',
                                    textColor: 'blue',
                                },
                                submitButton: {
                                    title: 'Submit',
                                    bgColor: 'blue',
                                    textColor: 'white',
                                },
                                fields: [
                                    {
                                        columns: '12',
                                        order: '1',
                                        widget: {
                                            component: 'TextField',
                                            props: {
                                                type: 'text',
                                                name: 'firstName',
                                                placeholder: 'First name',
                                                defaultValue: '',
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                    {
                                        columns: '12',
                                        widget: {
                                            component: 'DateField',
                                            props: {
                                                name: 'date',
                                                label: 'Select date',
                                                defaultValue: '',
                                                day: {
                                                    label: 'Day',
                                                },
                                                month: {
                                                    label: 'Month',
                                                },
                                                year: {
                                                    label: 'Year',
                                                },
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                    {
                                        columns: 12,
                                        widget: {
                                            component: 'TextField',
                                            props: {
                                                type: 'text',
                                                name: 'lastName',
                                                placeholder: 'Last name',
                                                defaultValue: '',
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                    {
                                        columns: 12,
                                        widget: {
                                            component: 'TextAreaField',
                                            props: {
                                                name: 'comments',
                                                placeholder: 'Comments',
                                                defaultValue: '',
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                    {
                                        columns: '12',
                                        widget: {
                                            component: 'RadioField',
                                            props: {
                                                name: 'radio',
                                                label: 'Select Radio Button',
                                                defaultValue: '',
                                                options: [
                                                    {
                                                        value: 1,
                                                        isSelected: true,
                                                        label: 'Option 1',
                                                    },
                                                    {
                                                        value: 2,
                                                        label: 'Option 2',
                                                    },
                                                    {
                                                        value: 3,
                                                        label: 'Option 3',
                                                    },
                                                ],
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                    {
                                        columns: 12,
                                        widget: {
                                            component: 'SelectField',
                                            props: {
                                                name: 'select',
                                                label: 'Choose Option',
                                                options: [
                                                    {
                                                        value: 1,
                                                        label: 'Option 1',
                                                    },
                                                    {
                                                        value: 2,
                                                        isSelected: true,
                                                        label: 'Option 2',
                                                    },
                                                    {
                                                        value: 3,
                                                        label: 'Option 3',
                                                    },
                                                ],
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                    {
                                        columns: 12,
                                        widget: {
                                            component: 'CheckboxField',
                                            props: {
                                                name: 'select',
                                                label: 'Choose Option',
                                                options: [
                                                    {
                                                        value: 1,
                                                        label: 'Option 1',
                                                        isSelected: true,
                                                    },
                                                    {
                                                        value: 2,
                                                        label: 'Option 2',
                                                        isSelected: true,
                                                    },
                                                    {
                                                        value: 3,
                                                        label: 'Option 3',
                                                        isSelected: true,
                                                    },
                                                ],
                                                validation: {
                                                    required: {
                                                        value: true,
                                                        errMsg: 'Is required',
                                                    },
                                                },
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    },
                ],
            },
        };
    }
}
