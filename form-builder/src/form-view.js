export class FormView {
    constructor() {

    }

    createElement(type, attributes) {
		let el = document.createElement(type);
		Object.keys(attributes).forEach(key => el[key] = attributes[key]);
		return el;
	}
	
	createPopup(hostElement) {
        let back = hostElement.appendChild(this.createElement('div', {className: 'ac-lre-form-back'}));
        let popup = back.appendChild(this.createElement('div', {className: 'ac-lre-form-popup'}));
		return { back, popup };
	}

	showPopup(popup, isVisible) {
		if (popup && popup.back) {
			popup.back.style.display = isVisible ? 'block' : 'none';
		}
	}

	isPopupVisible(popup) {
		return (popup && popup.back && popup.back.style.display !== 'none');
	}

}
