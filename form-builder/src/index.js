import { LibAPI } from "./lib-api";

let libName = 'ACFormBuilderLibrary';

if (typeof window[libName] === 'undefined') {
    window[libName] = LibAPI;

    // TODO integrate CSS using css loader (?)
    let css = document.head.appendChild(document.createElement('link'));
    css.rel = 'stylesheet';
    css.href = document.currentScript.src.replace('der.js', 'der.css');
    css.setAttribute('data-owner', libName);
}

console.log(`TYPE [${typeof window[libName]}]`);
window.dispatchEvent(new Event('form-builder-ready'));
