import { EventBus } from './event-bus';
import { FormBuilder } from './form-builder';
import { FormView } from './form-view';

export class LibAPI {
    constructor() {
        this.bus = new EventBus();
        this.builder = new FormBuilder();
        this.view = new FormView();
        this.popup = null;
    }

    createPopup(hostElement) {
        this.popup = this.view.createPopup(hostElement);
    }

    showForm(metadata) {
        console.log('showForm()', metadata);
        if (metadata) {
            // create new form
        }
        this.view.showPopup(this.popup, true);
        return this.view.isPopupVisible(this.popup);
    }

    hideForm() {
        console.log('hideForm()');
        this.view.showPopup(this.popup, false);
    }
    
    closeForm() {
        console.log('closeForm()');
        this.hideForm();
        return false;
    }
    
    isFormActive() {
        console.log('isFormActive()');
        return this.builder.isPopupVisible(this.popup);
    }
    
    getFormData() {
        let formData = {
            id: 'test-id',
        };
        console.log('getFormData()', formData);
        return formData;
    }
    
    /**
     * Subscribe to an event
     * @param {function} callback Callback function
     * @param {string} topic Event name
     * @param {object} [context] (optional) Context to run callback function
     * @param {number} [priority] (optional) Callback execution priority. Default value: 10
     */
    subscribe(callback, topic, context, priority) {
        console.log('subscribe()', callback, topic, context, priority);
        this.bus.subscribe(callback, topic, context, priority);
    }
    
    /**
     * Unsubscribe from an event
     * @param {string} topic Event name
     * @param {function} callback Callback function to unsubscribe
     */
    unsubscribe(callback, topic) {
        console.log('unsubscribe()', type, listener);
        this.bus.unsubscribe(callback, topic);
    }

    destroy() {
        console.log('destroy()');
        this.closeForm();
    }
}
