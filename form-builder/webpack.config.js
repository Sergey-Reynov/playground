const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'lre-form-builder.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
