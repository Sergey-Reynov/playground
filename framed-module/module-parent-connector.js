/**
 * @callback FrameMessageListener
 * @param {string} type
 * @param {Object} data
 * @returns {void}
 */

class ACFmoduleParentConnector {
    constructor() {
        /** @type {FrameMessageListener[]} */
        this.listeners = [];

        // communicator
        this.onWindowMessage = this.onWindowMessage.bind(this);
        window.addEventListener('message', this.onWindowMessage);
    }

    /**
     * @param {MessageEvent} event
     */
    onWindowMessage(event) {
        if (!this.listeners) return;
        let { data } = event;
        console.log('%cmodule received', 'color:white;background:green', data);
        this.listeners.forEach((listener) =>
            listener(data.type, data.data)
        );
    }

    /**
     * @param {string} type
     * @param {Object} data
     */
    postMessage(type, data) {
        if (window.parent && window.parent != window) {
            console.log('%cmodule sending', 'color:white;background:blue', type, data);
            window.parent?.postMessage({ type, data });
        }
    }

    /**
     * @param {FrameMessageListener} listener
     */
    addMessageListener(listener) {
        if (!this.listeners) return;
        if (listener && this.listeners.indexOf(listener) < 0) {
            this.listeners.push(listener);
        }
    }

    removeMessageListener(listener) {
        if (!this.listeners) return;
        let idx = this.listeners.indexOf(listener);
        if (idx >= 0) this.listeners.splice(idx, 1);
    }

    destroy() {
        if (!this.listeners) return;
        this.listeners = null;
        window.removeEventListener('message', this.onWindowMessage);
    }
}
