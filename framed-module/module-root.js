/** @type {FrameMessageListener} */
function exampleListener(type, data) {
    console.log('module - example listener', type, data);
}

// store reference here
var connector = new ACFmoduleParentConnector();
connector.addMessageListener(exampleListener);

var simpleTextFieldTemplate = '';
(function () {
    let tft = document.querySelector('[data-template-type="TEXT-FIELD"]');
    if (tft) {
        simpleTextFieldTemplate = tft.outerHTML;
        tft.parentElement.removeChild(tft);
        console.log('tft', simpleTextFieldTemplate);
    }
})();

function createSimpleTextField(id, label, initialValue) {
    let div = document.createElement('div');
    div.insertAdjacentHTML('afterbegin', simpleTextFieldTemplate
        .replace(/\[ID\]/g, id || 'tf_id')
        .replace(/\[LABEL\]/g, label || '')
        .replace(/\[VALUE\]/g, initialValue || '')
    );
    return div.removeChild(div.firstChild);
}

function createLabelExamples() {
    if (!simpleTextFieldTemplate) return;
    let labels = ['alpha', 'beta', 'gamma'];
    let section = document.querySelector('form');
    labels.forEach(key => {
        let label = createSimpleTextField(
            'id_' + key, 
            'Label for the ' + key, 
            'Initial value of the ' + key
        );
        section.appendChild(label);
    });
}

// initialize by minimum timeout - structure will be ready
setTimeout(() => {
    // continue initialization...

    createLabelExamples();

    // report to parent - module loaded
    connector.postMessage('loaded', { text: 'Hello' });
});

let xx = document.querySelector('.publisher-area');
xx.insertAdjacentHTML('afterbegin', `<p style="font-weight:bold;">Start at ${new Date().toLocaleString()}</p>`);

let formContainer = document.querySelector('.ac-lre-form-container');
let formAligns = ['align-left', 'align-center', 'align-right'];

let buttons = [
    document.querySelector('#btnA'),
    document.querySelector('#btnB'),
    document.querySelector('#btnC'),
];

buttons.forEach(btn => {
    btn.addEventListener('click', e => {
        let align = String(btn.dataset.align).toLowerCase();
        if (align) {
            formContainer.classList.remove(...formAligns);
            formContainer.classList.add('align-' + align);
        }

        let dt = btn.dataset.send;
        if (dt) {
            connector.postMessage('buttonClick', { button: btn.id, data: dt });
        }
    })
});
