(function () {
    const playground = window._____lrePlayground = window._____lrePlayground || {}

    const context = {
        playerUrl: 'lre/lre.js',
        mode: null,
        trigger: null,
        sessionData: null,
        controls: null,
    }
    const optimalWidth = 800
    
    const parseBool = strValue => {
        return (strValue && strValue.toLowerCase() == 'true') ? true : false
    }

    const loadStorage = () => {
        let str = localStorage.getItem('lre-redirect')
        return JSON.parse(str) || { enabled: true }
    }

    const saveStorage = () => {
        localStorage.setItem('lre-redirect', JSON.stringify(context.sessionData || {}))
    }

    const setupContext = () => {
        context.sessionData = loadStorage()
        
        context.sessionData.pubname = context.sessionData.pubname || 'usnewscom'
        context.sessionData.widgetname = context.sessionData.widgetname || '0011r00002IjDWF_725'
        context.sessionData.page = context.sessionData.page || 'https://usnews.com'
        context.sessionData.modetm = context.sessionData.modetm || 'prod'
        context.sessionData.modeconfig = context.sessionData.modeconfig || context.sessionData.modetm
        context.sessionData.modemp = context.sessionData.modemp || context.sessionData.modetm
        context.sessionData.enabled = context.sessionData.enabled || false
        context.sessionData.redirect = context.sessionData.redirect || 'https://anyclip.com'
        context.sessionData.same = context.sessionData.same || false

        if (typeof URLSearchParams !== 'undefined') {
            const params = new URLSearchParams(window.location.search)
            
            context.playerUrl = params.get('js') || context.playerUrl
            context.trigger = params.get('----anyclip-lre-config') || context.trigger

            context.sessionData.pubname = params.get('pubname') || context.sessionData.pubname
            context.sessionData.widgetname = params.get('widgetname') || context.sessionData.widgetname
            context.sessionData.modetm = params.get('mode') || context.sessionData.modetm
            context.sessionData.modeconfig = params.get('mode') || context.sessionData.modeconfig
            context.sessionData.modemp = params.get('mode') || context.sessionData.modemp
        }
    }

    const initPanel = () => {
        context.controls = {
            publishers: document.querySelector('#select-publishers'),
            widgets:    document.querySelector('#select-widgets'),
            pubname:    document.querySelector('#text-pubname'),
            widgetname: document.querySelector('#text-widgetname'),
            page:       document.querySelector('#text-page-url'),
            modetm:     document.querySelector('#select-mode-tm'),
            modeconfig: document.querySelector('#select-mode-config'),
            modemp:     document.querySelector('#select-mode-mp'),
            enabled:    document.querySelector('#check-redirect-enable'),
            redirect:   document.querySelector('#text-redirect-url'),
            same:       document.querySelector('#check-redirect-same'),
        }

        const modeSelectors = [
            context.controls.modetm, 
            context.controls.modeconfig, 
            context.controls.modemp,
        ]
        modeSelectors.forEach(playground.initSelectModes)

        try {
            playground.initPublisherList(context.controls.publishers)
            updateWidgetList(context.sessionData.pubname, context.sessionData.widgetname)
        } catch (err) {
            console.log('examples - error', err)
        }

        context.controls.pubname.value    = context.sessionData.pubname
        context.controls.widgetname.value = context.sessionData.widgetname
        context.controls.page.value       = context.sessionData.page
        context.controls.modetm.value     = context.sessionData.modetm
        context.controls.modeconfig.value = context.sessionData.modeconfig
        context.controls.modemp.value     = context.sessionData.modemp
        context.controls.enabled.checked  = context.sessionData.enabled
        context.controls.redirect.value   = context.sessionData.redirect
        context.controls.same.checked     = context.sessionData.same

        refreshControlsState()

        context.controls.publishers.addEventListener('open', () => console.log('publishers open'))

        initCombo(context.controls.pubname)
        initCombo(context.controls.widgetname)

        context.controls.pubname.addEventListener('change', onPublisherChanged)
        context.controls.widgetname.addEventListener('change', onExampleChanged)

        for (let key in context.controls) {
            context.controls[key].addEventListener('change', updateSession)
        }
    }

    const initCombo = (comboElement) => {
        let dataKey = 'data-last-value'
        comboElement.addEventListener('click', (event) => {
            let rect = comboElement.getBoundingClientRect()
            let offset = rect.width - event.offsetX
            if (offset < 32) {
                if (comboElement.value) {
                    comboElement.setAttribute(dataKey, comboElement.value)
                    comboElement.value = ''
                }
            }
        })
        comboElement.addEventListener('blur', (event) => {
            if (!comboElement.value) {
                comboElement.value = comboElement.getAttribute(dataKey)
                comboElement.removeAttribute(dataKey)
            }
        })
    }

    const updateWidgetList = (pubname, widgetname) => {
        const widgets = playground.initWidgetList(pubname, context.controls.widgets)
        context.controls.widgetname.value = ''
        context.controls.page.value       = ''
        if (widgetname || widgets.length === 1) {
            updateExample(widgetname || widgets[0])
        }
    }

    const onPublisherChanged = () => {
        updateWidgetList(context.controls.pubname.value, null)
    }

    const onExampleChanged = () => {
        updateExample(context.controls.widgetname.value)
    }
    
    const updateExample = (widgetname) => {
        let exampleData = playground.getWidgetExample(widgetname)
        if (exampleData) {
            context.controls.pubname.value    = exampleData.pubname
            context.controls.widgetname.value = exampleData.widgetname
            context.controls.page.value       = exampleData.page
        }
    }

    const refreshControlsState = () => {
        const isDisabled = !context.sessionData.enabled
        context.controls.redirect.disabled = isDisabled
        context.controls.same.disabled = isDisabled
    }

    const updateSession = () => {
        context.sessionData.pubname    = context.controls.pubname.value
        context.sessionData.widgetname = context.controls.widgetname.value
        context.sessionData.page       = context.controls.page.value
        context.sessionData.modetm     = context.controls.modetm.value
        context.sessionData.modeconfig = context.controls.modeconfig.value
        context.sessionData.modemp     = context.controls.modemp.value
        context.sessionData.enabled    = context.controls.enabled.checked
        context.sessionData.redirect   = context.controls.redirect.value
        context.sessionData.same       = context.controls.same.checked

        refreshControlsState()
        saveStorage()
    }

    const disablePanel = () => {
        for (let key in context.controls) {
            context.controls[key].disabled = true
        }
    }

    const getPlayerPlaceholder = (key) => {
        return document.querySelector(`[data-lre-player-placeholder${key ? `-${key}` : ''}]`)
    }

    const startPlayer = (placeholderKey, sessionId) => {
        const ph = getPlayerPlaceholder(placeholderKey)
        if (!ph) return

        console.log('player:', context.playerUrl)
        console.log('trigger:', context.trigger)

        let sessionData = context.sessionData
        for (let key in sessionData) {
            console.log(key, sessionData[key])
        }

        if (context.trigger) {
            console.log('secret trigger:', context.trigger)
        } else {
            let cfg = null
            if (context.sessionData.modetm || context.sessionData.modeconfig) {
                console.log('TM:', context.sessionData.modetm)
                console.log('config:', context.sessionData.modeconfig)
                console.log('marketplace:', context.sessionData.modemp)
                cfg = playground.getMixedConfig(context.sessionData.modetm, context.sessionData.modeconfig, context.sessionData.modemp)
            }
            cfg = cfg || {}
            cfg.fullScreenButtonToRedirect = {
                enabled: sessionData.enabled,
                url: sessionData.redirect,
                sameTab: sessionData.same,
            }
            window.anyclip = window.anyclip || {}
            window.anyclip.loadConf = cfg
            console.log('config', cfg)
        }

        const scr = document.createElement('script')
        scr.src = context.playerUrl
        scr.setAttribute('pubname', sessionData.pubname)
        scr.setAttribute('widgetname', sessionData.widgetname)
        if (sessionData.page) {
            scr.setAttribute('ourl', encodeURIComponent(sessionData.page))
        }
        if (sessionId) {
            scr.setAttribute('data-osid', sessionId);
        }
        ph.appendChild(scr);

        return ph
    }

    const setupControlsWidth = () => {
        const publisherArea = document.querySelector('.publisher-area')
        const indicator = document.querySelector('#indication-width')
        const getWidth = () => publisherArea.clientWidth
        const updateIndicator = () => indicator.innerHTML = `Width: ${getWidth()}px`
        const setWidth = amount => {
            publisherArea.style.width = `${amount}px`
            updateIndicator()
            context.sessionData.publisherWidth = amount
            saveStorage()
        }
        let defaultWidth = optimalWidth
        const updateDefaultWidth = () => {
            let width = document.body.clientWidth - 20
            defaultWidth = Math.min(optimalWidth, Math.round(Math.floor(width / 100) * 100))
        }
        
        const changeWidth = changeAmount => setWidth(getWidth() + changeAmount)
        document.querySelector('#button-minus-1').addEventListener('click', () => changeWidth(-1))
        document.querySelector('#button-minus-10').addEventListener('click', () => changeWidth(-10))
        document.querySelector('#button-minus-100').addEventListener('click', () => changeWidth(-100))
        document.querySelector('#button-plus-1').addEventListener('click', () => changeWidth(1))
        document.querySelector('#button-plus-10').addEventListener('click', () => changeWidth(10))
        document.querySelector('#button-plus-100').addEventListener('click', () => changeWidth(100))
        indicator.addEventListener('click', () => setWidth(updateDefaultWidth()))

        if (context.sessionData.publisherWidth) {
            defaultWidth = context.sessionData.publisherWidth
        } else {
            updateDefaultWidth()
        }
        console.log('default width:', defaultWidth)
        setWidth(defaultWidth)
        console.log('indication initialized')
    }

    const reloadMe = () => {
        window.location.href = window.location.href
    }

    if (!getPlayerPlaceholder()) return // no placeholder

    setupContext()
    initPanel()

    setupControlsWidth()
    playground.startPlayer = startPlayer

    let btnStart = document.querySelector('#start-player')
    if (btnStart) {
        btnStart.addEventListener('click', () => {
            btnStart.disabled = true
            disablePanel()

            console.log('%cstart player:', 'color: white; background: violet', context)
            startPlayer()
        })
    }

    let btnReload = document.querySelector('#reload')
    if (btnReload) {
        btnReload.addEventListener('click', reloadMe)
    }

    let btnReset = document.querySelector('#reset')
    if (btnReset) {
        btnReset.addEventListener('click', () => {
            context.sessionData = {}
            saveStorage()
            reloadMe()
        })
    }

})()
