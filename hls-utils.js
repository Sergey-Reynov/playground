function createTMResponse(clipUrl, imageUrl, ccUrl) {
    imageUrl = imageUrl || 'https://anyclip-lre-player-qa.s3.amazonaws.com/config/lre_qa_serg/pg/banners/black-248x140.png';
    let plData = {
        playlist: [
            {
                title: `test play video: ${clipUrl}`,
                mediaid: 'ABCDEDFGtestvideo',
                image: imageUrl,
            },
        ],
    };
    let pp = plData.playlist[0];
    if (clipUrl.indexOf('.m3u8')) {
        pp.hlsFile = { file: clipUrl };
    } else {
        pp.file = clipUrl;
    }
    if (ccUrl) {
        pp.ccFiles = [{
            file: ccUrl,
            lang: 'EN',
            langName: 'English',
        }];
        pp.lang = ['EN'];
    }
    return plData;
}

function setClipTo() {
    try { 
        window.__prepareFakeLiveCC();
    } catch (err) {}

    let widget = window.anyclip.getWidget();
    let url = document.querySelector('#clip-url').value;
    widget.setExternalPlaylistResponse(createTMResponse(url));
}

function getPlayer() {
    return window.anyclip.getWidget().player;//.tech({IWillNotUseThisInPlugins: true});
}

function testHLS() {
    let player = getPlayer();
    console.log('player', player);
    window.__player = player;
}
