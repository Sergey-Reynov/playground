(function () {
    var params = {
        // *** set actual selector here
        selector: "__SELECTOR_STRING_HERE__",  //"body > div.publisher-area > p:nth-child(1)",

        // *** set desired width and height of the frame
        width: "__WIDTH_HERE__",   // "500px",
        height: "__HEIGHT_HERE__", // "300px",

        pubname: "__PUBLISHER_NAME_HERE__", // "seekingalphacom",
        widgetname: "__WIDGET_NAME_HERE__", // "0011r00001omdBI_1533",

        // *** set actual page url here (important for TM), or comment to try using current page url
        pageUrl: "__PAGE_URL_HERE__", // "https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil",

        // *** set custom player url here, or leave commented to use default production player (v1)
        // src: "https://player.anyclip.com/anyclip-widget/altenv/other-env/qa/v1/src/lre.js",

        // *** set actual playlist ID here or leave commented if no playlist ID
        // playlistId: "ifmeuzcmljgxctklir4ge2jwgntfkmrq",

        // *** set custom aspect ratio for the video or leave commented for default aspect ratio 16:9
        // aspectRatio: "16:9",

        // *** uncomment next line to hide carousel, even if enabled in the configuration
        // showCarousel: false,

        // *** uncomment next line to show carousel, even if disabled in the configuration
        // showCarousel: true,

        // *** uncomment next line to specify autoplay mode, if need
        // autoplayMode: "in-view",

        // *** uncomment next line to specify hexadecimal background color for the frame (without leading #), if need
        // backgroundColor: "ff8800"
    };

    // **** don't change following code ****
    var frame = document.createElement('iframe');
    frame.id = "aclreplayer";
    if (params.width) frame.width = params.width;
    if (params.height) frame.height = params.height;
    frame.srcdoc = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ac-lre-player</title></head>
        <body style="margin:0px"><div>
        <script id="lre-player-widget" ac-embed-mode="in-iframe"
            src="${params.src || 'https://player.anyclip.com/anyclip-widget/lre-widget/prod/v1/src/lre.js'}"
            pubname="${params.pubname}"
            widgetname="${params.widgetname}"
            ${params.pageUrl ? `ourl="${encodeURIComponent(params.pageUrl)}"` : ''}
            ${params.playlistId ? `data-plid="${params.playlistId}"` : ''}
            ${params.aspectRatio ? `data-ar="${params.aspectRatio}"` : ''}
            ${params.showCarousel != undefined ? `data-cr="${params.showCarousel ? 1 : 0}"` : ''}
            ${params.autoplayMode ? `data-ap="${params.autoplayMode}"` : ''}
            ${params.backgroundColor ? `lre-body-bgc="${params.backgroundColor}"` : ''}
        ></script>
        </div></body></html>`;

    document.querySelector(params.selector).appendChild(document.createElement('div')).appendChild(frame);
})();
