(function () {
    var params = {
        // *** set actual selector here
        selector: "__SELECTOR_STRING_HERE__",  //"body > div.publisher-area > p:nth-child(1)",

        pubname: "__PUBLISHER_NAME_HERE__", // "seekingalphacom",
        widgetname: "__WIDGET_NAME_HERE__", // "0011r00001omdBI_1533",

        // *** set actual page url here (important for TM), or comment to try using current page url
        pageUrl: "__PAGE_URL_HERE__", // "https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil",

        // *** set custom player url here, or leave commented to use default production player (v1)
        // src: "https://player.anyclip.com/anyclip-widget/altenv/other-env/qa/v1/src/lre.js",

        // *** set actual playlist ID here or leave commented if no playlist ID
        // playlistId: "ifmeuzcmljgxctklir4ge2jwgntfkmrq",

        // *** set custom aspect ratio for the video or leave commented for default aspect ratio 16:9
        // aspectRatio: "16:9",

        // *** uncomment next line to hide carousel, even if enabled in the configuration
        // showCarousel: false,

        // *** uncomment next line to show carousel, even if disabled in the configuration
        // showCarousel: true,

        // *** uncomment next line to specify autoplay mode, if need
        // autoplayMode: "in-view",
    };

    // **** don't change following code ****
    var script = document.createElement("script");
    script.src = params.src || 'https://player.anyclip.com/anyclip-widget/lre-widget/prod/v1/src/lre.js';
    script.setAttribute("pubname", params.pubname);
    script.setAttribute("widgetname", params.widgetname);
    if (params.pageUrl) script.setAttribute("ourl", encodeURIComponent(params.pageUrl));
    if (params.playlistId) script.setAttribute("data-plid", params.playlistId);
    if (params.aspectRatio) script.setAttribute("data-ar", params.aspectRatio);
    if (params.showCarousel != undefined) script.setAttribute("data-cr", params.showCarousel ? "1" : "0");
    if (params.autoplayMode) script.setAttribute("data-ap", params.autoplayMode);

    document.querySelector(params.selector).appendChild(document.createElement('div')).appendChild(script);
})();
