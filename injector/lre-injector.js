window.ac_injector = {
    lastParams: {},
    that: null,
    
    inject_lre() {
        that = this;
        let pubname = window.prompt('Enter pubname:');
        if (pubname) {
            let widgetname = window.prompt('Enter widgetname:');
            if (widgetname) {
                that.lastParams = {
                    pubname,
                    widgetname,
                };
                console.log('waiting click', that.lastParams);
                document.body.addEventListener('click', that.inject_lre_click);
            }
        }
    },

    inject_lre_click(event) {
        console.log('inject_lre_click', event);
        document.body.removeEventListener('click', that.inject_lre_click);
        if (that.lastParams) {
            let pp = that.lastParams;
            that.lastParams = null;
            let holder = event.target;
            while (typeof holder.appendChild != 'function') {
                holder = holder.parentElement;
            }
            that.inject_lre_to(holder, pp.pubname, pp.widgetname);
        }
    },

    inject_lre_to_selector(selector, pubname, widgetname, pageUrl, lreUrl) {
        let holder = null;
        if (selector === 'body') holder = document.body;
        else if (selector) holder = document.querySelector(selector);
        if (holder) {
            holder = holder.appendChild(document.createElement('div'));
        }
        holder = holder || document.head;
        that.inject_lre_to(holder, pubname, widgetname, pageUrl, lreUrl);
    },

    inject_lre_to(holder, pubname, widgetname, pageUrl, lreUrl) {
        let ss = document.createElement('script');
        ss.src = lreUrl || 'https://player.anyclip.com/anyclip-widget/lre-widget/prod/v1/src/lre.js';
        if (pubname) ss.setAttribute('pubname', pubname);
        if (widgetname) ss.setAttribute('widgetname', widgetname);
        if (pageUrl) ss.setAttribute('ourl', encodeURIComponent(pageUrl));
        holder.appendChild(ss);
    },
};
