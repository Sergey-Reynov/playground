let incomeTaxes = [
    {
        yearTo: 2022,
        point: 223,
        points: 2.25,
        stairs: [
            { salary:  5300, tax: 10, stair:     0.00 },
            { salary:  6450, tax: 14, stair:   115.00 },
            { salary:  9240, tax: 20, stair:   505.60 },
            { salary: 14840, tax: 31, stair:  1625.60 },
            { salary: 20620, tax: 35, stair:  3417.40 },
            { salary: 42910, tax: 47, stair: 11218.90 },
            { salary: 55270, tax: 50, stair: 17028.10 },
        ],
    },
    {
        yearFrom: 2023,
        point: 235,
        points: 2.25,
        stairs: [
            { salary:  5300, tax: 10, stair:     0.00 },
            { salary:  6790, tax: 14, stair:   149.00 },
            { salary:  9730, tax: 20, stair:   560.60 },
            { salary: 15620, tax: 31, stair:  1738.60 },
            { salary: 21710, tax: 35, stair:  3626.50 },
            { salary: 45180, tax: 47, stair: 11841.00 },
            { salary: 58190, tax: 50, stair: 17955.70 },
        ],
    },
];
