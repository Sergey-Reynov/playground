(function() {
    /** @type {HTMLElement} */
    let container = document.querySelector('.box-container');
    /** @type {HTMLElement} */
    let reportPos = container.querySelector('.box-pos');
    /** @type {HTMLElement} */
    let reportBox = container.querySelector('.box-events');
    /** @type {HTMLTextAreaElement} */
    let report = reportBox.querySelector('textarea');

    let textTokens = [];

    const getPosition = () => {
        return {
            x: parseFloat(container.style.getPropertyValue('--pos-x') || 0),
            y: parseFloat(container.style.getPropertyValue('--pos-y') || 0),
        };
    };

    const updateReport = () => {
        let pos = getPosition();
        reportPos.innerHTML = `[${pos.x},${pos.y}]`;
        report.value = textTokens.join('\n');
        report.scrollTop = report.scrollHeight;
    };

    const move = (dx, dy) => {
        textTokens = [];

        let pos = getPosition();
        console.log('move', pos, dx, dy);

        container.style.setProperty('--pos-x', `${pos.x + (dx || 0)}px`);
        container.style.setProperty('--pos-y', `${pos.y + (dy || 0)}px`);

        updateReport();
    };

    container.querySelector('#btnUp').addEventListener('click', () => move(0, -10));
    container.querySelector('#btnDown').addEventListener('click', () => move(0, 10));
    container.querySelector('#btnLeft').addEventListener('click', () => move(-10, 0));
    container.querySelector('#btnRight').addEventListener('click', () => move(10, 0));

    const getPosString = function (xx) {
        return `page[${xx.pageX},${xx.pageY}] client[${xx.clientX},${xx.clientY}]`;
    }

    /**
     * @param {PointerEvent} event 
     */
    const onClick = function(event) {
        textTokens.push('onClick: ' + getPosString(event));
        updateReport();
    };

    /**
     * @param {MouseEvent} event 
     */
    const onMouseDown = function(event) {
        textTokens.push('onMouseDown: ' + getPosString(event));
        updateReport();
    };

    /**
     * @param {MouseEvent} event 
     */
    const onMouseUp = function(event) {
        textTokens.push('onMouseUp: ' + getPosString(event));
        updateReport();
    };

    /**
     * @param {MouseEvent} event 
     */
    const onMouseMove = function(event) {
        textTokens.push('onMouseMove: ' + getPosString(event));
        updateReport();
    };

    /**
     * @param {MouseEvent} event 
     */
    const onMouseOut = function(event) {
        textTokens.push('onMouseOut: ' + getPosString(event));
        updateReport();
    };

    /**
     * @param {TouchEvent} event 
     */
    const onTouchStart = function(event) {
        textTokens.push('onTouchStart: ' + getPosString(event.changedTouches[0]));
        updateReport();
    };

    /**
     * @param {TouchEvent} event 
     */
    const onTouchMove = function(event) {
        textTokens.push('onTouchMove: ' + getPosString(event.changedTouches[0]));
        updateReport();
    };

    /**
     * @param {TouchEvent} event 
     */
    const onTouchEnd = function(event) {
        textTokens.push('onTouchEnd: ' + getPosString(event.changedTouches[0]));
        updateReport();
    };

    /**
     * @param {TouchEvent} event 
     */
    const onTouchCancel = function(event) {
        textTokens.push('onTouchCancel: ' + getPosString(event.changedTouches[0]));
        updateReport();
    };

    let boxPos = container.querySelector('.box-pos');
    boxPos.addEventListener('click', onClick);
    boxPos.addEventListener('mousedown', onMouseDown);
    boxPos.addEventListener('mouseup', onMouseUp);
    boxPos.addEventListener('mousemove', onMouseMove);
    boxPos.addEventListener('mouseout', onMouseOut);
    boxPos.addEventListener('touchstart', onTouchStart);
    boxPos.addEventListener('touchend', onTouchEnd);
    boxPos.addEventListener('touchmove', onTouchMove);
    boxPos.addEventListener('touchcancel', onTouchCancel);

})();
