function loadJSON(url) {
  return fetch(url)
    .then((value) => value.json())
    .catch((err) => ({ error: err }));
}

async function tryLoadJson() {
  let jj = await loadJSON("json/example-01.json");
  console.log("JSON loaded", jj);
}

async function tryLoadJsonError() {
  let jj = await loadJSON("json/example-01-err.json");
  console.log("JSON loaded", jj);
}
