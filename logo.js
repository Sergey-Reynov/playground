(function () {
    const optimalWidth = 1000

    let defaultWidth = optimalWidth
    const updateDefaultWidth = () => {
        let width = document.body.clientWidth - 20
        defaultWidth = Math.min(optimalWidth, Math.round(Math.floor(width / 100) * 100))
        return defaultWidth
    }

    updateDefaultWidth()
    console.log(defaultWidth)

    const indicator = document.querySelector('#indication-width')
    const publisherArea = document.querySelector('.publisher-area')

    const getWidth = () => publisherArea.clientWidth
    const updateIndicator = () => indicator.innerHTML = `Width: ${getWidth()}px`
    const setWidth = amount => {
        publisherArea.style.width = `${amount}px`
        updateIndicator()
    }

    const changeWidth = changeAmount => setWidth(getWidth() + changeAmount)
    document.querySelector('#button-minus-1').addEventListener('click', () => changeWidth(-1))
    document.querySelector('#button-minus-10').addEventListener('click', () => changeWidth(-10))
    document.querySelector('#button-minus-100').addEventListener('click', () => changeWidth(-100))
    document.querySelector('#button-plus-1').addEventListener('click', () => changeWidth(1))
    document.querySelector('#button-plus-10').addEventListener('click', () => changeWidth(10))
    document.querySelector('#button-plus-100').addEventListener('click', () => changeWidth(100))
    document.querySelector('#indication-width').addEventListener('click', () => setWidth(updateDefaultWidth()))

    const defaultStyle = {}
    const scheme = {
        height: '#input-logo-height',
        top:    '#input-logo-top',
        right:  '#input-logo-right',
    }
    const getInputValue = selector => {
        let x = document.querySelector(selector)
        // console.log('getInputValue', selector, x)
        return x ? parseFloat(x.value) : undefined
    }
    const fillSchemeNode = node => {
        let cfg = {}
        Object.keys(node).map(key => {
            let x = node[key]
            if (typeof x === 'number') cfg[key] = x // value as is
            else if (typeof x === 'string') cfg[key] = getInputValue(x) // selector
            else if (typeof x === 'object') cfg[key] = fillSchemeNode(x) // sub-node
        })
        return cfg
    }
    const getConfigData = () => fillSchemeNode(scheme)
    const onConfigChanged = () => {
        let config = getConfigData()
        updateLogoStyle(config)
    }
    const getLogoElement = () => document.querySelector('.ac-logobrand-image')
    const updateLogoStyle = config => {
        let logo = getLogoElement()
        if (logo) {
            Object.keys(config).forEach(key => {
                logo.style[key] = `${config[key]}px`
                // console.log(key, config[key], logo.style[key])
            })
        }
    }
    const resetLogoStyle = () => {
        updateLogoStyle(defaultStyle)
        Object.keys(defaultStyle).forEach(key => {
            let input = document.querySelector(scheme[key])
            if (input) {
                input.value = parseFloat(defaultStyle[key])
            }
        })
    }
    document.querySelector('#button-logo-reset').addEventListener('click', resetLogoStyle)

    const addChangeListener = selector => {
        let x = document.querySelector(selector)
        if (x) x.addEventListener('change', onConfigChanged)
    }
    const addChangeListeners = node => {
        Object.keys(node).map(key => {
            let x = node[key]
            if (typeof x === 'string') addChangeListener(x) // selector
            else if (typeof x === 'object') addChangeListeners(x) // sub-node
        })
    }
    addChangeListeners(scheme)

    const setupWidth = () => {
        setWidth(defaultWidth)
        setTimeout(() => {
            setWidth(defaultWidth)
            console.log('indication initialized')
        }, 600)
    }
    setupWidth()

    let detector = setInterval(() => {
        let logo = getLogoElement()
        if (logo) {
            let ss = getComputedStyle(logo)
            if (parseFloat(ss.height) > 0) {
                clearInterval(detector)
                Object.keys(scheme).forEach(key => {
                    let input = document.querySelector(scheme[key])
                    if (input) {
                        input.removeAttribute('disabled')
                        input.value = defaultStyle[key] = parseFloat(ss[key])
                    }
                })
            }
        }
    }, 500)

})()
