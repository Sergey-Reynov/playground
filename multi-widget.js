(function () {
    const playground = window._____lrePlayground = window._____lrePlayground || {}

    const context = {
        playerUrl: 'lre/lre.js',
        mode: null,
        trigger: null,
        store: null,
    }

    if (typeof URLSearchParams !== 'undefined') {
        const params = new URLSearchParams(window.location.search)
        context.playerUrl = params.get('js') || context.playerUrl
        context.trigger = params.get('----anyclip-lre-config') || context.trigger
        context.mode = params.get('mode')
    }

    let placeholders = Array.from(document.querySelectorAll('[data-example-player]'))
    let panels = placeholders.map(item => {
        let panel = new WidgetConfigPanel(item.dataset.examplePlayer, item, context, playground)
        panel.create()
        return panel
    })

    const startAllPlayers = () => {
        panels.forEach(panel => panel.startPlayer())
    }

    let btns = Array.from(document.querySelectorAll('[data-click-start-all]'))
    btns.forEach(btn => btn.addEventListener('click', () => startAllPlayers()))

    const startPlayer = (panelId) => {
        let id = String(panelId)
        let panel = panels.find(item => item.id === id)
        if (panel) panel.startPlayer()
    }

    btns = Array.from(document.querySelectorAll('[data-click-start]'))
    btns.forEach(btn => btn.addEventListener('click', () => startPlayer(btn.dataset.panel)))

    playground.startAllPlayers = startAllPlayers

})()
