(function() {
    let resizableElement = null
    let panel = null
    let cssRoot = 'resize-box-panel'
    
    let src = document.currentScript
    if (src && src.dataset) {
        let selector = src.dataset.selector
        if (selector) {
            resizableElement = document.querySelector(selector)
            if (resizableElement) {
                let myUrl = document.currentScript.src
                let styles = document.createElement('link')
                styles.rel = 'stylesheet'
                styles.href = myUrl.replace('.js', '.css')
                document.head.appendChild(styles)

                let initialWidth = resizableElement.clientWidth;
                            
                setTimeout(() => {
                    panel = document.createElement('div')
                    panel.id = `${cssRoot}-${Math.random().toString().substr(2)}`
                    panel.classList.add(cssRoot, 'pane')
                    document.body.insertBefore(panel, document.body.firstChild)

                    panel.insertAdjacentHTML('afterbegin', `
                    <div class="line">
                        <div class="option" id="indication-width">Width</div>
                    </div>
                    <div class="line">
                        <div class="option" id="button-minus-100">-100px</div>
                        <div class="option" id="button-minus-10">-10px</div>
                        <div class="option" id="button-minus-1">-1px</div>
                    </div>
                    <div class="line">
                        <div class="option" id="button-plus-100">+100px</div>
                        <div class="option" id="button-plus-10">+10px</div>
                        <div class="option" id="button-plus-1">+1px</div>
                    </div>`)

                    const indicator = panel.querySelector('#indication-width')
                    const getWidth = () => resizableElement.clientWidth
                    const updateIndicator = () => indicator.innerHTML = `Width: ${getWidth()}px`
                    const setWidth = amount => {
                        resizableElement.style.width = `${amount}px`
                        resizableElement.style.maxWidth = 'unset'
                        resizableElement.style.minWidth = 'unset'
                        updateIndicator()
                    }
                    const changeWidth = changeAmount => setWidth(getWidth() + changeAmount)
                
                    panel.querySelector('#button-minus-1').addEventListener('click', () => changeWidth(-1))
                    panel.querySelector('#button-minus-10').addEventListener('click', () => changeWidth(-10))
                    panel.querySelector('#button-minus-100').addEventListener('click', () => changeWidth(-100))
                    panel.querySelector('#button-plus-1').addEventListener('click', () => changeWidth(1))
                    panel.querySelector('#button-plus-10').addEventListener('click', () => changeWidth(10))
                    panel.querySelector('#button-plus-100').addEventListener('click', () => changeWidth(100))
                    indicator.addEventListener('click', () => setWidth(initialWidth))

                    setWidth(initialWidth)
            
                }, 500)
            }
        }
    }
})()
