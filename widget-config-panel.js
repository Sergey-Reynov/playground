class WidgetConfigPanel {
    constructor(id, placeholder, context, playground) {
        this.id = id
        this.placeholder = placeholder
        this.context = context
        this.controls = null
        this.data = null
        this.playground = playground
        this.used = false
        this.template = `<div id="pcpanel-XXX" class="player-config-panel">
            <div class="item">
                <label for="text-width-XXX">Width of the widget:</label>
                <input type="text" id="text-width-XXX" value="">
            </div>
            <div class="item">
                <label for="text-pubname-XXX">Publisher:</label>
                <input type="text" id="text-pubname-XXX" value="" list="select-publishers-XXX" autocomplete="off" spellcheck="false">
                <datalist id="select-publishers-XXX"></datalist>
            </div>
            <div class="item">
                <label for="text-widgetname-XXX">Widget:</label>
                <input type="text" id="text-widgetname-XXX" value="" list="select-widgets-XXX" autocomplete="off" spellcheck="false">
                <datalist id="select-widgets-XXX"></datalist>
            </div>
            <div class="item"><label for="text-page-url-XXX">Page Url:</label><input type="url" id="text-page-url-XXX" value=""></div>
            <div class="item"><label for="text-lre-url-XXX">LRE Url:</label><input type="url" id="text-lre-url-XXX" value=""></div>
            <div class="item">
                <span><label for="select-mode-tm-XXX">TM:</label><select id="select-mode-tm-XXX"></select></span>
                <span><label for="select-mode-config-XXX">Config:</label><select id="select-mode-config-XXX"></select></span>
                <span><label for="select-mode-mp-XXX">MP:</label><select id="select-mode-mp-XXX"></select></span>
            </div>
            <div style="width: 100%;padding:2px;">
                Specify custom secret feature parameters here,<br/>using JS notation, without external braces:<br/>
                <textarea id="textarea-custom-XXX" style="width:98%;height:50px"></textarea>
            </div>
            <div style="width: 100%;padding:2px;">
                Specify embedded parameters here,<br/>using JS notation, without external braces:<br/>
                <textarea id="textarea-embedded-XXX" style="width:98%;height:50px"></textarea>
            </div>
            <div class="item" style="height: unset;align-items: flex-end;">
                <div>
                    <input type="checkbox" id="check-enabled-XXX"><label for="check-enabled-XXX">Enable this player</label>
                </div>
                <div>
                    <button data-click-start data-panel="XXX" style="height: 90%;">Start player</button>
                    <button data-click-start-all style="height: 90%;">Start all players</button>
                </div>
            </div>
        </div>`;
    }

    loadStorage() {
        this.context.store = JSON.parse(localStorage.getItem('lre-multiplayer')) || {}
        this.context.store.players = this.context.store.players || {}
        return {
            pubname: '',
            widgetname: '',
            page: '',
            lre: '',
            modetm: 'prod',
            modeconfig: 'prod',
            modemp: 'prod',
            enabled: true,
            custom: '',
            embedded: '',
            ...this.context.store.players[this.id],
        }
    }

    saveStorage() {
        this.loadStorage()
        this.context.store.players[this.id] = this.data
        localStorage.setItem('lre-multiplayer', JSON.stringify(this.context.store))
    }

    create() {
        this.data = this.loadStorage()
        
        this.data.modetm = this.data.modetm || 'prod'
        this.data.modeconfig = this.data.modeconfig || this.data.modetm
        this.data.modemp = this.data.modemp || this.data.modetm

        if (this.context.mode) {
            this.data.modetm = mode
            this.data.modeconfig = mode
            this.data.modemp = mode
        }

        this.initPanel()
    }

    initPanel() {
        let tempDiv = document.createElement('div')
        tempDiv.innerHTML = this.template.replace(/\X\X\X/g, this.id)
        let ph = tempDiv.firstChild
        this.placeholder.replaceWith(ph);
        this.placeholder = ph

        this.controls = {
            width:      this.placeholder.querySelector('#text-width-' + this.id),
            publishers: this.placeholder.querySelector('#select-publishers-' + this.id),
            widgets:    this.placeholder.querySelector('#select-widgets-' + this.id),
            pubname:    this.placeholder.querySelector('#text-pubname-' + this.id),
            widgetname: this.placeholder.querySelector('#text-widgetname-' + this.id),
            page:       this.placeholder.querySelector('#text-page-url-' + this.id),
            lre:        this.placeholder.querySelector('#text-lre-url-' + this.id),
            modetm:     this.placeholder.querySelector('#select-mode-tm-' + this.id),
            modeconfig: this.placeholder.querySelector('#select-mode-config-' + this.id),
            modemp:     this.placeholder.querySelector('#select-mode-mp-' + this.id),
            enabled:    this.placeholder.querySelector('#check-enabled-' + this.id),
            custom:     this.placeholder.querySelector('#textarea-custom-' + this.id),
            embedded:   this.placeholder.querySelector('#textarea-embedded-' + this.id),
        }

        this.playground.initSelectModes(this.controls.modetm)
        this.playground.initSelectModes(this.controls.modeconfig)
        this.playground.initSelectModes(this.controls.modemp)

        try {
            this.playground.initPublisherList(this.controls.publishers)
            this.updateWidgetList(this.data.pubname, this.data.widgetname)
        } catch (err) {
            console.log('examples - error', err)
        }

        this.controls.width.value      = this.data.width || ''
        this.controls.pubname.value    = this.data.pubname
        this.controls.widgetname.value = this.data.widgetname
        this.controls.page.value       = this.data.page
        this.controls.lre.value        = this.data.lre
        this.controls.modetm.value     = this.data.modetm
        this.controls.modeconfig.value = this.data.modeconfig
        this.controls.modemp.value     = this.data.modemp
        this.controls.enabled.checked  = this.data.enabled
        this.controls.custom.value     = this.data.custom || ''
        this.controls.embedded.value   = this.data.embedded || ''

        this.refreshControlsState()

        this.controls.publishers.addEventListener('open', () => console.log('publishers open'))

        this.initCombo(this.controls.pubname)
        this.initCombo(this.controls.widgetname)

        this.controls.pubname.addEventListener('change', () => this.onPublisherChanged())
        this.controls.widgetname.addEventListener('change', () => this.onExampleChanged())

        for (let key in this.controls) {
            this.controls[key].addEventListener('change', () => this.updateSession())
        }
    }

    initCombo(comboElement) {
        let dataKey = 'data-last-value'
        comboElement.addEventListener('click', (event) => {
            let rect = comboElement.getBoundingClientRect()
            let offset = rect.width - event.offsetX
            if (offset < 32) {
                if (comboElement.value) {
                    comboElement.setAttribute(dataKey, comboElement.value)
                    comboElement.value = ''
                }
            }
        })
        comboElement.addEventListener('blur', (event) => {
            if (!comboElement.value) {
                comboElement.value = comboElement.getAttribute(dataKey)
                comboElement.removeAttribute(dataKey)
            }
        })
    }

    updateWidgetList(pubname, widgetname) {
        const widgets = this.playground.initWidgetList(pubname, this.controls.widgets)
        this.controls.widgetname.value = ''
        this.controls.page.value       = ''
        if (widgetname || widgets.length === 1) {
            this.updateExample(widgetname || widgets[0])
        }
    }

    onPublisherChanged() {
        this.updateWidgetList(this.controls.pubname.value, null)
    }

    onExampleChanged() {
        this.updateExample(this.controls.widgetname.value)
    }
    
    updateExample(widgetname) {
        let exampleData = this.playground.getWidgetExample(widgetname)
        if (exampleData) {
            this.controls.pubname.value    = exampleData.pubname
            this.controls.widgetname.value = exampleData.widgetname
            this.controls.page.value       = exampleData.page
        }
    }

    refreshControlsState() {
        // const isDisabled = !this.sessionData.enabled
        // this.controls.redirect.disabled = isDisabled
        // this.controls.same.disabled = isDisabled
    }

    updateSession() {
        this.data.width      = this.controls.width.value
        this.data.pubname    = this.controls.pubname.value
        this.data.widgetname = this.controls.widgetname.value
        this.data.page       = this.controls.page.value
        this.data.lre        = this.controls.lre.value
        this.data.modetm     = this.controls.modetm.value
        this.data.modeconfig = this.controls.modeconfig.value
        this.data.modemp     = this.controls.modemp.value
        this.data.enabled    = this.controls.enabled.checked
        this.data.custom     = this.controls.custom.value
        this.data.embedded   = this.controls.embedded.value

        this.refreshControlsState()
        this.saveStorage()
    }

    disablePanel() {
        for (let key in this.controls) {
            this.controls[key].disabled = true
        }
    }

    startPlayer() {
        if (this.used) return
        this.used = true

        if (this.data.enabled && this.data.widgetname && this.data.pubname) {
            let cfg = null
            if (this.context.trigger) {
                console.log('secret trigger:', this.context.trigger)
            } else {
                cfg = this.playground.getMixedConfig(this.data.modetm, this.data.modeconfig, this.data.modemp)
                console.log('config', cfg)
                if (this.data.custom) {
                    try {
                        let ____CPS = null
                        eval('____CPS={' + this.data.custom + '};')
                        if (typeof ____CPS == 'object') {
                            cfg = {
                                ...cfg,
                                ...____CPS
                            }
                        }
                    } catch (ex) {
                        console.log('%cERROR PARSE CUSTOM PARAMS, IGNORED', 'color:yellow;background:darkred');
                    }
                }
            }

            this.playground.addWidgetExample(this.data.pubname, this.data.widgetname, this.data.page)
            let pageLink = ''

            const scr = document.createElement('script')
            scr.src = this.data.lre || this.context.playerUrl
            scr.setAttribute('pubname', this.data.pubname)
            scr.setAttribute('widgetname', this.data.widgetname)
            if (this.data.page) {
                scr.setAttribute('ourl', encodeURIComponent(this.data.page))
                pageLink = `<br><a href="${this.data.page}">${this.data.page}</a>`
            }
            if (this.data.embedded) {
                try {
                    let ____EPS = null
                    eval('____EPS={' + this.data.embedded + '};')
                    if (typeof ____EPS == 'object') {
                        Object.keys(____EPS).forEach(key => scr.setAttribute(key, `${____EPS[key]}`))
                    }
                } catch (ex) {
                    console.log('%cERROR PARSE EMBEDDED PARAMS, IGNORED', 'color:yellow;background:darkred');
                }
            }
            if (cfg) {
                scr.__reload_data = { ...cfg }
                // for compatibility
                window.anyclip = window.anyclip || {}
                window.anyclip.loadConf = cfg
            }

            let box = document.createElement('div')
            if (this.data.width) box.style.width = this.data.width
            box.insertAdjacentHTML('afterbegin', 
                `<div>${this.data.pubname} - ${this.data.widgetname}${pageLink}</div>`)
            box.appendChild(scr)
            this.placeholder.replaceWith(box)
        } else {
            this.disablePanel()
        }
    }
}
