(function() {
    const playground = window._____lrePlayground = window._____lrePlayground || {}

    /**
     * @typedef {Object} WidgetExampleData
     * @property {string} widgetname
     * @property {string} pubname
     * @property {string} page
     */

    /** @type {WidgetExampleData[]} */
    const widgetExamples = [
        { widgetname: '0011r00002IjDWF_725',  pubname: 'usnewscom',       page: 'https://usnews.com' },

        { widgetname: '0011r00002RYAhn_1419', pubname: 'zerohedgecom',    page: 'https://www.zerohedge.com/covid-19/johns-hopkins-prof-half-americans-have-natural-immunity-dismissing-it-biggest-failure' },

        { widgetname: '0011r00001omdBI_1103', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1362', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1363', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1371', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1391', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1392', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1432', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1473', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1489', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1500', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1530', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1531', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1532', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1533', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1534', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1587', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1599', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1600', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1606', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1607', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1608', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1609', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1610', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1611', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1622', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1673', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1770', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1772', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1782', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1929', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1938', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1939', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1940', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1941', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1946', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1947', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1951', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1952', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_1964', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_2000', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_2008', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_2009', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_2011', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_2036', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_2329', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3090', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3472', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3590', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3591', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3592', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3593', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3594', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3597', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_3598', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_4729', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_4730', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_5369', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_6447', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_6751', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_8220', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_8261', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_10348', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        { widgetname: '0011r00001omdBI_10352', pubname: 'seekingalphacom', page: 'https://seekingalpha.com/article/4431595-wall-street-breakfast-blow-to-big-oil' },
        
        { widgetname: '001w000001RrzqE_1830', pubname: 'ibtravelcom',     page: 'https://www.fodors.com/news/news/this-couple-is-bringing-brown-representation-into-vanlife' },
        { widgetname: '001w000001RrzqE_1831', pubname: 'ibtravelcom',     page: 'https://www.fodors.com/community/trending.php' },
        { widgetname: '001w000001RrzqE_5359', pubname: 'ibtravelcom',     page: 'https://wikitravel.org/en/List_of_country_calling_codes' },
        
        { widgetname: '001w000001RrzqE_1580', pubname: 'internetbrandscom', page: 'https://www.ford-trucks.com/forums/1626106-1949-f-3-radio.html' },
        { widgetname: '001w000001RrzqE_1950', pubname: 'internetbrandscom', page: 'https://www.carsdirect.com/2020/honda/accord' },

        { widgetname: '0011r000029hq6T_545',  pubname: 'hannitycom',      page: 'https://hannity.com/media-room/police-targeted-bystanders-save-san-francisco-cop-after-shes-attacked-by-homeless-man/' },

        { widgetname: '0011r00001omyud_1823', pubname: 'venturebeatcom',  page: 'https://venturebeat.com/transform-2020-video-hub/' },

        { widgetname: '0011r00002U19gr_1674', pubname: 'wordhippocom',    page: 'https://www.wordhippo.com/what-is/another-word-for/hope.html' },

        { widgetname: '0011r00002HpNT8_2106', pubname: 'ahoramismocom',   page: 'https://ahoramismo.com/entretenimiento/2015/10/late-del-castillo-novela-ingobernable-irene-urzua/amp/' },

        { widgetname: '0011r00002QDsVq_1112', pubname: 'Heuteat',         page: 'https://amp.heute.at/s/-am-arbeitsplatz-habe-ich-noch-nie-sexismus-erlebt--53380435' },

        { widgetname: '0011r00002NsxYJ_7033', pubname: 'nolacom',         page: 'https://nola.com' },

        { widgetname: '0011r00002K45eM_1386', pubname: 'anyclipnewscom',  page: 'https://playground.anyclip.com/player' },

        { widgetname: 'basic_alex_ccPlaylist_1', pubname: 'lre_qa_alexei',   page: 'https://playground.anyclip.com/player' },

        { widgetname: 'floating',                pubname: 'lre_qa_Alice',    page: 'https://playground.anyclip.com/player' },
        
        { widgetname: 'LiveStreamPCN',           pubname: 'lre_qa_sagi',     page: 'https://playground.anyclip.com/player' },
        { widgetname: 'sagiCC',                  pubname: 'lre_qa_sagi',     page: 'https://playground.anyclip.com/player' },
    ]

    /** @type {WidgetExampleData[]} */
    let history = null
    const loadHistory = () => {
        history = JSON.parse(localStorage.getItem('lre-widget-history')) || []
    }
    const saveHistory = () => {
        localStorage.setItem('lre-widget-history', JSON.stringify(history))
    }
    loadHistory()

    let widgetMap = {}
    let widgetExampleMap = {}

    const addToMap = (item) => {
        item = { ...item }
        widgetMap[item.widgetname] = item
        let list = widgetExampleMap[item.pubname]
        if (!list) list = widgetExampleMap[item.pubname] = []
        list.push(item)
    }
    
    const initWidgetMap = () => {
        widgetMap = {}
        widgetExampleMap = {}

        widgetExamples.forEach(addToMap)

        history.forEach(item => {
            if (!widgetMap[item.widgetname]) {
                addToMap(item)
            }
        })
    }
    initWidgetMap()

    /**
     * @param {HTMLDataListElement} dataList 
     */
    const initPublisherList = (dataList) => {
        Object.keys(widgetExampleMap).sort().forEach(pubname => {
            const option = document.createElement('option')
            option.textContent = pubname
            option.value = pubname
            dataList.appendChild(option)
        })
    }

    /**
     * @param {string} pubname
     * @param {HTMLDataListElement} dataList 
     * @returns {string[]} list of widget names for the pubname
     */
    const initWidgetList = (pubname, dataList) => {
        dataList.innerHTML = '' // remove all options
        const list = []
        const examples = getWidgetExamples(pubname)
        if (examples) {
            examples.sort(compareByWidgetname).forEach(example => {
                const option = document.createElement('option')
                option.textContent = example.widgetname
                option.value = example.widgetname
                dataList.appendChild(option)
                list.push(example.widgetname)
            })
        }
        return list
    }

    /**
     * @param {WidgetExampleData} a 
     * @param {WidgetExampleData} b 
     * @returns {number}
     */
    const compareByWidgetname = (a, b) => {
        return a.widgetname.localeCompare(b.widgetname)
    }

    /**
     * @param {string} widgetname 
     * @returns {WidgetExampleData} example data if exists
     */
    const getWidgetExample = (widgetname) => {
        return widgetMap[widgetname]
    }

    /**
     * @param {string} pubname 
     * @returns {WidgetExampleData[]} list of examples if exists
     */
    const getWidgetExamples = (pubname) => {
        return widgetExampleMap[pubname]
    }

    /**
     * @param {string} pubname 
     * @param {string} widgetname 
     * @param {string} page 
     */
    playground.addWidgetExample = (pubname, widgetname, page) => {
        if (pubname && widgetname) {
            loadHistory()
            let example = { widgetname, pubname, page }
            let idx = history.findIndex(item => item.widgetname === example.widgetname)
            if (idx < 0) history.push(example)
            else history[idx] = example
            saveHistory()
        }
    }

    /** Copy accumulated history to the console log as JSON-formatted string */
    playground.printWidgetHistory = () => {
        console.log('%c Widget History ', 'color: yellow; background: blue', history)
        let strHistory = JSON.stringify(history, null, 2)
        console.log(strHistory)
    }

    playground.clearWidgetHistory = () => {
        history = []
        saveHistory()
        initWidgetMap()
    }

    playground.getWidgetExamples = getWidgetExamples
    playground.getWidgetExample = getWidgetExample
    playground.initWidgetList = initWidgetList
    playground.initPublisherList = initPublisherList
})()
